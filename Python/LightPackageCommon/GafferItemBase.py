# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import NodegraphAPI
import PackageSuperToolAPI.NodeUtils as NU

_3DELIGHT_RENDERER_NAME = "dl"

class BasePackage:
    """
    Common code which serves as a base for all our GafferThree package classes,
    both lights and light filters.
    """
    ADD_MENU_GROUP_NAME = "3Delight"

    @classmethod
    def getLocationExpression(cls):
        """
        Expression to link our scene graph location to the name of the package.
        """
        return '=^/%s' % NU.GetPackageLocationParameterPath()

    @classmethod
    def appendNode(cls, packageNode, editing, node):
        """
        Append a node to either the package group or its edit stack.
        """
        if editing:
            packageNode.buildChildNode(adoptNode=node)
        else:
            NU.AppendNodes(packageNode, (node,))

    @classmethod
    def addTypeAndBBoxNodes(cls, packageNode, locType):
        """
        Create and append nodes to set the location type and delete the
        bounding box.
        """
        locExpr = cls.getLocationExpression()
        # Set the type of the package location.
        typeAttrSetNode = NodegraphAPI.CreateNode('AttributeSet', packageNode)
        typeAttrSetNode.setName("SetTypeAttributeSet")
        typeAttrSetNode.getParameter('paths.i0').setExpression(locExpr)
        typeAttrSetNode.getParameter('attributeName').setValue('type', 0)
        typeAttrSetNode.getParameter('attributeType').setValue('string', 0)
        typeAttrSetNode.getParameter('stringValue.i0').setValue(locType, 0)
        cls.appendNode(packageNode, False, typeAttrSetNode)

        # Delete bounding box. Otherwise it shows up in viewer despite our VMP.
        bbAttrSetNode = NodegraphAPI.CreateNode('AttributeSet', packageNode)
        bbAttrSetNode.setName("BBDelAttributeSet")
        bbAttrSetNode.getParameter('paths.i0').setExpression(locExpr)
        bbAttrSetNode.getParameter('attributeName').setValue('bound', 0)
        bbAttrSetNode.getParameter('action').setValue('Delete', 0)
        cls.appendNode(packageNode, False, bbAttrSetNode)

    @classmethod
    def addShaderNode(cls, packageNode, editing, shaderType, shaderName):
        """
        Create and append the node needed to set or edit a shader on the item's
        location. Returns that node.
        """
        locExpr = cls.getLocationExpression()
        matNode = NodegraphAPI.CreateNode('Material', packageNode)
        # For lights, material location == light location.
        matNode.getParameter('action').setValue('edit material', 0)
        matNode.getParameter('edit.location').setExpression(locExpr)

        # That's all there is to an edit.
        if editing:
            NU.AddNodeRef(packageNode, 'material_edit', matNode)
            cls.appendNode(packageNode, editing, matNode)
            return matNode

        # Add required shader to material node.
        shader = matNode.addShaderType(
                _3DELIGHT_RENDERER_NAME + shaderType)
        shader.getChild("value").setValue(shaderName, 0)
        shader.getChild("enable").setValue(1, 0)

        NU.AddNodeRef(packageNode, 'material', matNode)
        cls.appendNode(packageNode, editing, matNode)
        return matNode

    @classmethod
    def addTransformEditNode(cls, packageNode):
        """
        Create and append the TransformEdit node used in edit packages.
        """
        locExpr = cls.getLocationExpression()
        node = NodegraphAPI.CreateNode('TransformEdit', packageNode)
        actionParam = node.getParameter('action')
        actionParam.setValue('override interactive transform', 0)
        pathParam = node.getParameter('path')
        pathParam.setExpression(locExpr)
        # Add reference and add to package.
        NU.AddNodeRef(packageNode, 'transform_edit', node)
        cls.appendNode(packageNode, True, node)
