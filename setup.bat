@echo off

rem Set DEVROOT_3DFK to the location of this script, as a cygwin path. Inspired by
rem stuff in winenv.bat from 3Delight.
for /f "usebackq tokens=*" %%a in (`%ComSpec% /C "cygpath -au %0 | sed -e 's,/setup\\(\\.bat\\)\\?,,'"`) do set DEVROOT_3DFK=%%a

rem 3Delight home directory where various stuff is needed for the build.
if not "%HOME_3DELIGHT%" == "" goto home_3delight_defined
set HOME_3DELIGHT=q:
:home_3delight_defined

set KATANA_HOME=%HOME_3DELIGHT%\building\katana\windows\katana-3.5
