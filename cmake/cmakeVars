#
# 3Delight for Katana CMakeList
#
# There are three contexts that are supported for building:
# 
# 1. Building as part of a 3Delight distribution (daily build)
#    This is assumed when DELIGHT_BUILD_ROOT is defined. The 3Delight 
#    dependencies are fetched under that directory.
#    BUILD_ENV_3DELIGHT_DISTRIBUTION will be TRUE in this context.
#
# 2. Building against a 3Delight source tree.
#    This is assumed when the DELIGHT_ROOT env var is defined.
#    A working environment for building 3Delight is assumed. CMake will fetch 
#    components fromx "external", the standard public 3Delight includes and 
#    libs.
#    BUILD_ENV_3DELIGHT_SOURCES will be TRUE in this context.
#
# 3. Building without a full 3Delight source tree.
#    Required paths definitions:
#    DELIGHT -> path to your 3Delight package installation
#    EXTERNAL_BUILD_PATH -> path to a copy of the built 3Delight external packages
#    BUILD_ENV_CUSTOM will be TRUE in this context.
#
# In all scenarios, you must provide a path to XGen includes. This can be done
# by defining MAYA_LOCATION to the Maya installation directory. Alternately,
# for setups that have access to ~3delight/building, you may define the Maya
# version number in MAYA_VERSION and the related includes will be used. You will
# also need to provide the OSL shaders, which are in the osl_shaders reopsitory.
# Place your checkout next to your 3dfk checkout and it will be found
# automatically, or supply its location via OSL_SHADERS_DIR.
# 

# Default to installing where we build, in a "3DelightForKatana" directory. It
# makes no sense for us to default to /usr/local as we really have only two
# install cases:
# - Development (install here).
# - Packaging (install to 3Delight build, set by caller).
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "3DelightForKatana" CACHE PATH "..." FORCE)
endif()

#
# Handle the Cygwin paths of various daily build system env vars. CMake expects
# Windows-style paths, with slashes.
#
# This is activated by defining CONVERT_PATHS env var (or passed as -D to the 
# cmake command)
#
if(CMAKE_SYSTEM_NAME MATCHES Windows)
    if(NOT DEFINED CONVERT_PATHS)
        if(DEFINED ENV{CONVERT_PATHS})
            set(CONVERT_PATHS $ENV{CONVERT_PATHS})
        elseif(DEFINED DELIGHT_ROOT OR DEFINED ENV{DELIGHT_ROOT})
            set(CONVERT_PATHS TRUE)
        endif()
    endif()
endif()

macro(getNativePath path)
    if(CONVERT_PATHS)
        execute_process(COMMAND cygpath.exe -m ${${path}} OUTPUT_VARIABLE ${path})
        string(STRIP ${${path}} ${path})
    endif()
endmacro()

#
# DELIGHT_BUILD_ROOT is expected to be defined when building a 3Delight
# distribution (daily build process)
#
if(NOT DEFINED DELIGHT_BUILD_ROOT AND DEFINED ENV{DELIGHT_BUILD_ROOT})
    set(DELIGHT_BUILD_ROOT $ENV{DELIGHT_BUILD_ROOT})
endif()

if(DELIGHT_BUILD_ROOT)
    getNativePath(DELIGHT_BUILD_ROOT)
    set(BUILD_ENV_3DELIGHT_DISTRIBUTION TRUE)
endif()

#
# DELIGHT_ROOT is normally defined for a 3Delight dev envrionment.
#
if(NOT DEFINED DELIGHT_ROOT AND DEFINED ENV{DELIGHT_ROOT})
    set(DELIGHT_ROOT $ENV{DELIGHT_ROOT})
endif()

if(DELIGHT_ROOT)
    getNativePath(DELIGHT_ROOT)
endif()

if(NOT BUILD_ENV_3DELIGHT_DISTRIBUTION AND DEFINED DELIGHT_ROOT)
    set(BUILD_ENV_3DELIGHT_SOURCES TRUE)
endif()

#
# DELIGHT is expected to be defined for a 3Deligh dev environment or a custom
# 3dfk environment.
#
if(NOT DEFINED DELIGHT)
    if(DEFINED ENV{DELIGHT})
        set(DELIGHT $ENV{DELIGHT})
    endif()
endif()

if(NOT BUILD_ENV_3DELIGHT_DISTRIBUTION AND NOT BUILD_ENV_3DELIGHT_SOURCES)
    if(NOT DEFINED DELIGHT)
        # A more precise message would include DELIGHT_BUILD_ROOT, but it is not
        # really intended to be used outside of the daily build routine.
        #
        message(FATAL_ERROR "One of DELIGHT or DELIGHT_ROOT must be defined. Aborting.")
    else()
        set(BUILD_ENV_CUSTOM TRUE)
    endif()
endif()

getNativePath(DELIGHT)

#
# DL_ABI_STRING is expected to be defined for a 3Delight dev environment.
#
if(NOT DEFINED DL_ABI_STRING)
    if(DEFINED ENV{DL_ABI_STRING})
        set(DL_ABI_STRING $ENV{DL_ABI_STRING})
    else()
        if(CMAKE_SYSTEM_NAME MATCHES Windows)
            set(DL_ABI_STRING "windows-x86_64-vc14.0-release")
        else()
            set(DL_ABI_STRING "linux-x86_64")
        endif()
    endif()
endif()

#
# PLAT is expected to be defined in a 3Delight dev environment.
#
if(NOT DEFINED PLAT)
    if(DEFINED ENV{PLAT})
        set(PLAT $ENV{PLAT})
    else()
        if(CMAKE_SYSTEM_NAME MATCHES Windows)
            set(PLAT "win64_x64")
        elseif(CMAKE_SYSTEM_NAME MATCHES Linux)
            set(PLAT "Linux-x86_64")
        endif()
    endif()
endif()

#
# cmake Build type
# 
# Derive decent value from MAKE_MODE if CMAKE_BUILD_TYPE is not specified.
#
if(NOT DEFINED CMAKE_BUILD_TYPE OR NOT CMAKE_BUILD_TYPE)
    if(DEFINED ENV{MAKE_MODE})
        set(MAKE_MODE $ENV{MAKE_MODE})
        if(MAKE_MODE MATCHES debug)
            set(CMAKE_BUILD_TYPE Debug)
            message("Using CMAKE_BUILD_TYPE=Debug since MAKE_MODE is set to debug.")
        else()
            set(CMAKE_BUILD_TYPE Release)
            message("Using CMAKE_BUILD_TYPE=Release since MAKE_MODE is not set to debug.")
        endif()
    endif()    
endif()

#
# 3Delight external libs
#
if(NOT DEFINED EXTERNAL_BUILD_PATH)
    if(DEFINED ENV{EXTERNAL_BUILD_PATH})
        set(EXT_PATH $ENV{EXTERNAL_BUILD_PATH})
    elseif(BUILD_ENV_3DELIGHT_DISTRIBUTION OR BUILD_ENV_3DELIGHT_SOURCES)
        set(EXT_PATH ${DELIGHT_ROOT}/external-build/${DL_ABI_STRING})
    else()
        message(FATAL_ERROR "EXTERNAL_BUILD_PATH must be specified when building without 3Delight sources.")
    endif()

    getNativePath(EXT_PATH)
endif()

set(EXTERNAL_BUILD_PATH ${EXT_PATH} CACHE PATH
    "Path to 3Delight external libraries build directory.")

#
# 3Delight standard libs, includes and binaries
#
if(BUILD_ENV_3DELIGHT_DISTRIBUTION)
    set(DELIGHT_BINPATH ${DELIGHT_BUILD_ROOT}/bin)

    if(DEFINED DELIGHT_DYNAMIC_LIBS)
        set(DELIGHT_LIBPATH ${DELIGHT_DYNAMIC_LIBS}/lib)
        getNativePath(DELIGHT_LIBPATH)
    else()
        set(DELIGHT_LIBPATH ${DELIGHT_BUILD_ROOT}/lib)
    endif()

    set(DELIGHT_INCPATH ${DELIGHT_ROOT}/include)
else()
    # Use the libs and binaries from $DELIGHT
    # Use the includes from $DELIGHT_ROOT or $DELIGHT.
    #
    set(DELIGHT_BINPATH ${DELIGHT}/bin)
    set(DELIGHT_LIBPATH ${DELIGHT}/lib)

    if(BUILD_ENV_3DELIGHT_SOURCES)
        set(DELIGHT_INCPATH ${DELIGHT_ROOT}/include)
    else()
        set(DELIGHT_INCPATH ${DELIGHT}/include)
    endif()
endif()

#
# This is required when linking with some 3Delight static libs with the old
# libc we use for broad linux compatibility when building a distribution.
#
if(CMAKE_SYSTEM_NAME MATCHES Linux)
    set(CMAKE_SHARED_LINKER_FLAGS -lrt)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
endif()

# Typical crap needed on windows to minimize macro pollution.
# And more crap to avoid annoying warnings.
if(CMAKE_SYSTEM_NAME MATCHES Windows)
	add_definitions(-DNOMINMAX -DWIN32_LEAN_AND_MEAN)
	add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

#
# 3Delight OSL shaders
#
if(NOT DEFINED OSL_SHADERS_DIR)
    if(DEFINED ENV{OSL_SHADERS_DIR})
        set(OSL_SHADERS_DIR $ENV{OSL_SHADERS_DIR})
    else()
        set(OSL_SHADERS_DIR ${CMAKE_SOURCE_DIR}/../osl_shaders)
    endif()
    getNativePath(OSL_SHADERS_DIR)
endif()

set(OSL_SHADERS_DIR ${OSL_SHADERS_DIR} CACHE PATH
    "Path to 3Delight OSL shaders.")

#
# Maya dependencies (for XGen)
# 
if(NOT DEFINED MAYA_LOCATION AND DEFINED ENV{MAYA_LOCATION})
    set(MAYA_LOCATION $ENV{MAYA_LOCATION})
endif()

if(NOT DEFINED MAYA_VERSION AND DEFINED ENV{MAYA_VERSION})
    set(MAYA_VERSION $ENV{MAYA_VERSION})
endif()

if(DEFINED MAYA_LOCATION)
    set(MAYA_XGEN_INCPATH ${MAYA_LOCATION}/plug-ins/xgen/include)
    getNativePath(MAYA_XGEN_INCPATH)
endif()

if(DEFINED MAYA_VERSION)
    set(
        NETWORK_MAYA_XGEN_INCPATH
        $ENV{HOME_3DELIGHT}/building/mayalibs/${PLAT}/maya${MAYA_VERSION}/xgen/include)
    getNativePath(NETWORK_MAYA_XGEN_INCPATH)
endif()

if(NOT DEFINED MAYA_LOCATION AND NOT DEFINED MAYA_VERSION)
    message(
        FATAL_ERROR "One of MAYA_LOCATION or MAYA_VERSION must be defined.")
endif()

set(XGEN_INCLUDE_PATHS ${MAYA_XGEN_INCPATH} ${NETWORK_MAYA_XGEN_INCPATH})
