/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2018                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportInstance.h"

#include "AttributeUtils.h"
#include "ExportContext.h"
#include "ExportGeo.h"
#include "ExportSG.h"

/**
	\brief Export a hierarchical instance.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location to export as an instance.

	This will refer to another location in the scene graph (the instance
	source). Because we can't assume that location was already exported (it
	depends on traversal order), we'll always export it as a transform node.

	This type of instancing is the most flexible and is supported by both
	Arnold and PrMan as well.
*/
void ExportInstance::ExportHierarchicalInstance(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	std::string instanceSource;
	AttributeUtils::GetString(
		i_location, "geometry.instanceSource", instanceSource );

	if( instanceSource.empty() )
		return;

	/* Export this location. */
	ExportSG::ExportGroup( i_ctx, i_location );
	/* Always create instance source (in case it is exported later). */
	i_ctx.m_nsi.Create( instanceSource, "transform" );
	/* Connect them together. */
	i_ctx.m_nsi.Connect(
		instanceSource, "", i_location.getFullName(), "objects" );
	/*
		Once supported in 3Deligt, we should adapt ExportArbitraryValues to
		read instance.arbitrary and call it here to handle attribute
		overrrides.
	*/
}

/**
	\brief Attempt to export an instance defined by an ID.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location to attempt to export as an instance.
	\param i_nsi_node_type
		The NSI type of the node which will be instanced (ie. i_location|leaf).
		It may be created by this function to avoid a thread synchronization
		mess.
	\returns
		true if the location was exported as an instance and nothing else must
		be exported. false if the regular export must be done as usual.

	These are called leaf-level instances because they only work on geometry
	(leaf nodes) in Arnold. We keep that restriction for now.

	The katana instance demo scene has two "advanced" cases which rely on a
	node at a higher level of the hierarchy being exported before children of
	its siblings. We should keep that in mind when eventually doing multithread
	export.
*/
bool ExportInstance::ExportIDInstance(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	const std::string &i_nsi_node_type )
{
	std::string ID;
	AttributeUtils::GetString( i_location, "instance.ID", ID );

	if( ID.empty() )
		return false;

	/*
		Deletion is not handled. It would be tricky to track when the last
		instance is deleted and thus we can remove the actual mesh. For now,
		let the geo export do its thing and delete the mesh. This means
		removing the one we picked as master will nuke all instances.
	*/
	if( i_location.IsDeleted() )
	{
		assert( false );
		return false;
	}

	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";
	std::string instance_path = geo_name;

	i_ctx.FirstIDInstancePath( ID, instance_path, i_nsi_node_type );

	/* This is an instance but it's the first time we see it so the export must
	   be done anyway. */
	if( instance_path == geo_name )
		return false;

	/* Connect the instance. */
	i_ctx.m_nsi.Connect( instance_path, "", xform_name, "objects" );
	return true;
}

/**
	\brief Export an instance array.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location to export as an instance.

	An instance array location is what the USD point instancer generates:
	https://github.com/PixarAnimationStudios/USD/wiki/PointInstancer-Object-Model

	It creates multiple instances from a single location efficiently.
*/
void ExportInstance::ExportInstanceArray(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_ctx, i_location );

	/* We'll create the instances node as "/path|leaf". */
	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	if( i_location.IsDeleted() )
	{
		/* Live update deletion. */
		i_ctx.m_nsi.Delete( geo_name );
		return;
	}

	/* Create and connect the instancer. */
	i_ctx.m_nsi.Create( geo_name, "instances" );
	i_ctx.m_nsi.Connect( geo_name, "", xform_name, "objects" );

	if( i_location.IsLiveUpdate() )
	{
		/*
			Delete attributes which might have vanished and not be exported in
			the update so they don't stick around.
		*/
		i_ctx.m_nsi.DeleteAttribute( geo_name, "modelindices" );
		i_ctx.m_nsi.DeleteAttribute( geo_name, "disabledinstances" );
		/* Same idea for source model connections. */
		i_ctx.m_nsi.Disconnect( NSI_ALL_NODES, "", geo_name, "sourcemodels" );
	}

	/* Check for minimal instancing data. */
	FnAttribute::StringAttribute instanceSource_attr(
		i_location.getAttribute( "geometry.instanceSource" ) );
	FnAttribute::DoubleAttribute matrix_attr(
		i_location.getAttribute( "geometry.instanceMatrix" ) );

	if( !instanceSource_attr.isValid() || !matrix_attr.isValid() )
		return;

	/* First connect the sources. */
	auto instanceSource_value = instanceSource_attr.getNearestSample( 0.0f );
	int idx = 0;
	for( const char *source_name : instanceSource_value )
	{
		/* Create source node in case it has not been exported yet. */
		i_ctx.m_nsi.Create( source_name, "transform" );
		/* Connect, with an index. NSI's version of an array of connections. */
		i_ctx.m_nsi.Connect( source_name, "", geo_name, "sourcemodels",
			NSI::IntegerArg( "index", idx++ ) );
	}

	/* Export optional instanceIndex. */
	ExportSG::ExportAnyTypeAttribute(
		i_ctx,
		i_location.getAttribute( "geometry.instanceIndex" ),
		geo_name.c_str(),
		"modelindices",
		NSITypeInteger,
		0, 0 );

	/* Export optional instanceSkipIndex. */
	ExportSG::ExportAnyTypeAttribute(
		i_ctx,
		i_location.getAttribute( "geometry.instanceSkipIndex" ),
		geo_name.c_str(),
		"disabledinstances",
		NSITypeInteger,
		0, 0 );

	/* Export the transformation matrices. */
	ExportSG::ExportFloatAttribute(
		i_ctx,
		matrix_attr,
		geo_name.c_str(),
		"transformationmatrices",
		NSITypeDoubleMatrix,
		0, 0 );

	/* Export arbitrary values. */
	ExportGeo::ExportArbitraryValues( i_ctx, i_location, {} );
}
