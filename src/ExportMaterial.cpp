/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportMaterial.h"

#include "AttributeUtils.h"
#include "ExportContext.h"
#include "Procedural.h" // TODO: remove, does not belong here
#include "utils.h"

#include <stack>
#include <stdio.h>

namespace ExportMaterial
{

const ShaderInfo kShaderTypeInfo[5] =
{
{
	_3DELIGHT_RENDERER_NAME _3DELIGHT_SURFACE_TERMINAL,
	_3DELIGHT_RENDERER_NAME _3DELIGHT_SURFACE_TERMINAL _3DELIGHT_SHADER,
	"surfaceshader"
},
{
	_3DELIGHT_RENDERER_NAME _3DELIGHT_LIGHT_TERMINAL,
	_3DELIGHT_RENDERER_NAME _3DELIGHT_LIGHT_TERMINAL _3DELIGHT_SHADER,
	"surfaceshader"
},
{
	_3DELIGHT_RENDERER_NAME _3DELIGHT_DISPLACEMENT_TERMINAL,
	_3DELIGHT_RENDERER_NAME _3DELIGHT_DISPLACEMENT_TERMINAL _3DELIGHT_SHADER,
	"displacementshader"
},
{
	_3DELIGHT_RENDERER_NAME _3DELIGHT_ENVIRONMENT_TERMINAL,
	_3DELIGHT_RENDERER_NAME _3DELIGHT_ENVIRONMENT_TERMINAL _3DELIGHT_SHADER,
	"surfaceshader"
},
{
	_3DELIGHT_RENDERER_NAME _3DELIGHT_VOLUME_TERMINAL,
	_3DELIGHT_RENDERER_NAME _3DELIGHT_VOLUME_TERMINAL _3DELIGHT_SHADER,
	"volumeshader"
}
};

/**
	\brief Export one OSL material (network or not).

	\param i_ctx
		The export context.
	\param i_materialLocation
		The location of the material to export.
	\param i_material_group
		The group which contains the material. Katana uses "material".

	This also exports an attributes node which can be used to assign that
	material to some geometry. This allows us to hide the detail of which
	shaders the material contains (eg. only surface, surface + displacement,
	...).
*/
void ExportOneMaterial(
	ExportContext &i_ctx,
	const SGLocation &i_materialLocation,
	const std::string &i_material_group )
{
	unsigned nTypes = sizeof(kShaderTypeInfo)/sizeof(kShaderTypeInfo[0]);

	std::string material_name = i_materialLocation.getFullName();
	std::string attr_handle = material_name + "|material";

	FnAttribute::GroupAttribute material_group(
		i_materialLocation.getAttribute( i_material_group.c_str() ) );

	/*
		On top of the location being deleted, we also handle a missing material
		group in a live update as a deleted material. That case happens with
		local material definitions (ie. dl_local_material). It's important to
		only do this with live updates as otherwise our attempt to export
		dl_local_material in groups will break all lights, more or less.
	*/
	if( i_materialLocation.IsDeleted() ||
	    (i_materialLocation.IsLiveUpdate() && !material_group.isValid()) )
	{
		/* Just wipe the attributes and all shaders connected to it. */
		i_ctx.m_nsi.Delete( attr_handle, NSI::IntegerArg( "recursive", 1 ) );
		return;
	}

	/*
		Always recursive delete the shading networks first. This is so removal
		of nodes in a network works correctly. We don't do the attributes node
		as a whole so the material assignments are not lost. Note that this
		code should really be inside WriteNSI_Shading_Network() once that is
		cleaned up.
	*/
	if( i_materialLocation.IsLiveUpdate() )
	{
		for( unsigned i = 0; i < nTypes; ++i )
		{
			const ShaderInfo &si = kShaderTypeInfo[i];
			/* This covers both the cases below. */
			i_ctx.m_nsi.Delete(
				material_name + "|" + si.m_name,
				NSI::IntegerArg( "recursive", 1 ) );
		}
	}

	/*
		There's nothing useful to export if there is no material group at all.
		The code below would already do nothing except create a dummy
		attributes node which is wasteful because of our attempt to export
		dl_local_material on all group locations.
	*/
	if( !material_group.isValid() )
		return;

	// TODO: move this code in here and clean it up
	Procedural::WriteNSI_Shading_Network(
		i_ctx, i_materialLocation, i_material_group );

	/* Now export the attributes node. */
	i_ctx.m_nsi.Create( attr_handle, "attributes" );

	FnAttribute::GroupAttribute terminals_group(
		material_group.getChildByName( "terminals" ) );

	bool has_light_shader = false;
	for( unsigned i = 0; i < nTypes; ++i )
	{
		const ShaderInfo &si = kShaderTypeInfo[i];

		if( material_group.getChildByName( si.m_shader_name ).isValid() )
		{
			/* From a Material node. */
			i_ctx.m_nsi.Connect(
				material_name + "|" + si.m_name, "",
				attr_handle, si.m_nsi_attribute );

			has_light_shader = has_light_shader || i == shader_light;
		}
		else if( terminals_group.getChildByName( si.m_name ).isValid() )
		{
			/* From a NetworkMaterial node. */
			i_ctx.m_nsi.Connect(
				material_name + "|" + si.m_name, "",
				attr_handle, si.m_nsi_attribute );

			if( i == shader_displacement )
			{
				/* 
					Retrieve the bounding sphere that may have been stored by
					WriteNSI_Shading_Network, and output it as the displacement
					bound.
					Since this relies specifically on "displacementShader" and
					because this shader cannot realistically be useful by itself
					in a material node, the bounding sphere gets computed only
					for network materials, and thus we only attempt to output
					it for netwrok materials.
				*/
				std::string shader_name = material_name + "|dlDisplacement";
				float bound = -1.0f;
				if( Utils::GetBoundingSphereCache( shader_name, bound ) )
				{
					NSI::DynamicArgumentList attributes;
					attributes.Add( 
						new NSI::FloatArg( "displacementbound", bound ) );

					i_ctx.m_nsi.SetAttribute(
						attr_handle, 
						attributes );
				}
			}
		}
	}

	if( has_light_shader )
	{
		ExportLightFilters( i_ctx, i_materialLocation );
	}
}

/**
	\brief Export light filters and connect them to light shader.

	\param i_ctx
		The export context.
	\param i_materialLocation
		The location of the material being exported. It must be a light shader.

	This exports light filters, which non network materials which get copied by
	a terminal Op to the 'material.filters.filterName' group.
*/
void ExportLightFilters(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	std::string material_name = i_location.getFullName();

	FnAttribute::GroupAttribute filters_group(
		i_location.getAttribute( "material.filters" ) );

	std::string last_filter;
	int64_t nfilters = filters_group.getNumberOfChildren();
	for( int64_t i = 0; i < nfilters; ++i )
	{
		std::string filter_name = filters_group.getChildName( i );
		std::string filter_base_attr = "material.filters.";
		filter_base_attr += filter_name;

		/* Export the filter's shader. */
		ExportOneShader(
			i_ctx, i_location,
			filter_base_attr, "LightFilter", filter_name );

		std::string filter_handle = material_name + "|" + filter_name;

		if( last_filter.empty() )
		{
			last_filter = filter_handle;
		}
		else
		{
			/*
				Because OSL won't let us use an array for the 'filter'
				input and connect to its elements, we resort to using an
				intermediate node to merge multiple filters together.
			*/
			std::string filter_mix_handle =
				material_name + "|filter_mix_" + std::to_string(i);

			i_ctx.m_nsi.Create( filter_mix_handle, "shader" );
			i_ctx.m_nsi.SetAttribute(
				filter_mix_handle, NSI::StringArg(
					"shaderfilename",
					Utils::GetOSLShaderPath("filter_multiply" ) ) );

			/* Connect previous filter and current filters to mix node. */
			i_ctx.m_nsi.Connect(
				last_filter, "filter_output",
				filter_mix_handle, "filter1" );
			i_ctx.m_nsi.Connect(
				filter_handle, "filter_output",
				filter_mix_handle, "filter2" );

			last_filter = filter_mix_handle;
		}
	}

	/* Connect the last filter node to the light shader. */
	if( !last_filter.empty() )
	{
		i_ctx.m_nsi.Connect(
			last_filter, "filter_output",
			material_name + "|dlLight", "filter" );
	}
}

void CreateShader(
	ExportContext &i_ctx,
	const std::string &i_handle,
	const NSI::ArgumentList &i_attributes )
{
	i_ctx.m_nsi.Create( i_handle, "shader" );
	i_ctx.m_nsi.SetAttribute( i_handle, i_attributes );
}

/**
	\brief Exports a single shader for a non network material.

	This means either a surface, displacement, etc shader. Non network
	materials are represented with:
	- A string attribute, like 'material.dlSurfaceShader', with the shader's
	name.
	- A group attribute, like 'material.dlSurfaceParams', with the shader's
	arguments as attributes in that group.

	'Surface' can be any terminal name as defined in
	RendererInfo::fillRendererObjectTypes() for kFnRendererObjectTypeShader.

	\param i_ctx
		The export context.
	\param i_location
		The location of the material to export.
	\param i_material_group
		The group which contains the two attributes described above. Typically
		just "material" (always that for shaders defined by katana).
	\param i_terminal_name
		Which shader of the material to export (eg. Surface, Displacement, etc).
	\param i_handle_name
		Name which is appended to the location to create the handle for the
		shader. ie. "location|i_handle_name". If this is not provided, the
		default is "dl" + i_terminal_name.
*/
void ExportOneShader(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	const std::string &i_material_group,
	const std::string &i_terminal_name,
	std::string i_handle_name )
{
	/* eg. 'material.dlSurface' */
	std::string base_attr_name = i_material_group;
	base_attr_name += "." _3DELIGHT_RENDERER_NAME;
	base_attr_name += i_terminal_name;

	std::string shaderName;
	AttributeUtils::GetString(
		i_location, (base_attr_name + "Shader").c_str(), shaderName );

	if( shaderName.empty() )
		return;

	FnAttribute::GroupAttribute shaderParamsAttr =
		i_location.getAttribute( (base_attr_name + "Params").c_str() );

	if( i_handle_name.empty() )
	{
		i_handle_name = _3DELIGHT_RENDERER_NAME;
		i_handle_name += i_terminal_name;
	}

	const DlShaderInfo& osl_shader =
		Utils::GetOSLShaders().getShaderInfo(shaderName);

	NSI::DynamicArgumentList attributes;

	/* Add the shader name. */
	{
		std::string shader = Utils::GetOSLDirectory();
		shader += "/";
		shader += shaderName;

		attributes.Add( new NSI::StringArg( "shaderfilename", shader ) );
	}

	/*
		Process shader attributes. Only the attributes that have non-default
		values will be contained in shaderParamsAttr.
	*/
	if( shaderParamsAttr.isValid() )
	{
		std::vector<Procedural::shader_arg_t> args;
		args.reserve(shaderParamsAttr.getNumberOfChildren());
		for( int i=0; i<shaderParamsAttr.getNumberOfChildren(); i++)
		{
			args.emplace_back(
				shaderParamsAttr.getChildName(i),
				shaderParamsAttr.getChildByIndex(i));
		}
		Procedural::ProcessAllShaderAttributes(osl_shader, args, attributes);
	}

	std::string shader_name = i_location.getFullName() + "|" + i_handle_name;

	/* Output the shader and its attributes. */
	CreateShader( i_ctx, shader_name, attributes );

	std::vector<std::string> createdNodeNames;
	/* Iterate on all shader parameters to process virtual connections. */
	for( int i = 0; i < osl_shader.nparams(); ++i )
	{
		const DlShaderInfo::Parameter* param = osl_shader.getparam(i);

		Procedural::ProcessDefaultConnection(
			i_ctx,
			i_location,
			osl_shader,
			param,
			shader_name,
			createdNodeNames );
	}
}

/**
	\brief Export the default material and assign it to a location.
*/
void ExportDefaultMaterial(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	if( i_location.IsDeleted() )
		return;

	std::string shader = Utils::GetOSLShaderPath( "defaultKatana" );
	i_ctx.m_nsi.Create( "default_material", "shader" );
	i_ctx.m_nsi.SetAttribute( "default_material",
			NSI::StringArg( "shaderfilename", shader ) );

	i_ctx.m_nsi.Create( "default_material_attr", "attributes" );
	i_ctx.m_nsi.Connect(
		"default_material", "",
		"default_material_attr", "surfaceshader" );
	i_ctx.m_nsi.Connect(
		"default_material_attr", "",
		i_location.getFullName(), "geometryattributes" );
}

}
