/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportSG_h
#define __ExportSG_h

#include "nsi.h"
#include "SGLocation.h"

class ExportContext;

/*
	This is a place for scene graph export functions which don't have any other
	more specific place to be or which are common to several places.

	As a convention, scene graph locations are exported to NSI nodes with
	handle equal to the scene graph path.

	The meshes add "|leaf" to the path.
	The attributes nodes add "|attr" to the path.
	The attributes nodes created for a material add "|material" to the path.
	The nodes of a shading network add "|network|" to the path, followed by a
	node name.
	The attributes nodes used on lights for light linking add "|visibility" to
	the light's path.
	The cameras add "|leaf" for the camera node and "|screen" for the
	corresponding screen node.

	Attributes and material are connected to the transform nodes, not the
	geometry (ie. no "|leaf"). This allows uniformity between attributes on
	leaves and attributes in the middle of the hierarchy.
*/
namespace ExportSG
{
	void ListAttributes(
		const FnAttribute::Attribute &i_attr,
		const char *i_attr_name,
		unsigned i_depth );

	void ListAllNodeAttributes(
		const SGLocation &i_location );

	void ExportSGSubtree(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_iterator );

	void ExportSGSubtreeBasic(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_iterator );

	void ExportSGSubtreeMultithread(
		ExportContext &i_ctx,
		const FnKat::FnScenegraphIterator &i_iterator );

	void ExportSGLocation(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportLiveUpdateItem(
		ExportContext &i_ctx,
		FnAttribute::GroupAttribute &i_update_item );

	void ExportGroup(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportTransform(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportLocalMaterialAndAttributes(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportSet(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportFloatAttribute(
		ExportContext &i_ctx,
		const FnAttribute::Attribute &i_attribute,
		const char *i_nsi_node,
		const char *i_nsi_attribute_name,
		NSIType_t i_nsi_type,
		unsigned i_arraylength,
		int i_nsi_flags );

	void ExportAnyTypeAttribute(
		ExportContext &i_ctx,
		const FnAttribute::Attribute &i_attribute,
		const char *i_nsi_node,
		const char *i_nsi_attribute_name,
		NSIType_t i_nsi_type,
		unsigned i_arraylength,
		int i_nsi_flags );
}

#endif
