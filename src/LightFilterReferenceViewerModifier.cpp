/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "LightFilterReferenceViewerModifier.h"

#include "AttributeUtils.h"
#include <FnViewerModifier/plugin/FnViewerModifierInput.h>

FnKat::ViewerModifier* LightFilterReferenceViewerModifier::create(
	FnKat::GroupAttribute args )
{
	return new LightFilterReferenceViewerModifier( args );
}

const char* LightFilterReferenceViewerModifier::getLocationType()
{
	return "light filter reference";
}


LightFilterReferenceViewerModifier::LightFilterReferenceViewerModifier(
	FnKat::GroupAttribute args )
:
	LightFilterViewerModifier( args )
{
}


void LightFilterReferenceViewerModifier::setup(
	FnKat::ViewerModifierInput& input )
{
	FnAttribute::StringAttribute ref_path_attr(
		input.getLiveAttribute( "referencePath" ) );
	std::string ref_path = ref_path_attr.getValue( "", false );
	if( !ref_path.empty() )
	{
		FilterSetup(
			input, true, input.getLiveAttribute( "material", ref_path ) );
	}
}
