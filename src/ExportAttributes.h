/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportAttributes_h
#define __ExportAttributes_h

#include "ExportMaterial.h"
#include "SGLocation.h"

class ExportContext;

/*
	This is a namespace for functions which export values which end up on an
	NSI attributes node. For example, material assignments, visibility, etc.
*/
namespace ExportAttributes
{
	void ExportAttributes(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	std::string ExportAttributesNode(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		bool &io_has_attr_node );

	void ExportMaterialAssign(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportOneVisibilityAttribute(
		FnAttribute::GroupAttribute &i_group_attr,
		const char* i_attr_name,
		NSI::ArgumentList &io_attributes );

	void ExportVisibilityAttributes(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		bool &io_has_attr_node );

	void ExportLightLinking(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		bool &io_has_attr_node );

	void ExportLightAttributes(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		bool &io_has_attr_node );

	void ExportCompositingMode(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		bool &io_has_attr_node );
}

#endif
