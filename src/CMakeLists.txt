set(CMAKE_CXX_STANDARD 11)
#
# Use a platform-specific default value for KATANA_HOME if it is not provided on
# the command line nor as an environment variable.
#
if(DEFINED ENV{KATANA_HOME})
    set(KATANA_PATH $ENV{KATANA_HOME})
else()
    # Find Katana
    if (CMAKE_SYSTEM_NAME MATCHES Windows)
        get_filename_component( KATANA_BIN_PATH "[HKEY_CLASSES_ROOT\\KatanaProject\\DefaultIcon]" DIRECTORY CACHE )
        get_filename_component( KATANA_PATH "${KATANA_BIN_PATH}" DIRECTORY )
    else ()
        find_program( KATANA_EXEC_PATH katana )
        get_filename_component( KATANA_PATH "${KATANA_EXEC_PATH}" DIRECTORY )
    endif ()
endif()

getNativePath(KATANA_PATH)
set(KATANA_HOME ${KATANA_PATH} CACHE PATH "Path to Katana")

list(INSERT CMAKE_MODULE_PATH 0 "${KATANA_HOME}/plugins/Src/cmake")
find_package(Katana PATHS "${KATANA_HOME}/plugin_apis/cmake" REQUIRED)

add_library(3Delight_for_Katana SHARED
    AttributeUtils.cpp
    ExportAttributes.cpp
    ExportCamera.cpp
    ExportContext.cpp
    ExportGeo.cpp
    ExportInstance.cpp
    ExportLight.cpp
    ExportMaterial.cpp
    ExportOptions.cpp
    ExportOutputs.cpp
    ExportSG.cpp
    GlobalSettings.cpp
    KatanaMonitorDriver.cpp
    LogReporting.cpp
    OSLShaderInfo.cpp
    LightFilterReferenceViewerModifier.cpp
    LightFilterViewerModifier.cpp
    LightLocator.cpp
    LightViewerModifier.cpp
    Procedural.cpp
    RenderSettings.cpp
    Renderer.cpp
    RendererInfo.cpp
    SGLocation.cpp
    System.cpp
    XGenLoader.cpp
    XGenNSICallbacks.cpp
    XGenViewerModifier.cpp
    main.cpp
    utils.cpp
    ${KATANA_HOME}/plugin_apis/src/FnViewer/utils/FnGLShaderProgram.cpp
    ${KATANA_HOME}/plugin_apis/src/FnViewer/utils/FnBaseLocator.cpp )

# Boost
set(BOOST_ROOT ${EXTERNAL_BUILD_PATH}/boost CACHE PATH "Path to Boost library")
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)

find_package(Boost REQUIRED COMPONENTS filesystem system thread regex date_time chrono iostreams)

set(OpenGL_GL_PREFERENCE LEGACY)
find_package(OpenGL)

# Other libraries
set(CMAKE_LIBRARY_PATH 
    ${CMAKE_LIBRARY_PATH}
    ${DELIGHT_LIBPATH}
    ${EXTERNAL_BUILD_PATH}
    ${EXTERNAL_BUILD_PATH}/ilmbase/lib
    ${EXTERNAL_BUILD_PATH}/tbb/lib
    ${EXTERNAL_BUILD_PATH}/glew/lib)

# Fix library lookup on Windows
if(CMAKE_SYSTEM_NAME MATCHES Windows)
    set(CMAKE_FIND_LIBRARY_PREFIXES "lib" "")
    set(CMAKE_FIND_LIBRARY_SUFFIXES ".a" ".lib" ".dll")
endif()

find_library(_3DELIGHT_LIBRARY 3delight)
target_link_libraries(3Delight_for_Katana PUBLIC ${_3DELIGHT_LIBRARY})

# Libs from the "external build"
#
find_library(HALF_LIBRARY Half)
target_link_libraries(3Delight_for_Katana PRIVATE ${HALF_LIBRARY})

find_library(TBB_LIBRARY tbb)
target_link_libraries(3Delight_for_Katana PRIVATE ${TBB_LIBRARY})

find_library(GLEW_LIBRARY GLEW)
target_link_libraries(3Delight_for_Katana PRIVATE ${GLEW_LIBRARY})

# Includes
target_include_directories(3Delight_for_Katana PUBLIC ${DELIGHT_INCPATH})
target_include_directories(3Delight_for_Katana PUBLIC ${XGEN_INCLUDE_PATHS})
target_include_directories(3Delight_for_Katana PRIVATE ${Boost_INCLUDE_DIRS})
target_include_directories(3Delight_for_Katana PRIVATE ${EXTERNAL_BUILD_PATH}/ilmbase/include)
target_include_directories(3Delight_for_Katana PRIVATE ${EXTERNAL_BUILD_PATH}/ilmbase/include/OpenEXR)
target_include_directories(3Delight_for_Katana PRIVATE ${EXTERNAL_BUILD_PATH}/tbb/include)
target_include_directories(3Delight_for_Katana PRIVATE ${EXTERNAL_BUILD_PATH}/glew/include)

if (MSVC)
    set(CompilerFlags
        CMAKE_CXX_FLAGS
        CMAKE_CXX_FLAGS_DEBUG
        CMAKE_CXX_FLAGS_RELEASE
        CMAKE_C_FLAGS
        CMAKE_C_FLAGS_DEBUG
        CMAKE_C_FLAGS_RELEASE )
    foreach(CompilerFlag ${CompilerFlags})
        string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
    endforeach()

    # M_PI
    add_definitions(-D_USE_MATH_DEFINES)

    # OIIO warnings
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4005 /wd4244 /wd4305 /wd4800")

    # OpenVDB warnings
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4146")

    # Deprecated functions warnings (eg. strdup)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4996")
else ()
    add_compile_options(-Wall)

    set(CMAKE_SHARED_LINKER_FLAGS
        "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--no-undefined")
    link_libraries(-lrt ${CMAKE_DL_LIBS})
endif ()

set_property(TARGET 3Delight_for_Katana PROPERTY PREFIX "")

# This part of the Katana API is using deprecated functions from elsewhere in
# the API so build it without warnings on Linux.
if( CMAKE_SYSTEM_NAME STREQUAL Linux )
	target_compile_options( foundry.katana.FnRenderPlugin PRIVATE -w )
endif()

target_link_libraries(3Delight_for_Katana PRIVATE
    Katana::FnAsset
    Katana::FnAttribute
    Katana::FnConfig
    Katana::FnDisplayDriver
    Katana::FnGeolibServices
    Katana::FnLogging
    Katana::FnRenderOutputUtils
    Katana::FnRenderPlugin
    Katana::FnRendererInfo
    Katana::FnRendererInfoPlugin
    Katana::FnScenegraphIterator
    Katana::FnViewerAPI
    Katana::FnViewerModifierPlugin
    Katana::pystring

    ${Boost_LIBRARIES}
    OpenGL::GL
    OpenGL::GLU )

if (MSVC)
    target_link_libraries(3Delight_for_Katana PRIVATE
        ws2_32
        psapi )
endif ()

install(TARGETS 3Delight_for_Katana DESTINATION Libs)

