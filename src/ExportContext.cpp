/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportContext.h"
#include "SGLocation.h"

#include <assert.h>

ExportContext::ExportContext(
	const NSI::DynamicArgumentList &i_nsi_begin_args,
	const FnKat::FnScenegraphIterator &i_rootIterator,
	Renderer &i_renderer )
:
	m_renderer( i_renderer ),
	m_globalSettings( i_rootIterator ),
	m_renderSettings( i_rootIterator ),
	m_output_layer_count( 0 )
{
	m_nsi.Begin( i_nsi_begin_args );
}

/**
	\brief Returns the handle to an output driver.

	Because several outputs might go to the same driver but we don't really
	know this in a structured way (it's a loose binding based on file name) so
	manage a map of output drivers to reuse the same one when required.
*/
std::string ExportContext::OutputDriverHandle(
	const std::string &i_filename,
	const std::string &i_drivername,
	bool *o_first_request )
{
	std::lock_guard<std::mutex> ctx_l( m_mutex );

	*o_first_request = false;
	std::string key = i_drivername + "!@#$%" + i_filename;
	std::string &handle = m_output_driver_map[key];
	if( handle.empty() )
	{
		*o_first_request = true;
		handle = "driver" + std::to_string( m_output_driver_map.size() );
	}
	return handle;
}

/**
	\brief Generate a unique handle for an output layer node.
*/
std::string ExportContext::GenOutputLayerHandle()
{
	std::lock_guard<std::mutex> ctx_l( m_mutex );

	return "layer" + std::to_string( ++m_output_layer_count );
}

/**
	\brief Record or retrieve the first path for a given instance ID.

	\param i_ID
		The instance ID string. Must not be empty.
	\param io_path
		On input, the scene graph path of the location which is potentially
		instanced. On exit, the path of the object to use. Will be unchanged if
		there is no instancing or this object is the one instanced (ie. it must
		be output in both cases).
	\param i_nsi_node_type
		If the object is the master copy being instanced, this function will
		create the NSI node with this provided type. This is done in here,
		while we hold a mutex, so that the node is immediately available to
		other threads which might want to instance it (ie. connect it to a
		transform). It's perfectly fine for the regular export code to create
		the node again as creating an existing node is a NOP in NSI.
*/
void ExportContext::FirstIDInstancePath(
	const std::string &i_ID,
	std::string &io_path,
	const std::string &i_nsi_node_type )
{
	assert( !io_path.empty() );
	assert( !i_ID.empty() );

	std::lock_guard<std::mutex> ctx_l( m_mutex );

	Instance &instance = m_instance_map[i_ID];
	if( !instance.m_master_path.empty() )
	{
		io_path = instance.m_master_path;
		return;
	}

	instance.m_master_path = io_path;
	/* See i_nsi_node_type comment above. */
	m_nsi.Create( io_path, i_nsi_node_type );
}

/**
	\brief Add a screen to the list of screens in the scene.
*/
void ExportContext::AddScreen( const std::string &i_handle )
{
	std::lock_guard<std::mutex> ctx_l( m_mutex );
	m_screens.insert( i_handle );
}

/**
	\brief Remove a camera from the list of cameras in the scene.
*/
void ExportContext::RemoveScreen( const std::string &i_handle )
{
	std::lock_guard<std::mutex> ctx_l( m_mutex );
	m_screens.erase( i_handle );
}

/**
	\brief Disconnect old screen and connect new screen for all layers.
*/
void ExportContext::ChangeScreenForLayers(
	const std::string &i_oldScreen,
	const std::string &i_newScreen )
{
	std::lock_guard<std::mutex> ctx_l( m_mutex );
	for (unsigned i = 1; i <= m_output_layer_count; i++)
	{
		std::string layer_handle = "layer" + std::to_string( i );
		m_nsi.Disconnect( layer_handle, "", i_oldScreen, "outputlayers" );
		m_nsi.Connect( layer_handle, "", i_newScreen, "outputlayers" );
	}
}

/**
	\returns A cache eviction manager for the current thread.
*/
CacheEvictionManager& ExportContext::EvictionManager()
{
	return m_cem_tls.local();
}
