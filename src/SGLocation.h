/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __SGLocation_h
#define __SGLocation_h

#include "FnScenegraphIterator/FnScenegraphIterator.h"

/**
	\brief Handle to a scene graph location and its attributes.

	This class represents a scene graph location and allows accessing its
	attributes. Its purpose is to abstract how that information is actually
	obtained, which can be either a scene graph iterator or a group attribute
	for live render updates.

	Conceptually, this is like the access part of a scene graph iterator (ie.
	an iterator without the methods to actually iterate). That's why it has a
	very similar API.
*/
class SGLocation
{
public:
	/* The iterator is held by reference so keep it valid. */
	SGLocation( const FnKat::FnScenegraphIterator &i_iterator );

	/* The attribute group is held by reference so keep it valid. */
	SGLocation(
		const std::string &i_location,
		const std::string &i_type,
		const FnAttribute::GroupAttribute &i_attributes );

	std::string getType() const;

	/**
		\brief Returns the location's path.

		eg. /root/world/geo/sphere
	*/
	const std::string& getFullName() const { return m_location; }

	/**
		\brief Returns the location's parent's path.

		eg. /root/world/geo
	*/
	std::string getParentFullName() const;

	FnAttribute::Attribute getAttribute( const char *i_name ) const;

	FnAttribute::StringAttribute getAttributeNames() const;

	/** \brief Returns true if this location is from a live render update. */
	bool IsLiveUpdate() const { return m_attributes != 0x0; }

	/** \brief Returns true if this location was deleted in a live update. */
	bool IsDeleted() const { return m_deleted; }

	FnKat::FnScenegraphIterator getRoot() const;

private:
	std::string m_location;
	std::string m_type;
	const FnKat::FnScenegraphIterator *m_iterator;
	const FnAttribute::GroupAttribute *m_attributes;
	int m_deleted;
};

#endif
