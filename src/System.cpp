#include "System.h"

#ifdef _WIN32
#	include <windows.h>
#else
#	include <dirent.h>
#	include <dlfcn.h>
#	include <pwd.h>
#	include <unistd.h>
#	include <sys/stat.h>
#	include <sys/types.h>
#endif

#include <stdlib.h>
#include <string.h>

namespace System
{

std::string LibraryPath()
{
	static int foo = 0;

#ifdef _WIN32
	HMODULE hMod;
	if( !GetModuleHandleEx(
			GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS
			| GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
			(LPCTSTR)&foo,
			&hMod ) )
	{
		return std::string();
	}

	char name[1024];
	if( 0 != GetModuleFileName( hMod, name, sizeof(name) ) )
	{
		name[sizeof(name)-1] = 0;
		return std::string( name );
	}
#else
	Dl_info info;
	if( dladdr( &foo, &info ) )
	{
		if( info.dli_fname )
			return std::string( info.dli_fname );
	}
#endif
	return std::string();
}


std::string UserLoginName()
{
#ifdef _WIN32
	char name[100];
	unsigned buf_size = sizeof(name);

	if (GetUserName(name, (LPDWORD) &buf_size))
		return name;
#else
	uid_t uid = getuid();
	struct passwd *user = getpwuid(uid);

	if( user && user->pw_name )
		return user->pw_name;
#endif
	return std::string();
}


std::string FullPathName( const std::string &i_path )
{
	if( i_path.empty() )
		return std::string();

#ifdef _WIN32
	unsigned bufSize = GetFullPathName( i_path.c_str(), 0, 0x0, 0x0 );

	if( bufSize == 0 )
		return 0x0;

	char *fullname = (char*) malloc( bufSize );

	unsigned retval = GetFullPathName( i_path.c_str(), bufSize, fullname, 0x0 );

	if( retval == 0 || retval > bufSize )
	{
		free( fullname );
		return std::string();
	}

	std::string r( fullname );
	free( fullname );
	return r;
#else
	/*
		FIXME: This could cause a buffer overflow. This function is broken by
		design according to its man page so we should probably implement a
		'fixed' version for both Linux and OS X (Linux already has a safe way
		of using it but it's an extension).
	*/
	char buffer[4096];
	if( realpath( i_path.c_str(), buffer ) )
	{
		return std::string( buffer );
	}

	return std::string();
#endif
}


bool FileIsDir( const char *i_path )
{
#ifdef _WIN32
	DWORD result = GetFileAttributes(i_path);
	return
		((result!=0xFFFFFFFF)&&((result&FILE_ATTRIBUTE_DIRECTORY)!=0)) ?
			true : false;
#else
	struct stat buf;

	if( lstat(i_path, &buf) < 0 )
	{
		return false;
	}

	return (buf.st_mode & S_IFDIR) == S_IFDIR;
#endif
}


bool ScanDir(
	const char *i_path,
	void (*i_userFct)(const char*, const char*, void *),
	void *i_userArg,
	bool i_recurse )
{
#ifdef _WIN32
	bool all_ok = true;
	if( FileIsDir(i_path) )
	{
		char toFind[_MAX_PATH];
		WIN32_FIND_DATA searchData;
		HANDLE handle = NULL;

		_snprintf( toFind, _MAX_PATH-1, "%s\\*", i_path );

		handle = FindFirstFile( toFind, &searchData );

		if( handle == INVALID_HANDLE_VALUE )
		{
			return false;
		}

		do
		{
			if ( strcmp(searchData.cFileName, "."  ) &&
			     strcmp(searchData.cFileName, ".." ) )
			{
				char filename[ _MAX_PATH ];
				sprintf( filename,"%s\\%s", i_path, searchData.cFileName );

				if( i_recurse )
				{
					if( !ScanDir( filename, i_userFct, i_userArg ) )
						all_ok = false;
				}
				else if( !FileIsDir(filename) )
				{
					i_userFct( filename, searchData.cFileName, i_userArg );
				}
			}

		} while( FindNextFile(handle, &searchData) );

		FindClose( handle );
	}
	else
	{
		i_userFct( i_path, i_path, i_userArg );
	}
	return all_ok;
#else
	DIR *pDir;

	struct dirent *pDirent;
	char tmp[4096];

	if ( (pDir = opendir(i_path)) == 0x0 )
	{
		return false;
	}

	bool all_ok = true;
	while ( (pDirent = readdir(pDir)) )
	{
		if( strcmp(pDirent->d_name, "." )  &&
			strcmp(pDirent->d_name, "..") )
		{
			strcpy(tmp, i_path);
			strcat(tmp, "/");
			strcat(tmp, pDirent->d_name);

			if( i_recurse )
			{
				if( FileIsDir(tmp) )
				{
					if( !ScanDir(tmp, i_userFct, i_userArg) )
						all_ok = false;
				}
			}
			else
			{
				i_userFct( tmp, pDirent->d_name, i_userArg );
			}
		}
	}

	closedir(pDir);
	return all_ok;
#endif
}

} /* namespace System */
