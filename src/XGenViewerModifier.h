/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include <FnViewerModifier/plugin/FnViewerModifier.h>
#include <FnAttribute/FnAttribute.h>

class XGenViewerModifier : public FnKat::ViewerModifier
{
	public:
		XGenViewerModifier(FnKat::GroupAttribute args);
		~XGenViewerModifier();

		static FnKat::ViewerModifier* create(FnKat::GroupAttribute args);

		static FnKat::GroupAttribute getArgumentTemplate();

		static const char* getLocationType();

		/**
		   Called per VMP instance before each draw
		 */
		virtual void deepSetup(FnKat::ViewerModifierInput& input);
		/**
		   Called once per VMP instance when constructed
		 */
		virtual void setup(FnKat::ViewerModifierInput& input);
		/**
		   Performs the render code.
		 */
		virtual void draw(FnKat::ViewerModifierInput& input);

		/**
		   Called when the GLM's host location is removed/refreshed.
		 */
		virtual void cleanup(FnKat::ViewerModifierInput& input);
		/**
		   Called per VMP instance after each draw
		 */
		virtual void deepCleanup(FnKat::ViewerModifierInput& input);

		/**
		   Returns a DoubleAttribute representing the local space bounding box
		   for the current location to be used with the viewer scenegraph, and
		   useful for culling.

		   The bounding box is represented by 6 double values, that are
		   the interleaved coordinates of minimum and maximum vertices of the
		   bounding box (xMin, xMax, yMin, yMax, zMin, zMax).

		   @param input The input interface to Katana's scenegraph.
		   @return The double attribute representing the bounding box.
		 */
		virtual FnKat::DoubleAttribute getLocalSpaceBoundingBox(
				FnKat::ViewerModifierInput& input);

		static void flush();

		static void onFrameBegin();

		static void onFrameEnd();

	private:
		int LoadGetXGenGeoID(const FnKat::ViewerModifierInput& input);
		void ClearXGenCache(const FnKat::ViewerModifierInput& input);
};

