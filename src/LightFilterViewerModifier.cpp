/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

/* Lots of includes from the katana example. */
#ifdef _WIN32
#include <FnPlatform/Windows.h>
#define _USE_MATH_DEFINES // for C++
#endif
#include <cmath>

#include <FnViewerModifier/plugin/FnViewerModifier.h>
#include <FnViewerModifier/plugin/FnViewerModifierInput.h>
#include <FnAttribute/FnGroupBuilder.h>
#include <FnAttribute/FnAttribute.h>
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
/* End of katana example includes. */

#include "LightFilterViewerModifier.h"

#include "AttributeUtils.h"

FnKat::ViewerModifier* LightFilterViewerModifier::create(
	FnKat::GroupAttribute args )
{
	return new LightFilterViewerModifier( args );
}

FnKat::GroupAttribute LightFilterViewerModifier::getArgumentTemplate()
{
	return FnKat::GroupAttribute();
}

const char* LightFilterViewerModifier::getLocationType()
{
	return "light filter";
}

void LightFilterViewerModifier::flush()
{
}

void LightFilterViewerModifier::onFrameBegin()
{
}

void LightFilterViewerModifier::onFrameEnd()
{
}


LightFilterViewerModifier::LightFilterViewerModifier(
	FnKat::GroupAttribute args )
:
	FnKat::ViewerModifier( args ),
	m_type( type_unknown )
{
}


void LightFilterViewerModifier::setup( FnKat::ViewerModifierInput& input )
{
	FilterSetup( input, false, input.getLiveAttribute( "material" ) );
}

void LightFilterViewerModifier::cleanup( FnKat::ViewerModifierInput& input )
{
}


void LightFilterViewerModifier::deepSetup( FnKat::ViewerModifierInput& input )
{
	/* Disable default drawing. */
	input.overrideHostGeometry();
}


void LightFilterViewerModifier::draw( FnKat::ViewerModifierInput& input )
{
	glPushAttrib( GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_LINE_BIT );
	glDisable( GL_LIGHTING );
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	if( input.getDrawOption("isPicking") )
	{
		/* Easier picking. */
		glLineWidth( 7 );
	}
	else
	{
		if( input.isSelected() )
		{
			/* White when selected. */
			glColor3f( 1.0, 1.0, 1.0 );
		}
		else
		{
			/* Yellow seems to be the convention for katana lights. */
			glColor3f( 1.0, 1.0, 0.0 );
		}
	}

	switch( m_type )
	{
		case type_unknown:
			break;
		case type_gobo:
			drawGobo();
			break;
	}

	glPopAttrib();
}


void LightFilterViewerModifier::deepCleanup( FnKat::ViewerModifierInput& input )
{
}


FnKat::DoubleAttribute LightFilterViewerModifier::getLocalSpaceBoundingBox(
	FnKat::ViewerModifierInput& input )
{
	/*
		The gobo sits in front of the light and that's the only thing we draw
		for now.
	*/
	double bounds[6] = { -1, 1, -1, 1, -2, -1 };
	return FnKat::DoubleAttribute( bounds, 6, 1 );
}

/**
	\brief Setup the filter VMP with the filter's material group.

	Note that for a filter reference, this has to be fetched from the source
	filter's location.
*/
void LightFilterViewerModifier::FilterSetup(
	FnKat::ViewerModifierInput &input,
	bool is_reference,
	const FnAttribute::GroupAttribute &i_material )
{
	m_type = type_unknown;

	std::string shader;
	AttributeUtils::GetString( i_material, "dlLightFilterShader", shader );

	if( shader == "goboFilter" )
	{
		m_type = type_gobo;

		/* This default value should match goboFilter.osl's default for
		   useFilterCoordinateSystem. */
		int local = 1;

		AttributeUtils::GetInteger(
			i_material, "dlLightFilterParams.useFilterCoordinateSystem",
			local );

		/*
			Don't draw references if the gobo is set to use filter coordinates.
			Only the original should be drawn as it will have the correct
			location.
		*/
		if( local && is_reference )
		{
			m_type = type_unknown;
		}

		/*
			When not using the local filter coordinates, don't draw the filter
			if it is not parented under a light (that means it would probably
			be used by references).

			If it is parented under a light, we should cancel its transform.
			This is TODO (would involve computing the local xform and inverting
			it). It does not need to be done for references as they can't be
			transformed with the available UI. For now, just hope the user has
			enough sense not to transform the filter.
		*/
		if( !local && !is_reference )
		{
			std::string loc = input.getFullName();
			std::string parent_loc = loc.substr( 0, loc.find_last_of( "/" ) );
			FnAttribute::StringAttribute parent_type_attr(
				input.getAttribute( "type", parent_loc ) );
			std::string parent_type = parent_type_attr.getValue( "", false );
			if( parent_type != "light" )
				m_type = type_unknown;
		}
	}
}

/*
	Just a quad until I think of something nice to show orientation, at least
	up/down and ideally front/back as well.
*/
void LightFilterViewerModifier::drawGobo()
{
	/* Scale a little to put in front of spot light. */
	glPushMatrix();
	glScalef( 1.5, 1.5, 1.5 );

	glBegin( GL_POLYGON );
	glVertex3f( -0.5, -0.5, -1.0f );
	glVertex3f( 0.5, -0.5, -1.0f );
	glVertex3f( 0.5, 0.5, -1.0f );
	glVertex3f( -0.5, 0.5, -1.0f );
	glEnd();

	glPopMatrix();
}
