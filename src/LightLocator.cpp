/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2018                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "LightLocator.h"

#include <FnViewer/plugin/FnGLStateHelper.h>

using namespace std;
LightLocatorVDC::LightLocatorVDC()
{
}

bool LightLocatorVDC::locationEvent(
    const Foundry::Katana::ViewerAPI::ViewerLocationEvent& i_event,
    bool locationHandled)
{
    bool handled = FnBaseLocatorVDC::locationEvent(i_event, locationHandled);

    FnAttribute::StringAttribute packageClassAttr =
        i_event.attributes.getChildByName("info.gaffer.packageClass");

    std::string package = packageClassAttr.getValue("", false);

    if( package == "MeshLightPackage" )
    {
        return false;
    }

    return handled;
}

bool LightLocator::matches(
    const FnKat::ViewerAPI::ViewerLocationEvent& i_event)
{
    FnAttribute::StringAttribute packageClassAttr =
        i_event.attributes.getChildByName("info.gaffer.packageClass");

    std::string package = packageClassAttr.getValue("", false);

    /*
        Trap the incandescenceLightPackage & EnvironmentLightPackage here
        so the Viewer does not draw the default locator for them
    */
    if( package == "AreaLightPackage" ||
        package == "DistantLightPackage" ||
        package == "SpotLightPackage" || 
        package == "MeshLightPackage" ||
        package == "PointLightPackage" ||
        package == "IncandescenceLightPackage" ||
        package == "EnvironmentLightPackage" ||
        package == "GoboFilterPackage" )
    {
        return true;
    }

    return false;
}

bool LightLocator::overridesBaseGeometry(
    const FnKat::ViewerAPI::ViewerLocationEvent& event)
{
    return true;
}

/// Gets the bounds of the given location.
FnAttribute::DoubleAttribute LightLocator::getBounds(
    const FnKat::ViewerAPI::ViewerLocationEvent& event)
{
    const double bound[6] = { -1, 1, -1, 1,  -1, 1.5 };
    static const FnAttribute::DoubleAttribute s_boundAttr(bound, 6, 2);
    return s_boundAttr;
}

/// Computes the extent of the given location.
FnAttribute::DoubleAttribute LightLocator::computeExtent(
    const FnKat::ViewerAPI::ViewerLocationEvent& event)
{
    return LightLocator::getBounds(event);
}

void LightLocator::draw( const std::string& locationPath )
{
    // GLStateRestore prevents us from polluting the GL context
    FnKat::ViewerUtils::GLStateRestore glStateRestore(
        FnKat::ViewerUtils::Polygon | FnKat::ViewerUtils::Lighting |
        FnKat::ViewerUtils::Line);

    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    bool isSelected = isLocationSelected(locationPath);

    drawLight(locationPath, false, isSelected );
}

void LightLocator::pickerDraw( const std::string& locationPath )
{
    FnKat::ViewerUtils::GLStateRestore glStateRestore(
        FnKat::ViewerUtils::Polygon | FnKat::ViewerUtils::Lighting |
        FnKat::ViewerUtils::Line);

    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    /* Easier picking. */
    glLineWidth(7);

    drawLight(locationPath, true, false);
}

LightLocator::eType LightLocator::getLightType(
    const std::string& i_locationPath )
{
    FnAttribute::StringAttribute packageClassAttr =
        getAttribute( i_locationPath, "info.gaffer.packageClass" );

    std::string package = packageClassAttr.getValue("", false);

    eType lightType = type_unknown;

    if( package == "AreaLightPackage" )
    {
        lightType = type_area;

        /*
        FnAttribute::GroupAttribute geo_grp(
            getAttribute( i_locationPath, "geometry" ) );

        AttributeUtils::GetString( geo_grp, "shape", shape );
        */

        FnAttribute::StringAttribute shapeAttr =
            getAttribute( i_locationPath, "geometry.shape" );

        std::string shape = shapeAttr.getValue("", false);

        if( shape == "disk" )
            lightType = type_area_disk;
        else if( shape == "sphere" )
            lightType = type_area_sphere;
        else if( shape == "cylinder" )
            lightType = type_area_cylinder;
    }
    else if( package == "DistantLightPackage" )
        lightType = type_distant;
    else if( package == "SpotLightPackage" )
        lightType = type_spot;
    else if( package == "MeshLightPackage" )
        lightType = type_mesh;
    else if( package == "PointLightPackage" )
        lightType = type_point;
    else if( package == "GoboFilterPackage")
        lightType = type_gobo_filter;

    return lightType;
}

void LightLocator::drawLight(
    const std::string& i_locationPath,
    bool i_drawForPicking,
    bool i_selected )
{
    if( !i_drawForPicking )
    {
        setDrawColor( i_selected, 1.0f );
    }

    eType lightType = getLightType( i_locationPath );
    switch( lightType )
    {
        case type_unknown:
            break;
        case type_area:
            drawAreaLight( i_locationPath );
            break;
        case type_distant:
            drawDistantLight( i_locationPath );
            break;
        case type_spot:
            drawSpotLight( i_locationPath );
            break;
        case type_mesh:
            /* Everything is done by the viewer's default drawing. */
            break;
        case type_area_disk:
            drawDiskLight( i_locationPath );
            break;
        case type_area_sphere:
            drawSphereLight( i_locationPath );
            break;
        case type_area_cylinder:
            drawCylinderLight( i_locationPath );
            break;
        case type_point:
            drawPointLight( i_locationPath, i_drawForPicking, i_selected );
            break;
        case type_gobo_filter:
            drawGoboFilter( i_locationPath );
            break;
    }
}

void LightLocator::drawAreaLight( 
    const std::string& i_locationPath )
{
    /*
        We could fetch the geometry from the attributes. But for now it's much
        simpler to just assume the values used in our AreaLightPackage.
    */
    glBegin( GL_POLYGON );
    glVertex3f( -0.5, 0.5, 0.0 );
    glVertex3f( 0.5, 0.5, 0.0 );
    glVertex3f( 0.5, -0.5, 0.0 );
    glVertex3f( -0.5, -0.5, 0.0 );
    glEnd();

    /* Extra line for direction. */
    glBegin( GL_LINE_STRIP );
    glVertex3f( 0.0, 0.0, 0.0 );
    glVertex3f( 0.0, 0.0, -0.5 );
    glEnd();
}

/*
    Draws 3 arrows in a circle, aimed at -Z
*/
void LightLocator::drawDistantLight( 
    const std::string& i_locationPath )
{
    float theta = 0.0;
    float y_offset = 0.3;
    float x_offset = 0.0;

    for( unsigned i = 0; true; ++i )
    {
        glBegin( GL_POLYGON );

        float x = std::cos(theta);
        float y = std::sin(theta);

        glVertex3f( 0.04 * x + x_offset, 0.04 * y + y_offset, -0.7 );
        glVertex3f( 0.04 * x + x_offset, 0.04 * y + y_offset, 1.0 );
        glVertex3f( -0.04 * x + x_offset, -0.04 * y + y_offset, 1.0 );
        glVertex3f( -0.04 * x + x_offset, -0.04 * y + y_offset, -0.7 );
        glVertex3f( -0.13 * x + x_offset, -0.13 * y + y_offset, -0.7 );
        glVertex3f( x_offset, y_offset, -1.0 );
        glVertex3f( 0.13 * x + x_offset, 0.13 * y + y_offset, -0.7 );
        
        glEnd();

        if( i == 2 )
            break;

        theta += 120 * 2 * M_PI / 360.0;
        x_offset = std::sin( theta ) * -0.4;
        y_offset = -0.3;
    }
}

/*
    Draws a cone with its apex at the origin and its base aimed at -Z.
*/
void LightLocator::drawSpotLight( const std::string& i_locationPath )
{
    /* Fetch cone angle from spotLight shader's parameter. */
    double radius = 0.5;
    FnAttribute::FloatAttribute coneAttr(
        getAttribute( i_locationPath, "material.dlLightParams.coneAngle" ) );

    if( coneAttr.isValid() )
    {
        double angle = coneAttr.getValue( 0.0f );
        angle = std::min( 179.0, std::max( 0.0, angle ) );
        angle = angle * M_PI / 180.0;
        radius = std::tan( angle * 0.5 );
    }

    double length = 1.0;
    /* Shorten wide cones to stay inside bbox. */
    if( radius > 1.0 )
    {
        length /= radius;
        radius = 1.0;
    }

    glBegin( GL_LINE_STRIP );
    int numSegments = 24;
    float delta = ( 2.0 * M_PI ) / numSegments;

    for( int i = 0; i < numSegments; i++ )
    {
        float theta = delta * i;
        float x = std::cos( theta );
        float y = std::sin( theta );

        glVertex3f( x * radius, y * radius, -length );
    }

    glVertex3f( radius, 0, -length );

    glEnd();

    glBegin( GL_LINES );
    glVertex3f( 0.0, 0.0, 0.0 );
    glVertex3f( radius, 0.0, -length );
    glVertex3f( 0.0, 0.0, 0.0 );
    glVertex3f( -radius, 0.0, -length );
    glVertex3f( 0.0, 0.0, 0.0 );
    glVertex3f( 0.0, radius, -length );
    glVertex3f( 0.0, 0.0, 0.0 );
    glVertex3f( 0.0, -radius, -length );
    glEnd();
}

void LightLocator::drawDiskLight( const std::string& i_locationPath )
{
    GLUquadric *q = gluNewQuadric();
    gluQuadricDrawStyle( q, GLU_SILHOUETTE );
    gluDisk( q, 0.0, 0.5, 24, 1 );
    gluDeleteQuadric( q );

    /* Extra line for direction. */
    glBegin( GL_LINE_STRIP );
    glVertex3f( 0.0, 0.0, 0.0 );
    glVertex3f( 0.0, 0.0, -0.5 );
    glEnd();
}

void LightLocator::drawSphereLight( const std::string& i_locationPath )
{
    GLUquadric *q = gluNewQuadric();
    gluSphere( q, 0.5, 18, 9 );
    gluDeleteQuadric( q );
}

void LightLocator::drawPointLight( 
    const std::string& i_locationPath, 
    bool i_drawForPicking,
    bool i_selected )
{
    double radius = 0.5;
    FnAttribute::DoubleAttribute pointRadius(
        getAttribute( i_locationPath, "geometry.pointRadius" ) );

    if( pointRadius.isValid() )
    {
        radius = pointRadius.getValue( 0.0f );
    }

    GLUquadric *q = gluNewQuadric();
    gluSphere( q, radius, 18, 9 );

    /*
        Draw a larger, translucent sphere when the radius is set to a very
        small value (which it is by default). A single pixel locator by
        default is not very useful.
    */
    if( radius < 0.3 )
    {
        if( !i_drawForPicking )
        {
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            setDrawColor( i_selected, 0.3f );
        }
        gluSphere( q, 0.3, 18, 9 );
    }
    gluDeleteQuadric( q );
}

void LightLocator::drawCylinderLight( const std::string& i_locationPath )
{
    glBegin( GL_QUAD_STRIP );

    int numSegments = 18;
    float delta = ( 2.0 * M_PI ) / numSegments;
    float height = 1.0;
    float radius = 0.5;
    for( int i = 0; i < numSegments; i++ )
    {
        float theta = delta * i;
        float x = std::cos( theta );
        float z = std::sin( theta );

        glVertex3f( x * radius, -height / 2.0, z * radius );
        glVertex3f( x * radius, height / 2.0, z * radius );
    }

    glVertex3f( radius, -height / 2.0, 0.0 );
    glVertex3f( radius, height / 2.0, 0.0 );

    glEnd();
}

void LightLocator::drawGoboFilter( const std::string& i_locationPath )
{
    glBegin( GL_POLYGON );
    glVertex3f( -0.75, 0.75, -1.2 );
    glVertex3f( 0.75, 0.75, -1.2 );
    glVertex3f( 0.75, -0.75, -1.2 );
    glVertex3f( -0.75, -0.75, -1.2 );
    glEnd();
}

void LightLocator::setDrawColor( bool i_selected, float i_alpha )
{
    auto shaderProgram = getDefaultShaderProgram();
    assert(shaderProgram);

    if( i_selected )
    {
        // Selected objects draw in white
        shaderProgram->setUniform("Color", 1.0f, 1.0f, 1.0f, i_alpha);
    }
    else
    {
        // Draw light in yellow
        shaderProgram->setUniform("Color", 1.0f, 1.0f, 0.0f, i_alpha);
    }
}
