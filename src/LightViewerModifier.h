/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __LightViewerModifier_h
#define __LightViewerModifier_h

#include <FnViewerModifier/plugin/FnViewerModifier.h>

class LightViewerModifier : public FnKat::ViewerModifier
{
public:
	/* DEFINE_VMP_PLUGIN expects these. */
	static FnKat::ViewerModifier* create( FnKat::GroupAttribute args );
	static FnKat::GroupAttribute getArgumentTemplate();
	static const char* getLocationType();
	static void flush();
	static void onFrameBegin();
	static void onFrameEnd();


	LightViewerModifier( FnKat::GroupAttribute args );
	virtual ~LightViewerModifier();

	/* Required virtual methods. */
	virtual void setup( FnKat::ViewerModifierInput& input );
	virtual void cleanup( FnKat::ViewerModifierInput& input );

	virtual void deepSetup( FnKat::ViewerModifierInput& input );
	virtual void draw( FnKat::ViewerModifierInput& input );
	virtual void deepCleanup( FnKat::ViewerModifierInput& input );

	/* Optional virtual methods. */
	FnKat::DoubleAttribute getLocalSpaceBoundingBox(
		FnKat::ViewerModifierInput& input );

private:
	void drawAreaLight( FnKat::ViewerModifierInput& input );
	void drawDistantLight( FnKat::ViewerModifierInput& input );
	void drawSpotLight( FnKat::ViewerModifierInput& input );
	void drawDiskLight( FnKat::ViewerModifierInput& input );
	void drawSphereLight( FnKat::ViewerModifierInput& input );
	void drawCylinderLight( FnKat::ViewerModifierInput& input );
	void drawPointLight(  FnKat::ViewerModifierInput& );

private:
	enum eType
	{
		type_unknown, type_area, type_distant, type_spot, type_mesh, type_point,
		type_area_disk, type_area_sphere, type_area_cylinder
	};

	eType m_type;
};

#endif
