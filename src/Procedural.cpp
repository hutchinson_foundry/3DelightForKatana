/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "Procedural.h"

#include "ExportContext.h"
#include "ExportMaterial.h"
#include "LogReporting.h"
#include "utils.h"

#include <cstring>

using namespace FnKat;

namespace
{

bool ends_width(const std::string &str, const char *suffix)
{
	size_t l = std::strlen(suffix);
	return
		str.length() >= l &&
		0 == std::memcmp(str.data() + str.length() - l, suffix, l);
}

/*
	Returns true if the specified parameter indicates a shader parameter
	for a "maya" color or float ramp (i.e. metadata widget has value
	"maya_colorRamp" for values: suffix "_Colors"; or metadata widget has value
	"maya_floatRamp" for values: suffix "_Floats"; or metadata related_to_widget
	has value "maya_floatRamp" or "maya_colorRamp" for position:
	suffix "_Knots").
*/ 
bool IsMayaColorOrFloatRamp(
	const DlShaderInfo::Parameter* i_parameter,
	const std::string& i_param_name)
{
	std::string meta_widget =
		Utils::GetMetaDataValue(*i_parameter, "widget");
	std::string meta_related_to_widget =
		Utils::GetMetaDataValue(*i_parameter, "related_to_widget");

	return
		( ( meta_widget == "maya_colorRamp" ) &&
			ends_width(i_param_name, "_Colors") ) ||
		( ( meta_widget == "maya_floatRamp" ) &&
			ends_width(i_param_name, "_Floats") ) ||
		( ( meta_related_to_widget == "maya_floatRamp" ||
			meta_related_to_widget == "maya_colorRamp" ) &&
			ends_width(i_param_name, "_Knots") );
}

int GetArgInt(
	const std::vector<Procedural::shader_arg_t> &i_all_args,
	const std::string &i_name)
{
	for( const auto &arg : i_all_args )
	{
		if( arg.first == i_name )
		{
			FnAttribute::IntAttribute ia{arg.second};
			return ia.getValue(0, false);
		}
	}
	return 0;
}

void ProcessShaderAttribute(
		const DlShaderInfo& i_shader,
		const std::vector<Procedural::shader_arg_t> &i_all_args,
		const std::string& i_param_name,
		const FnAttribute::Attribute& i_attribute,
		NSI::DynamicArgumentList& io_attributes )
{
	const DlShaderInfo::Parameter* parameter =
		Utils::GetParam(i_shader, i_param_name);

	if( !parameter )
	{
		return;
	}

	auto element_type = parameter->type.elementtype();

	/*
		Handle one element or an array of float-based data type.
	*/
	if( element_type == NSITypeColor ||
		element_type == NSITypePoint ||
		element_type == NSITypeNormal ||
		element_type == NSITypeVector ||
		element_type == NSITypeMatrix ||
		element_type == NSITypeFloat )
	{
		FnAttribute::FloatAttribute fa( i_attribute );
		FloatConstVector vector = fa.getNearestSample(0.f);

		size_t element_size = parameter->type.elementsize();
		size_t num_floats_in_element = element_size / sizeof( float );

		// Make sure we at least have one data element in the attribute
		if( vector.size() < num_floats_in_element )
		{
			return;
		}

		NSIType_t nsi_type;

		if( element_type == NSITypeColor )
		{
			nsi_type = NSITypeColor;
		}
		else if( element_type == NSITypePoint )
		{
			nsi_type = NSITypePoint;
		}
		else if( element_type == NSITypeVector )
		{
			nsi_type = NSITypeVector;
		}
		else if( element_type == NSITypeNormal )
		{
			nsi_type = NSITypeNormal;
		}
		else if( element_type == NSITypeMatrix )
		{
			nsi_type = NSITypeMatrix;
		}
		else
		{
			nsi_type = NSITypeFloat;
		}

		NSI::Argument* arg = new NSI::Argument( parameter->name.c_str() );
		size_t num_elements = 0;

		if( parameter->type.is_array() )
		{
			if( IsMayaColorOrFloatRamp(parameter, i_param_name) ) 
			{
				// Removed first and last duplicate position/value added
				// in RendererInfo::BuildSingleShaderParameter (color or float
				// ramp widget needs duplicate for first and last
				// position/value, but not the maya shader)
				FloatAttribute fa2(
					(const float*)&vector.data()[num_floats_in_element],
					vector.size()-2*num_floats_in_element, 1);
				FloatConstVector vector2 = fa2.getNearestSample(0.f);
				num_elements = vector2.size() / num_floats_in_element;
				arg->SetArrayType( nsi_type, num_elements );
				arg->CopyValue( vector2.data(), num_elements * element_size );
				io_attributes.Add( arg );
				return;
			}

			num_elements = vector.size() / num_floats_in_element;
			arg->SetArrayType( nsi_type, num_elements );
		}
		else
		{
			num_elements = 1;
			arg->SetType( nsi_type );
		}

		arg->CopyValue( vector.data(), num_elements * element_size );
		io_attributes.Add( arg );
	}
	else if( parameter->type == NSITypeInteger )
	{
		FnAttribute::IntAttribute ia( i_attribute );
		io_attributes.Add( new NSI::IntegerArg( 
			parameter->name.c_str(), ia.getValue( 0, 0 ) ) );
	}
	else if( parameter->type == NSITypeString )
	{
		FnAttribute::StringAttribute sa( i_attribute );
		io_attributes.Add(
			new NSI::StringArg( parameter->name.c_str(), sa.getValue() ) );
		/* Look for the metadata which tells us this is a texture. */
		for( const auto &m : parameter->metadata )
		{
			if( m.name == "texturefile" &&
			    m.type == NSITypeInteger &&
			    m.idefault[0] == 1 )
			{
				/* We don't have UI to set the color space yet. */
				io_attributes.Add( new NSI::StringArg(
					parameter->name.string() + ".meta.colorspace", "auto" ) );
			}
		}
	}
	else if( Utils::IsOSLTypeIntArray( parameter->type ) )
	{
		std::string meta_related_to_widget =
			Utils::GetMetaDataValue(*parameter, "related_to_widget");
		if( meta_related_to_widget == "floatRamp" ||
			meta_related_to_widget == "maya_floatRamp" ||
			meta_related_to_widget == "colorRamp" ||
			meta_related_to_widget == "maya_colorRamp" )
		{
			/* Katana uses a single string value internally for interpolation.
			   The shader expects one integer per data point. So convert it. */
			FnAttribute::StringAttribute sa( i_attribute );
			int value = Utils::RampInterpolationStr2Int(sa.getValue());

			/* Get the required count from the base arg. */
			std::string base_name{
				i_param_name, 0, i_param_name.find_last_of("_")};
			int nv = GetArgInt(i_all_args, base_name);
			/* Remove the two duplicate control points used by Katana. */
			nv = std::max(nv, 3) - 2;

			NSI::Argument* arg = new NSI::Argument( parameter->name.c_str() );
			arg->SetArrayType( NSITypeInteger, nv );
			std::fill_n((int*)arg->AllocValue(nv * sizeof(int)), nv, value);
			io_attributes.Add( arg );
		}
	}
}

/*
	Add scene graph location and "|network|" string as prefix to name.

	The purpose of this is so the same network material can be copied to
	several scene graph locations, altered, and then exported without the nodes
	of the network colliding with each other. This is what happens with
	material overrides.
*/ 
std::string AddFullPrefixPath(
	const SGLocation &i_location,
	const std::string& i_name)
{
	return i_location.getFullName() + "|network|" + i_name;
}

const DlShaderInfo* CreateVirtualShadingNode(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const std::string& i_node_type,
		const std::string &i_node_name,
		std::vector<std::string>& io_created_nodes_names)
{
	const DlShaderInfo& osl_shader =
		Utils::GetOSLShaders().getShaderInfo(i_node_type);

	std::vector<std::string>::const_iterator found =
		std::find(
				io_created_nodes_names.begin(),
				io_created_nodes_names.end(),
				i_node_name);
	if( found != io_created_nodes_names.end() )
	{
		/* we've already built it */
		return &osl_shader;
	}

	io_created_nodes_names.push_back(i_node_name);

	NSI::DynamicArgumentList attributes;
	/* Add the shader name. */
	{
		std::string shader = Utils::GetOSLDirectory();
		shader += "/";
		shader += i_node_type;

		attributes.Add(
			new NSI::StringArg( "shaderfilename", shader ) );
	}

	/* Output everything */
	ExportMaterial::CreateShader(
		i_ctx, AddFullPrefixPath(i_location, i_node_name), attributes );

	return &osl_shader;
}

} /* namespace */

void Procedural::ProcessAllShaderAttributes(
	const DlShaderInfo& i_shader,
	const std::vector<shader_arg_t> &i_args,
	NSI::DynamicArgumentList& io_attributes )
{
	for( const shader_arg_t &arg : i_args )
	{
		ProcessShaderAttribute(
			i_shader, i_args, arg.first, arg.second, io_attributes);
	}
}

/*
	Check if a given osl shader parameter has a "default connection" metadata;
	if it does, create a virtual shading node and define connections between
	it and i_shader_handle.

	This code was part of ProcessShadingNode and was moved in a separate
	function so we can use it when handling Material nodes too.
*/ 
void Procedural::ProcessDefaultConnection(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	const DlShaderInfo& i_osl_shader,
	const DlShaderInfo::Parameter* i_osl_param,
	const std::string& i_shader_handle,
	std::vector<std::string>& io_createdNodeNames )
{
	if( i_osl_param == NULL )
	{
		return;
	}

	/* Get metadata. */
	for (int i = 0; i < i_osl_param->metadata.size(); i++)
	{
		const DlShaderInfo::Parameter& meta = i_osl_param->metadata[ i ];

		if( meta.name == "default_connection" )
		{
			if( meta.type == NSITypeString && meta.sdefault.size() == 1 )
			{
				/* Fetch the node type for the default connection. */
				const std::string default_connection = meta.sdefault[0].c_str();
				/* Generate a hopefully unique name for the node. */
				std::string virtual_node_name = default_connection + " virtual";

				const DlShaderInfo* osl_shader_from =
					CreateVirtualShadingNode(
						i_ctx,
						i_location,
						default_connection,
						virtual_node_name,
						io_createdNodeNames );

				if(!osl_shader_from)
				{
					break;
				}

				const DlShaderInfo::Parameter* port_from = NULL;

				/* Get first output parameter of the same type */
				for(unsigned int k=0; k<osl_shader_from->nparams(); k++)
				{
					const DlShaderInfo::Parameter* pp =
						osl_shader_from->getparam(k);

					if(pp && pp->isoutput && i_osl_param->type == pp->type)
					{
						port_from = pp;
					}
				}

				if(!port_from)
				{
					break;
				}

				i_ctx.m_nsi.Connect(
					AddFullPrefixPath(i_location, virtual_node_name),
					port_from->name.c_str(),
					i_shader_handle,
					i_osl_param->name.c_str() );

				break;
			}
		}
	}
}

namespace
{
/*
	Parse the element index suffix in a connection's name. This is added to
	network materials by using the ShadingNodeArrayConnector.

	In Katana, the connected attribute name will have the form 'something:3'
	which we want to turn into 'something[3]' for the renderer. Note that this
	could also be 'someotherthing[3]' if the parameter is remapped with metadata
	in the shader.
*/
void ParseConnectionName(
	const std::string &i_connectionName,
	std::string &o_paramName,
	std::string &o_paramSuffix)
{
	auto p = i_connectionName.find_last_of(":");
	if( p == std::string::npos )
	{
		o_paramName = i_connectionName;
		o_paramSuffix.clear();
	}
	else
	{
		o_paramName = i_connectionName.substr(0, p);
		o_paramSuffix = "[" + i_connectionName.substr(p + 1) + "]";
	}
}

const DlShaderInfo* ProcessShadingNode(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		RenderOutputUtils::ShadingNodeDescriptionMap& nodeMap,
		const std::string& name,
		std::vector<std::string>& createdNodeNames,
		float& io_bounding_sphere )
{
	RenderOutputUtils::ShadingNodeDescription node =
		nodeMap.getShadingNodeDescriptionByName( name );

	if( !node.isValid() )
	{
		/* TODO, error? */
		return NULL;
	}

	std::string nodetype = node.getType();
	const DlShaderInfo& osl_shader =
		Utils::GetOSLShaders().getShaderInfo(nodetype);

	std::vector<std::string>::const_iterator found =
		std::find(createdNodeNames.begin(), createdNodeNames.end(), name);
	if( found != createdNodeNames.end() )
	{
		/* we've already built it */
		return &osl_shader;
	}

	createdNodeNames.push_back(name);

	NSI::DynamicArgumentList attributes;

	/* Add the shader name. */
	{
		std::string shader = Utils::GetOSLDirectory();
		shader += "/";
		shader += nodetype;

		attributes.Add( new NSI::StringArg( "shaderfilename", shader ) );
	}

	/* Process parameters. */
	std::vector<Procedural::shader_arg_t> args;
	args.reserve(node.getNumberOfParameterNames());
	for (int i = 0, e = node.getNumberOfParameterNames(); i < e; ++i)
	{
		std::string paramName = node.getParameterName(i);
		args.emplace_back(paramName, node.getParameter(paramName));
	}
	Procedural::ProcessAllShaderAttributes(osl_shader, args, attributes);

	/* Output everything */
	ExportMaterial::CreateShader(
		i_ctx, AddFullPrefixPath(i_location, node.getName()), attributes );

	/* Trying to guess the displacement bounding sphere. */
	if( nodetype == "displacementShader" )
	{
		/* If scale is default we will not be able to get the attribute.
		   it will not be valid. */
		const float default_scale = 1.0f;

		/* Get the "scale" parameter. */
		FnAttribute::FloatAttribute attr = node.getParameter("scale");
		if( attr.isValid() )
		{
			io_bounding_sphere =
				std::max(io_bounding_sphere, attr.getValue(default_scale));
		}
		else
		{
			io_bounding_sphere =
				std::max(io_bounding_sphere, default_scale);
		}
	}

	std::set<DlShaderInfo::conststring> processed_parameters;

	/* Process connections. */
	for (int i = 0, e = node.getNumberOfConnectionNames(); i < e; ++i)
	{
		std::string connectionName = node.getConnectionName(i);

		RenderOutputUtils::ShadingNodeConnectionDescription connection =
			node.getConnection(connectionName);

		if (connection.isValid())
		{
			std::string connectedNodeName =
				connection.getConnectedNodeName();

			if (!connectedNodeName.empty())
			{
				const DlShaderInfo* osl_shader_from =
					ProcessShadingNode(
							i_ctx,
							i_location,
							nodeMap,
							connectedNodeName,
							createdNodeNames,
							io_bounding_sphere);

				/* Get the OSL parameter of the "from" shader. We need it to get
				   the correct name */
				if( !osl_shader_from )
				{
					continue;
				}

				const DlShaderInfo::Parameter* port_from =
					Utils::GetParam(
							*osl_shader_from,
							connection.getConnectedPortName() );

				std::string destParamName, destParamSuffix;
				ParseConnectionName(
					connectionName, destParamName, destParamSuffix);

				const DlShaderInfo::Parameter* port_to =
					Utils::GetParam(osl_shader, destParamName);

				if( !port_from || !port_to )
				{
					continue;
				}

				i_ctx.m_nsi.Connect(
					AddFullPrefixPath(i_location, connectedNodeName),
					port_from->name.c_str(),
					AddFullPrefixPath(i_location, name),
					port_to->name.string() + destParamSuffix );

				processed_parameters.insert(port_to->name);
			}
		}
	}

	for( int i=0; i<osl_shader.nparams(); i++ )
	{
		const DlShaderInfo::Parameter* p = osl_shader.getparam(i);

		if(processed_parameters.find(p->name) != processed_parameters.end())
		{
			/* it's already connected */
			continue;
		}

		Procedural::ProcessDefaultConnection(
			i_ctx,
            i_location,
			osl_shader,
			p,
			AddFullPrefixPath( i_location, name ),
			createdNodeNames );
	}

	return &osl_shader;
}
} /* namespace */

void Procedural::WriteNSI_Shading_Network(
	ExportContext &i_ctx,
	const SGLocation &i_sgIterator,
	const std::string &i_material_group )
{
	// Write out current material settings
	FnAttribute::GroupAttribute material_attr =
		i_sgIterator.getAttribute( i_material_group.c_str() );
	if (!material_attr.isValid())
	{
		return;
	}

	FnAttribute::GroupAttribute terminals_attr =
		material_attr.getChildByName( "terminals" );
	if( !terminals_attr.isValid() )
	{
		/* Try to output it as a single node */
		ExportMaterial::ExportOneShader(
			i_ctx, i_sgIterator, i_material_group,
			_3DELIGHT_LIGHT_TERMINAL, "" );
		ExportMaterial::ExportOneShader(
			i_ctx, i_sgIterator, i_material_group,
			_3DELIGHT_SURFACE_TERMINAL, "" );
		ExportMaterial::ExportOneShader(
			i_ctx, i_sgIterator, i_material_group,
			_3DELIGHT_DISPLACEMENT_TERMINAL, "" );
		ExportMaterial::ExportOneShader(
			i_ctx, i_sgIterator, i_material_group,
			_3DELIGHT_ENVIRONMENT_TERMINAL, "" );
		ExportMaterial::ExportOneShader(
			i_ctx, i_sgIterator, i_material_group,
			_3DELIGHT_VOLUME_TERMINAL, "" );
		return;
	}

	RenderOutputUtils::ShadingNodeDescriptionMap nodeMap( material_attr );
	std::vector<std::string> createdNodeNames;

	for( int i=0, e=terminals_attr.getNumberOfChildren(); i<e; ++i )
	{
		std::string terminalName = terminals_attr.getChildName( i );
		if( terminalName.find(_3DELIGHT_RENDERER_NAME) != 0 )
		{
			continue;
		}

		std::string terminalShortName(
				terminalName, strlen(_3DELIGHT_RENDERER_NAME));

		if( terminalShortName != _3DELIGHT_SURFACE_TERMINAL &&
			terminalShortName != _3DELIGHT_DISPLACEMENT_TERMINAL &&
			terminalShortName != _3DELIGHT_LIGHT_TERMINAL &&
			terminalShortName != _3DELIGHT_ENVIRONMENT_TERMINAL &&
			terminalShortName != _3DELIGHT_VOLUME_TERMINAL )
		{
			continue;
		}

		FnAttribute::StringAttribute terminalAttr =
			terminals_attr.getChildByIndex( i );
		if ( !terminalAttr.isValid() )
		{
			continue;
		}

		FnAttribute::StringAttribute terminalPortAttr =
			terminals_attr.getChildByName( terminalName + "Port" );
		if ( !terminalPortAttr.isValid() )
		{
			continue;
		}

		std::string terminalNodeName = terminalAttr.getValue();
		std::string terminalPortName = terminalPortAttr.getValue();

		std::string shader_name =
			i_sgIterator.getFullName() + "|" + terminalName;

		float bounding_sphere = -1.0f;

		const DlShaderInfo* osl_shader_from =
			ProcessShadingNode(
				i_ctx, i_sgIterator,
				nodeMap, terminalNodeName, createdNodeNames, bounding_sphere );

		if( !osl_shader_from )
		{
			continue;
		}

		if( bounding_sphere > 0.0f )
		{
			Utils::SetBoundingSphereCache(shader_name, bounding_sphere);
		}

		/* Attributes */
		NSI::DynamicArgumentList attributes;
		{
			std::string shader = Utils::GetOSLDirectory();
			shader += "/NetworkMaterial_" + terminalShortName;

			attributes.Add( new NSI::StringArg( "shaderfilename", shader ) );
		}

		/* Output this root shader */
		ExportMaterial::CreateShader( i_ctx, shader_name, attributes );

		const DlShaderInfo::Parameter* port_from =
			Utils::GetParam(
					*osl_shader_from,
					terminalPortName );

		if( !port_from )
		{
			continue;
		}

		/* Connections */
		i_ctx.m_nsi.Connect(
				AddFullPrefixPath(i_sgIterator, terminalNodeName),
				port_from->name.c_str(),
				shader_name.c_str(),
				"i_input" );
	}
}
