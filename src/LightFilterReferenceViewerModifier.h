/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __LightFilterReferenceViewerModifier_h
#define __LightFilterReferenceViewerModifier_h

#include "LightFilterViewerModifier.h"

class LightFilterReferenceViewerModifier : public LightFilterViewerModifier
{
public:
	/* DEFINE_VMP_PLUGIN expects these. */
	static FnKat::ViewerModifier* create( FnKat::GroupAttribute args );
	static const char* getLocationType();


	LightFilterReferenceViewerModifier( FnKat::GroupAttribute args );

	/* Required virtual methods. */
	virtual void setup( FnKat::ViewerModifierInput& input );
};

#endif
