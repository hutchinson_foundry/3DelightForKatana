/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __rendersettings_h_
#define __rendersettings_h_

#include <FnRender/plugin/RenderSettings.h>
#include <FnRender/plugin/IdSenderInterface.h>

#include <string>
#include <map>

class SGLocation;

namespace FnKatRender = Foundry::Katana::Render;

/* Class that provides access to the renderer global parameters */
class RenderSettings : public FnKatRender::RenderSettings
{
	public:
		RenderSettings(FnKat::FnScenegraphIterator rootIterator);
		virtual ~RenderSettings() {}

		/* Reads all the necessary parameters from the attribute and saves them
		 * for future use. */
		virtual int initialise();

		void ParseSettings( const SGLocation &i_location );

		/* Update the render camera name, return the old one. */
		std::string UpdateCamera( const std::string &i_camera )
		{
			std::string old = _cameraName;
			_cameraName = i_camera;
			return old;
		}

		/* Returns the screen window. */
		void getScreenWindow( double o_screenWindow[4] ) const
		{
			o_screenWindow[0] = m_screenWindow[0];
			o_screenWindow[1] = m_screenWindow[1];
			o_screenWindow[2] = m_screenWindow[2];
			o_screenWindow[3] = m_screenWindow[3];
		}

		/* Updates the screen window. */
		void UpdateScreenWindow( double i_screenWindow[4] )
		{
			m_screenWindow[0] = i_screenWindow[0];
			m_screenWindow[1] = i_screenWindow[1];
			m_screenWindow[2] = i_screenWindow[2];
			m_screenWindow[3] = i_screenWindow[3];
		}

		/* Returns the crop window without the ROI applied to it. */
		void getRealCropWindow( float o_cropWindow[4] ) const
		{
			o_cropWindow[0] = _cropWindow[0];
			o_cropWindow[1] = _cropWindow[1];
			o_cropWindow[2] = _cropWindow[2];
			o_cropWindow[3] = _cropWindow[3];
		}

		/*
			Returns the Region of Interest. Also returns true if it is valid,
			false if there is no ROI.
		*/
		bool getROI( int o_ROI[4] ) const
		{
			o_ROI[0] = m_real_ROI[0];
			o_ROI[1] = m_real_ROI[1];
			o_ROI[2] = m_real_ROI[2];
			o_ROI[3] = m_real_ROI[3];
			/* No ROI if width or height are 0. */
			return _regionOfInterest[2] != 0 && _regionOfInterest[3] != 0;
		}

		void UpdateROI( const FnAttribute::IntAttribute &i_ROI_attr );

		void ComputeRenderResolution( bool i_interactive, int o_res[2] ) const;

		int GetShadingSamples() const
		{
			return m_shading_samples;
		}

		int GetPixelSamples() const
		{
			return m_pixel_samples;
		}

		int GetVolumeSamples() const
		{
			return m_volume_samples;
		}

		const std::string& GetPixelFilter() const
		{
			return m_pixel_filter;
		}

		float GetFilterWidth() const
		{
			return m_filter_width;
		}

		int GetDiffuseDepth() const
		{
			return m_diffuse_depth;
		}

		int GetReflectionDepth() const
		{
			return m_reflection_depth;
		}

		int GetRefractionDepth() const
		{
			return m_refraction_depth;
		}

		int GetHairDepth() const
		{
			return m_hair_depth;
		}

		float GetMaxRayLength() const
		{
			return m_max_ray_length;
		}

		int GetLayersBatch()const
		{
			return m_layers_batch;
		}

		int GetLayersInteractive()const
		{
			return m_layers_interactive;
		}

		int GetDisableMotionBlur()const
		{
			return m_disable_motion_blur;
		}

		int GetDisableDepthOfField()const
		{
			return m_disable_depth_of_field;
		}

		int GetEnableDisplacement()const
		{
			return m_disable_displacement == 0 ? 1 : 0;
		}

		int GetEnableSubsurface()const
		{
			return m_disable_subsurface == 0 ? 1 : 0;
		}

		float GetSamplingReduceFactor()const
		{
			return m_sampling_reduce_factor;
		}

		void SetFStop(float i_fstop)
		{
			m_fstop = i_fstop;
		}

		float GetFStop()const
		{
			return m_fstop;
		}

		void InitIDSender(const std::string& host, int64_t frameID);

		int GetObjectID(const char* const objectName)const;

		void GetMotionSampleOffsets(std::vector<double>& o_offsets) const;
		
	protected:
		int m_shading_samples;
		int m_pixel_samples;
		int m_volume_samples;
		std::string m_pixel_filter;
		float m_filter_width;
		int m_diffuse_depth;
		int m_reflection_depth;
		int m_refraction_depth;
		int m_hair_depth;
		float m_max_ray_length;
		int m_resolution_multiplier;
		int m_layers_batch;
		int m_layers_interactive;
		int m_disable_motion_blur;
		int m_disable_depth_of_field;
		int m_disable_displacement;
		int m_disable_subsurface;
		int m_resolution_reduce_factor;
		float m_sampling_reduce_factor;
		/*
			fstop of the camera; used by ExportOptions of ExportCamera.
		*/
		float m_fstop;
		/*
			Region of Interest, directly as provided by katana. The
			_regionOfInterest member in the parent class has a different
			representation which does not match live render updates.
		*/
		int m_real_ROI[4];
		/*
			Screen window of the scene camera. We keep here because the
			CameraSettings does not update screen window in live render.
		*/
		double m_screenWindow[4];

		static int64_t m_next_id;
		FnKatRender::IdSenderInterface* m_id_sender;
};

#endif /* __rendersettings_h_ */

