/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportCamera_h
#define __ExportCamera_h

#include "SGLocation.h"

class ExportContext;

/*
	This namespace is for code which handles exporting cameras.
*/
namespace ExportCamera
{
	void ExportCamera(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportScreen(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportPriorityWindow(
		ExportContext &i_ctx,
		const std::string &i_screen_handle );

	void ExportOptions(	ExportContext &i_ctx );
}

#endif

