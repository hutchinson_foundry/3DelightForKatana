/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportGeo.h"

#include "AttributeUtils.h"
#include "ExportAttributes.h"
#include "ExportContext.h"
#include "ExportInstance.h"
#include "ExportSG.h"
#include "FnAttribute/FnAttributeUtils.h"
#include "XGenLoader.h"
#include "XGenNSICallbacks.h"

#include <assert.h>

#include <algorithm>
#include <sstream>

using AttributeUtils::GetString;
using AttributeUtils::GetDouble;
using AttributeUtils::GetInteger;

namespace ExportGeo
{

void ExportID(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	int id = i_ctx.m_renderSettings.GetObjectID(
			i_location.getFullName().c_str() );

	/* We get -1 if there is no ID pass. */
	if( id == -1 )
		return;

	i_ctx.m_nsi.SetAttribute(
		i_location.getFullName() + "|leaf",
		NSI::IntegerArg( "__id", id ) );
}

void ExportFaceSet(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	/*
		TODO: We'll need to do something to avoid exporting face sets on ID
		based instances, except on the master where the geo is actually
		exported. A possible solution is to:
		- Add an OpScript to duplicate instance ID on face sets.
		- Look it up in here. If present, look in our instance registry to find
		  the master geo and compare its path with ours to see if it is our
		  parent.
	*/

	/*
		The face set is exported as "/path" just like a transform so attributes
		export can handle them the same way. It would match geometry better to
		use "/path|leaf" but we'd have no "/path" to connect attributes to.
	*/
	std::string faceset_name = i_location.getFullName();
	std::string geo_name = i_location.getParentFullName() + "|leaf";

	if( i_location.IsDeleted() )
	{
		/* Live update deletion. */
		i_ctx.m_nsi.Delete( faceset_name );
		return;
	}

	/* Create the node first so we can export attributes. */
	i_ctx.m_nsi.Create( faceset_name, "faceset" );
	i_ctx.m_nsi.Connect( faceset_name, "", geo_name, "facesets" );

	/*
		These come with separate updates so don't bother trying to export
		them in a live update.
	*/
	if( !i_location.IsLiveUpdate() )
	{
		ExportSG::ExportLocalMaterialAndAttributes( i_ctx, i_location );
	}

	/* Export the face list. */
	FnAttribute::IntAttribute faces_attr(
		i_location.getAttribute( "geometry.faces" ) );

	if( !faces_attr.isValid() )
		return;

	FnAttribute::IntAttribute::array_type faces_data =
		faces_attr.getNearestSample( 0.0f );

	i_ctx.EvictionManager().CountData( faces_data.size() * sizeof(int) );
	i_ctx.m_nsi.SetAttribute(
		faceset_name,
		*NSI::Argument( "faces" )
			.SetType( NSITypeInteger )
			->SetCount( faces_data.size() )
			->SetValuePointer( faces_data.data() ) );
}

void ExportPointCloud(
	ExportContext &i_context,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_context, i_location );

	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	if( i_location.IsDeleted() )
	{
		i_context.m_nsi.Delete( geo_name );
		return;
	}

	FnAttribute::GroupAttribute geometry( i_location.getAttribute("geometry") );

	if( !geometry.isValid() )
		return;

	i_context.m_nsi.Create( geo_name, "particles" );
	i_context.m_nsi.Connect( geo_name, "", xform_name, "objects" );

	FnAttribute::GroupAttribute point_group =
		geometry.getChildByName("point");

	FnAttribute::Attribute P = point_group.getChildByName("P");

	if( !P.isValid() )
		return;


	ExportSG::ExportFloatAttribute(
		i_context,
		P,
		geo_name.c_str(),
		"P",
		NSITypePoint,
		0,
		0 );

	FnAttribute::Attribute width = point_group.getChildByName("width");
	if( !width.isValid() )
	{
		width = point_group.getChildByName("constantwidth");
	}

	if( width.isValid() )
	{
		ExportSG::ExportFloatAttribute(
			i_context,
			width,
			geo_name.c_str(),
			"width",
			NSITypeFloat,
			0,
			0 );
	}
	else
	{
		printf(
			"'%s' has no valid 'width' attribute, will default to 0.1\n",
			i_location.getFullName().c_str() );

		/* Use an arbitrary constant point width */
		i_context.m_nsi.SetAttribute(
			geo_name, NSI::FloatArg( "width", 0.1f));
	}

	ExportArbitraryValues( i_context, i_location, {} );
}

/**
	\brief Export a bunch of curves

	We support linear and cubic curves. In the cubic case, we support either
	b-spline or catmnull. Support for  other basis would need support from
	3Delight/NSI.
	Know vectors are not even looked at. But people don't understand knot
	vectors, so they are mostly 0s and 1s ... at least in Alembic :)

	Live render notes:

	- Can't switch between cubic and linear in live. To do so would mean to
	  delete and re-create the node. Can be done if needed.
*/
void ExportCurves(
	ExportContext &i_context,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_context, i_location );

	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	if( i_location.IsDeleted() )
	{
		i_context.m_nsi.Delete( geo_name );
		return;
	}

	FnAttribute::GroupAttribute geometry( i_location.getAttribute("geometry") );

	if( !geometry.isValid() )
		return;

	int degree = 1;
	GetInteger( i_location, "geometry.degree", degree );
	FnAttribute::FloatAttribute constantwidth =
		geometry.getChildByName("constantWidth");

	i_context.m_nsi.Create( geo_name, "curves" );
	if( degree == 1 )
	{
		i_context.m_nsi.SetAttribute(
			geo_name, NSI::StringArg("basis", "linear") );
	}
	else if( degree == 3 )
	{
		/* Would have been a nice touch to have this given to us by Katana ...*/
		int basis_number = 0;
		GetInteger( i_location, "geometry.basis", basis_number );

		const char *basis = nullptr;
		switch( basis_number )
		{
		case 1: basis = "bezier"; break;
		case 2: basis = "b-spline"; break;
		case 3: basis = "catmull-rom"; break;
		case 4: basis = "hermite"; break;
		case 5: basis = "power"; break;
		default:
		case 0: basis = "none"; break;
		}

		if( basis_number!=2 && basis_number!=3 )
		{
			printf(
				"'%s' curve has unsupported basis: '%s'\n",
				i_location.getFullName().c_str(), basis );
			return;
		}

		i_context.m_nsi.SetAttribute( geo_name, (
			NSI::StringArg("basis", basis),
			NSI::IntegerArg("extrapolate", 1)) );
	}
	else
	{
		return;
	}

	i_context.m_nsi.Connect( geo_name, "", xform_name, "objects" );

	FnAttribute::GroupAttribute point_group =
		geometry.getChildByName("point");

	FnAttribute::Attribute P = point_group.getChildByName("P");
	FnAttribute::Attribute N = point_group.getChildByName("orientation");
	FnAttribute::Attribute width = point_group.getChildByName("width");

    FnAttribute::IntAttribute numVertices =
		geometry.getChildByName("numVertices");

	if( !P.isValid() || !numVertices.isValid() )
		return;

	FnAttribute::IntAttribute::array_type nvertices =
		numVertices.getNearestSample( 0.0f );

	i_context.m_nsi.SetAttribute(
		geo_name,
		*NSI::Argument( "nvertices" )
			.SetType( NSITypeInteger )
			->SetCount( nvertices.size() )
			->SetValuePointer( &nvertices[0]) );

	ExportSG::ExportFloatAttribute(
		i_context,
		P,
		geo_name.c_str(),
		"P",
		NSITypePoint,
		0,
		0 );

	ExportSG::ExportFloatAttribute(
		i_context,
		N,
		geo_name.c_str(),
		"N",
		NSITypeNormal,
		0,
		0 );

	/*
		In Katana's API documentation, it is said that width has higher
		priority than constantwidth.
	*/
	ExportSG::ExportFloatAttribute(
		i_context,
		width.isValid() ? width : constantwidth,
		geo_name.c_str(),
		"width",
		NSITypeFloat,
		0,
		0 );

	/* Arbitrary values. */
	ExportArbitraryValues( i_context, i_location, {} );

	/* Alembic user attributes. */
	ExportAlembicUserAttributes( i_context, i_location );
}

/**
	\brief Export a polymesh location.

	\returns
		true if we actually exported a geometry node. It does not mean
		everything went well, just that the node exists.

	The reason we return a status here is that polymesh export is a also a part
	of the subdivision mesh export.
*/
bool ExportPolymesh(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_ctx, i_location );

	/* Handle ID based instancing. */
	if( ExportInstance::ExportIDInstance( i_ctx, i_location, "mesh" ) )
		return false;

	/* We'll create a "/path|leaf" node for the geometry itself. */
	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	if( i_location.IsDeleted() )
	{
		/* Live update deletion. */
		i_ctx.m_nsi.Delete( geo_name );
		return false;
	}

	/*
		Don't output the mesh at all if there is no geometry group. This check
		is for the case of a mesh light with no geo but children instead.
	*/
	FnAttribute::GroupAttribute geo_attr(
		i_location.getAttribute( "geometry" ) );
	if( !geo_attr.isValid() )
		return false;

	i_ctx.m_nsi.Create( geo_name, "mesh" );
	i_ctx.m_nsi.Connect( geo_name, "", xform_name, "objects" );

	ExportID( i_ctx, i_location );

	if( i_location.IsLiveUpdate() )
	{
		/* Delete subdivision scheme to handle subdiv -> polymesh change. */
		i_ctx.m_nsi.DeleteAttribute( geo_name, "subdivision.scheme" );
		/* Also delete 'N' to handle polymesh -> subdiv change. */
		i_ctx.m_nsi.DeleteAttribute( geo_name, "N" );
	}

	FnAttribute::GroupAttribute point_attr(
		geo_attr.getChildByName( "point" ) );
	FnAttribute::Attribute P_attr(
		point_attr.getChildByName( "P" ) );

	FnAttribute::GroupAttribute poly_attr(
		geo_attr.getChildByName( "poly" ) );
	FnAttribute::IntAttribute startIndex_attr(
		poly_attr.getChildByName( "startIndex" ) );
	FnAttribute::IntAttribute vertexList_attr(
		poly_attr.getChildByName( "vertexList" ) );

	if(
		!P_attr.isValid() ||
		!startIndex_attr.isValid() ||
		!vertexList_attr.isValid() )
	{
		/*
			TODO: Some kind of error message? Although we'll get one at
			render time so it may not be relevant.
		*/
		return true;
	}

	FnAttribute::IntAttribute::array_type startIndex_data =
		startIndex_attr.getNearestSample( 0.0f );

	FnAttribute::IntAttribute::array_type vertexList_data =
		vertexList_attr.getNearestSample( 0.0f );

	std::vector<int> nvertices, Pindices;
	nvertices.reserve( startIndex_data.size() - 1u );
	Pindices.reserve( vertexList_data.size() );

	for( unsigned i = 0; i < startIndex_data.size() - 1u; ++i )
	{
		int begin = startIndex_data[i];
		int end = startIndex_data[i + 1u];
		if( begin == end )
			continue;

		nvertices.push_back( end - begin );
		for( int j = begin; j < end; ++j )
		{
			assert( j < vertexList_data.size() );
			Pindices.push_back( vertexList_data[j] );
		}
	}

	/*
		Katana appears to use clockwise winding despite its right handed
		camera. Go figure.
	*/
	i_ctx.m_nsi.SetAttribute(
		geo_name, NSI::IntegerArg( "clockwisewinding", 1 ) );

	i_ctx.m_nsi.SetAttribute(
		geo_name,
		*NSI::Argument( "nvertices" )
			.SetType( NSITypeInteger )
			->SetCount( nvertices.size() )
			->SetValuePointer( &nvertices[0] ) );

	i_ctx.EvictionManager().CountData( Pindices.size() * sizeof(int) );
	i_ctx.m_nsi.SetAttribute(
		geo_name,
		*NSI::Argument( "P.indices" )
			.SetType( NSITypeInteger )
			->SetCount( Pindices.size() )
			->SetValuePointer( &Pindices[0] ) );

	ExportSG::ExportFloatAttribute(
		i_ctx,
		P_attr,
		geo_name.c_str(),
		"P",
		NSITypePoint,
		0,
		0 );


	/* Don't export N for subdivs. It may be present for polygon meshes
	   converted to subdivs and is quite likely to cause bad shading. */
	if( i_location.getType() != "subdmesh" )
	{
		FnAttribute::Attribute pointN_attr(
			point_attr.getChildByName( "N" ) );

		if( pointN_attr.isValid())
		{
			i_ctx.EvictionManager().CountData( Pindices.size() * sizeof(int) );
			i_ctx.m_nsi.SetAttribute(
				geo_name,
				*NSI::Argument( "N.indices" )
					.SetType( NSITypeInteger )
					->SetCount( Pindices.size() )
					->SetValuePointer( &Pindices[0] ) );

			ExportSG::ExportFloatAttribute(
				i_ctx,
				pointN_attr,
				geo_name.c_str(),
				"N",
				NSITypeNormal,
				0,
				0 );
		}
		else
		{
			FnAttribute::Attribute vertexN_attr(
				geo_attr.getChildByName( "vertex.N" ) );

			if( vertexN_attr.isValid() )
			{
				ExportSG::ExportFloatAttribute(
					i_ctx,
					vertexN_attr,
					geo_name.c_str(),
					"N",
					NSITypeNormal, 0, 0 );
			}
		}
	}

	/*
		The RenderMan code also does Pref and Nref, stored the same as P and N.
		There's no reference to that in the katana docs but we should probably
		do it eventually. We'll have to see that it integrates in a useful way
		with shading nodes.
	*/

	/* Arbitrary values (eg. "st"). */
	ExportArbitraryValues( i_ctx, i_location, vertexList_attr );

	/* Alembic user attributes. */
	ExportAlembicUserAttributes( i_ctx, i_location );

	return true;
}

void ExportSubdivmesh(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	/* Start with a polymesh export as subdiv data is a superset of polymesh. */
	if( !ExportPolymesh( i_ctx, i_location ) )
		return;

	std::string geo_name = i_location.getFullName() + "|leaf";

	/* Make it a subdiv :) */
	i_ctx.m_nsi.SetAttribute(
		geo_name, NSI::CStringPArg( "subdivision.scheme", "catmull-clark" ) );

	/* Export optional corners. */
	FnAttribute::IntAttribute cornerIndices_attr(
		i_location.getAttribute( "geometry.cornerIndices" ) );
	FnAttribute::FloatAttribute cornerSharpness_attr(
		i_location.getAttribute( "geometry.cornerSharpness" ) );

	if( cornerIndices_attr.isValid() && cornerSharpness_attr.isValid() )
	{
		FnAttribute::IntAttribute::array_type cornerIndices_data =
			cornerIndices_attr.getNearestSample( 0.0f );
		FnAttribute::FloatAttribute::array_type cornerSharpness_data =
			cornerSharpness_attr.getNearestSample( 0.0f );

		if( !cornerIndices_data.empty() && !cornerSharpness_data.empty() )
		{
			i_ctx.m_nsi.SetAttribute(
				geo_name,
				(
					*NSI::Argument( "subdivision.cornervertices" )
						.SetType( NSITypeInteger )
						->SetCount( cornerIndices_data.size() )
						->SetValuePointer( cornerIndices_data.data() ),
					*NSI::Argument( "subdivision.cornersharpness" )
						.SetType( NSITypeFloat )
						->SetCount( cornerSharpness_data.size() )
						->SetValuePointer( cornerSharpness_data.data() )
				) );
		}
	}

	/* Export optional creases. */
	FnAttribute::IntAttribute creaseLengths_attr(
		i_location.getAttribute( "geometry.creaseLengths" ) );
	FnAttribute::IntAttribute creaseIndices_attr(
		i_location.getAttribute( "geometry.creaseIndices" ) );
	FnAttribute::FloatAttribute creaseSharpness_attr(
		i_location.getAttribute( "geometry.creaseSharpness" ) );

	if( creaseLengths_attr.isValid() && creaseIndices_attr.isValid() &&
	    creaseSharpness_attr.isValid() )
	{
		FnAttribute::IntAttribute::array_type
			creaseLengths_data = creaseLengths_attr.getNearestSample( 0.0f ),
			creaseIndices_data = creaseIndices_attr.getNearestSample( 0.0f );
		FnAttribute::FloatAttribute::array_type creaseSharpness_data =
			creaseSharpness_attr.getNearestSample( 0.0f );

		/* Convert crease chains of arbitrary length into single edges. */
		std::vector<int> creaseIndices;
		std::vector<float> creaseSharpness;
		/* This will be the right size when no conversion is actually needed. */
		creaseIndices.reserve( creaseIndices_data.size() );
		creaseSharpness.reserve( creaseSharpness_data.size() );

		size_t num_edges =
			std::min( creaseLengths_data.size(), creaseSharpness_data.size() );
		for( size_t i = 0, j = 0; i < num_edges; ++i )
		{
			int cl = creaseLengths_data[i] - 1;
			for( int k = 0; k < cl; ++k, ++j )
			{
				/* Safety check for too short array. */
				if( j + 1u >= creaseIndices_data.size() )
					break;

				creaseIndices.push_back( creaseIndices_data[j] );
				creaseIndices.push_back( creaseIndices_data[j + 1u] );
				creaseSharpness.push_back( creaseSharpness_data[i] );
			}
			++j;
		}

		if( !creaseIndices.empty() )
		{
			i_ctx.m_nsi.SetAttribute(
				geo_name,
				(
					*NSI::Argument( "subdivision.creasevertices" )
						.SetType( NSITypeInteger )
						->SetCount( creaseIndices.size() )
						->SetValuePointer( &creaseIndices[0] ),
					*NSI::Argument( "subdivision.creasesharpness" )
						.SetType( NSITypeFloat )
						->SetCount( creaseSharpness.size() )
						->SetValuePointer( &creaseSharpness[0] )
				) );
		}
	}

	/* TODO: Export geometry.interpolateBoundary. */
	/* TODO: Export geometry.facevaryinginterpolateboundary. */
	/* TODO: Export geometry.facevaryingpropagatecorners. */
	/* TODO: Export optional holes (geometry.holePolyIndices). */

	/* 3Delight extension. */
	int scc = 1;
	FnAttribute::IntAttribute scc_attr(
		i_location.getAttribute( "geometry.smoothcreasecorners" ) );
	if( scc_attr.isValid() )
	{
		scc = scc_attr.getValue(1, false);
	}
	i_ctx.m_nsi.SetAttribute(geo_name,
		NSI::IntegerArg("subdivision.smoothcreasecorners", scc));
}

/**
	\brief Export the geometry for a specific shape of area light.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location of the light to export.
	\param i_shape
		The shape of the area light. This is a parameter of our area lights.
		This function supports "disk", "sphere" and "cylinder" (open ended).
*/
void ExportLightShape(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	const std::string &i_shape )
{
	assert( !i_location.IsDeleted() );

	ExportSG::ExportGroup( i_ctx, i_location );

	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	if( i_shape == "square" )
	{
		i_ctx.m_nsi.Create( geo_name, "mesh" );
		i_ctx.m_nsi.Connect( geo_name, "", xform_name, "objects" );
		ExportID( i_ctx, i_location );

		float scale = 0.5f;
		float P[] = {
			-scale, -scale, 0.0f,  -scale, scale, 0.0f,
			scale, scale, 0.0f,   scale, -scale, 0.0f };
		int nvertices[] = { 4 };

		NSI::ArgumentList mesh_attributes;
		mesh_attributes.Add(
			NSI::Argument::New( "P" )
			->SetType( NSITypePoint )
			->SetCount( 4 )
			->SetValuePointer( const_cast<float*>(P)) );

		mesh_attributes.Add(
			NSI::Argument::New( "nvertices" )
			->SetType( NSITypeInteger )
			->SetCount( 1 )
			->SetValuePointer( const_cast<int*>(nvertices)) );

		i_ctx.m_nsi.SetAttribute( geo_name, mesh_attributes );
	}
	else if( i_shape == "disk" || i_shape == "sphere" )
	{
		i_ctx.m_nsi.Create( geo_name, "particles" );
		i_ctx.m_nsi.Connect( geo_name, "", xform_name, "objects" );
		ExportID( i_ctx, i_location );

		NSI::ArgumentList args;
		float P[3] = { 0.0f, 0.0f, 0.0f };
		args.push( NSI::Argument::New( "P" )
				->SetType( NSITypePoint )
				->SetValuePointer( &P[0] ) );

		args.push( new NSI::FloatArg( "width", 1.0f ) );

		float N[3] = { 0.0f, 0.0f, -1.0f };
		if( i_shape == "disk" )
		{
			args.push( NSI::Argument::New( "N" )
					->SetType( NSITypeNormal )
					->SetValuePointer( &N[0] ) );
		}

		i_ctx.m_nsi.SetAttribute( geo_name, args );
	}
	else if( i_shape == "cylinder" )
	{
		i_ctx.m_nsi.Create( geo_name, "mesh" );
		i_ctx.m_nsi.Connect( geo_name, "", xform_name, "objects" );
		ExportID( i_ctx, i_location );

		/* The cylinder is 1 unit long (in X) and has a radius of 0.5. */
		std::vector<float> P;
		std::vector<int> indices, nvertices;
		const int kNumSteps = 18;
		for( int i = 0; i < kNumSteps; ++i )
		{
			float angle = (float(i) / float(kNumSteps)) * float(2.0 * M_PI);
			float z = 0.5f * cos( angle );
			float y = 0.5f * sin( angle );
			P.push_back( y ); P.push_back( 0.5f ); P.push_back( z );
			P.push_back( y ); P.push_back( -0.5f ); P.push_back( z );

			nvertices.push_back( 4 );
			indices.push_back( i * 2 );
			indices.push_back( i * 2 + 1 );
			indices.push_back( (i * 2 + 3) % (2 * kNumSteps) );
			indices.push_back( (i * 2 + 2) % (2 * kNumSteps) );
		}

		NSI::ArgumentList args;
		args.push( NSI::Argument::New( "nvertices" )
				->SetType( NSITypeInteger )
				->SetCount( nvertices.size() )
				->SetValuePointer( &nvertices[0] ) );
		args.push( NSI::Argument::New( "P" )
				->SetType( NSITypePoint )
				->SetCount( 2 * kNumSteps )
				->SetValuePointer( &P[0] ) );
		args.push( NSI::Argument::New( "P.indices" )
				->SetType( NSITypeInteger )
				->SetCount( 4 * kNumSteps )
				->SetValuePointer( &indices[0] ) );
		i_ctx.m_nsi.SetAttribute( geo_name, args );
	}
}

void ExportXGen(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	auto sceneRoot = i_location.getRoot();
	if( !sceneRoot.isValid() )
	{
		/* Live render not supported here. */
		assert( false );
		return;
	}

	ExportSG::ExportGroup( i_ctx, i_location );

	// Retreive the XGen attributes
	FnAttribute::GroupAttribute xgen_attr( 
		i_location.getAttribute( "xgen" ) );

	FnAttribute::StringAttribute file_attr( 
		xgen_attr.getChildByName( "file" ) );
	FnAttribute::StringAttribute palette_attr( 
		xgen_attr.getChildByName( "palette" ) );
	FnAttribute::StringAttribute description_attr( 
		xgen_attr.getChildByName( "description" ) );
	FnAttribute::StringAttribute patch_attr( 
		xgen_attr.getChildByName( "patch" ) );
	FnAttribute::StringAttribute geom_attr( 
		xgen_attr.getChildByName( "geom" ) );
	FnAttribute::StringAttribute xgenlibdir_attr( 
		xgen_attr.getChildByName( "xgenlibdir" ) );
	FnAttribute::FloatAttribute frametime_attr( 
		xgen_attr.getChildByName( "frametime" ) );

	// Construct a parameter string to pass all these attributes
	std::string args;

	args += "-file ";
	args += file_attr.getValue();
	args += " -palette ";
	args += palette_attr.getValue();
	args += " -description ";
	args += description_attr.getValue();
	args += " -patch ";
	args += patch_attr.getValue();
	args += " -geom ";
	args += geom_attr.getValue();
	args += " -frame ";
	args += std::to_string( frametime_attr.getValue() );
	args += " -fps 24";  // Fixme

	// Motion sample offsets
	std::vector<double> offsets;
	i_ctx.m_renderSettings.GetMotionSampleOffsets(offsets);

	if( offsets.size() > 1 )
	{
		std::ostringstream offset_args;
		for( auto o : offsets )
		{
			offset_args << " " << o;
		}

		args += " -motionSamplesLookup";
		args += offset_args.str();
		args += " -motionSamplesPlacement";
		args += offset_args.str();
	}

	// Process the camera attributes that XGen needs. This used to be passed as
	// user attributes when the scene was output with the RenderMan API.
	//
	XGenCameraInfo cameraInfo( i_ctx, sceneRoot );

	// At this point we could call the procedural, if there was such a thing in
	// NSI. Until then, call the XGen lib directly and output the primitives on
	// the fly.
	//

	// Load the XGen libs. Not much we can do if that fails.
	if( !XGenLoader::Setup( xgenlibdir_attr.getValue() ) )
		return;

	// The XGen primitive nodes will be named "/path|leafX". Each 'face renderer'
	// will create a NSI primitve node.
	//
	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	// Create the object the XGen lib will call to produce the primitives
	// At this point, the NSI node name does not matter.
	//
	XGenNSICallbacks* patch_renderer_cb = 
		new XGenNSICallbacks( i_ctx, geo_name, cameraInfo );

	XGenRenderAPI::PatchRenderer* patch_renderer = 
		XGenLoader::gs_patchrender_init( patch_renderer_cb, args.c_str() );

	if( !patch_renderer )
	{
		/* Happens at least when xgen file is not found. */
		delete patch_renderer_cb;
		return;
	}

	bbox b;
	unsigned int f = -1;
	int xgenNameSuffix = 0;

	while( patch_renderer->nextFace( b, f ) )
	{
		// Skip camera culled bounding boxes
		if( XGenRenderAPI::Utils::isEmpty( b ) )
		{
			continue;
		}

		// Construct this face renderer's primitive NSI node name
		std::string curr_geo_name =
			geo_name + std::to_string( xgenNameSuffix++ );

		// Create the face renderer
		XGenNSICallbacks* face_renderer_cb = 
			new XGenNSICallbacks( i_ctx, curr_geo_name, cameraInfo );

		XGenRenderAPI::FaceRenderer* face_renderer = XGenLoader::gs_facerender_init( 
			patch_renderer, 
			f, 
			face_renderer_cb );

		// This will call ProceduralCallbacks::flush
		face_renderer->render();

		// Connect the NSI node the face renderer created to its parent transform
		i_ctx.m_nsi.Connect( curr_geo_name, "", xform_name, "objects" );

		delete face_renderer_cb;
	}

	delete patch_renderer_cb;
}

void ExportYeti(
	ExportContext& i_ctx,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_ctx, i_location );

	FnAttribute::GroupAttribute yeti_attr( 
		i_location.getAttribute( "yeti" ) );

	if( yeti_attr.isValid() )
	{
		// Figure the Yeti procedural parameter string based on the yeti 
		// attributes.
		//
		std::ostringstream params;

		FnAttribute::FloatAttribute density_attr(
			yeti_attr.getChildByName( "density" ) );

		if( density_attr.isValid() )
		{
			params << "--density " << density_attr.getValue() << " ";
		}

		FnAttribute::FloatAttribute length_attr(
			yeti_attr.getChildByName( "length" ) );

		if( length_attr.isValid() )
		{
			params << "--length " << length_attr.getValue() << " ";
		}

		FnAttribute::FloatAttribute width_attr(
			yeti_attr.getChildByName( "width" ) );

		if( width_attr.isValid() )
		{
			params << "--width " << width_attr.getValue() << " ";
		}

		FnAttribute::FloatAttribute frame_attr( 
			yeti_attr.getChildByName( "frame" ) );

		if( frame_attr.isValid() )
		{
			// Yeti procedural expects an integer frame number.
			//
			int integer_frame = frame_attr.getValue();
			params << "--frame " << integer_frame << " ";
		}

		FnAttribute::FloatAttribute verbose_attr( 
			yeti_attr.getChildByName( "verbosity" ) );

		if( verbose_attr.isValid() )
		{
			int verbose = verbose_attr.getValue();
			params << "--verbose " << verbose << " ";
		}

		// FIXME: not sure about center_samples. In Maya it is controlled by a
		// bool attribute. It should perhaps match the render pass motion blur
		// position, but what's the point of it when --samples lists the samples
		// to use?
		//
		params << "--curveChunkSize 0 --center_samples ";

		// Produce --samples parameter with the proper motion sample offsets
		params << "--samples ";

		std::vector<double> offsets;
		i_ctx.m_renderSettings.GetMotionSampleOffsets(offsets);
		for( auto o : offsets )
		{
			params << o << " ";
		}

		FnAttribute::StringAttribute paths_attr( 
			yeti_attr.getChildByName( "imagesearchpaths" ) );

		if( paths_attr.isValid() && paths_attr.getValue() != "" )
		{
			params << "--imageSearchPath " << paths_attr.getValue() << " ";
		}

		FnAttribute::StringAttribute file_attr( 
			yeti_attr.getChildByName( "file" ) );

		if( file_attr.isValid() )
		{
			params << "--file " << file_attr.getValue() << " ";
		}

		i_ctx.m_nsi.Evaluate( (
			NSI::StringArg( "type", "RiProcDynamicLoad" ),
			NSI::StringArg( "filename", "pgYetiDLRender" ),
			NSI::StringArg( "parameters", params.str() ),
			NSI::StringArg( "transform", i_location.getFullName() ) ) );
	}
}

void ExportNSIArchive(
	ExportContext& i_ctx,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_ctx, i_location );

	std::string file_name, root;
	GetString( i_location, "nsiarchive.file", file_name );
	GetString( i_location, "nsiarchive.roothandle", root );

	if(file_name.empty())
	{
		return;
	}

	i_ctx.m_nsi.Evaluate((
		NSI::StringArg("type", "apistream"),
		NSI::StringArg("filename", file_name)));

	if(root.empty())
	{
		return;
	}

	i_ctx.m_nsi.Connect(root, "", i_location.getFullName(), "objects");
}

/**
	\brief Export the geometry for a location of the DlOpenVDB node.

	\param i_ctx
		The export context.
	\param i_location
		The scene graph location of the vdb to export.

	Note that the NSI volume node only has the OpenVDB file name and grid
	names. All other shading parameters are on the shader.
*/
void ExportOpenVDB(
	ExportContext& i_ctx,
	const SGLocation &i_location )
{
	ExportSG::ExportGroup( i_ctx, i_location );

	std::string xform_name = i_location.getFullName();
	std::string geo_name = xform_name + "|leaf";

	if( i_location.IsDeleted() )
	{
		/* Live update deletion. */
		i_ctx.m_nsi.Delete( geo_name );
		return;
	}

	i_ctx.m_nsi.Create( geo_name, "volume" );
	i_ctx.m_nsi.Connect( geo_name, "", xform_name, "objects" );

	std::string vdbfilename, densitygrid, temperaturegrid, intensitygrid;
	std::string colorgrid;
	GetString( i_location, "openvdb.filename", vdbfilename );
	GetString( i_location, "openvdb.grids.smokeDensityGrid", densitygrid );
	GetString( i_location, "openvdb.grids.smokeColorGrid", colorgrid );
	GetString( i_location, "openvdb.grids.temperatureGrid", temperaturegrid );
	GetString( i_location, "openvdb.grids.emissionIntensityGrid",
		intensitygrid );

	std::string velocitygrid;
	double velocityscale = 1.0;
	GetString( i_location, "openvdb.motion.velocityGrid", velocitygrid );
	GetDouble( i_location, "openvdb.motion.velocityScale", velocityscale );

	i_ctx.m_nsi.SetAttribute(
		geo_name,
		(
			NSI::StringArg( "vdbfilename", vdbfilename ),
			NSI::StringArg( "densitygrid", densitygrid ),
			NSI::StringArg( "colorgrid", colorgrid ),
			NSI::StringArg( "temperaturegrid", temperaturegrid ),
			NSI::StringArg( "emissionintensitygrid", intensitygrid ),
			NSI::StringArg( "velocitygrid", velocitygrid ),
			NSI::DoubleArg( "velocityscale", velocityscale )
		) );
}

/**
	\brief Export the atmosphere object

	\param i_ctx
		The export context.
	\param i_location
		The location where to export the atmosphere.

	This exports the environment node which implements the global atmosphere
	set in DlSettings as well as the implicit assignment of its material.

	For now, unfortunately, there can only be one atmosphere and it is stored
	in dlGlobalStatements on /root
*/
void ExportAtmosphere(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	assert( i_location.getFullName() == "/root" );

	/* Fetch the assigned material. */
	std::string atmosphere_material;
	GetString(
		i_location,
		"dlGlobalStatements.atmosphere.assignAtmosphere",
		atmosphere_material );

	std::string atmosphere_handle = "atmosphere";
	std::string material_handle = atmosphere_material + "|material";

	/*
		Because this is on /root, it is never properly deleted. Instead, we'll
		just get an update which does not have atmosphere set.
	*/
	if( atmosphere_material.empty() )
	{
		if( i_location.IsLiveUpdate() )
		{
			i_ctx.m_nsi.Delete( atmosphere_handle );
		}
		return;
	}

	/* Create the environment node for the atmosphere. */
	i_ctx.m_nsi.Create( atmosphere_handle, "environment" );
	i_ctx.m_nsi.Connect( atmosphere_handle, "", NSI_SCENE_ROOT, "objects" );

	/* Always create this in case the material does not get exported first. */
	i_ctx.m_nsi.Create( material_handle, "attributes" );

	/* Connect the material to the environment node. */
	i_ctx.m_nsi.Connect(
		material_handle, "",
		atmosphere_handle, "geometryattributes" );
}

/**
	\brief Export the arbitrary values of a primitive.

	\param i_ctx
		The export context.
	\param i_location
		The location where to export the arbitrary values.
	\param i_vertexList
		Optional list of indices into the vertices for the primitive. This is
		used for "point" scope arbitrary values, if provided.

	Exports the stuff under geometry.arbitrary.<group>
*/
void ExportArbitraryValues(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	const FnAttribute::IntAttribute &i_vertexList )
{
	FnAttribute::GroupAttribute arbitrary_group(
		i_location.getAttribute( "geometry.arbitrary" ) );

	if( !arbitrary_group.isValid() )
		return;

	int64_t n = arbitrary_group.getNumberOfChildren();
	for( int64_t i = 0; i < n; ++i )
	{
		ExportOneArbitraryValue(
			i_ctx,
			i_location,
			i_vertexList,
			arbitrary_group.getChildName( i ),
			arbitrary_group.getChildByIndex( i ) );
	}
}

namespace
{
/**
	\brief Figure out which type to use to export an attribute.

	\param i_value_attribute
		The attribute which contains the values to export.
	\param i_group
		The group which contains the value attribute, so we can fetch related
		metadata (in the context of the geometry.arbitrary group).
	\param[out] o_nsi_type
		Outputs the NSI type to use for exporting.
	\param[out] o_arraylength
		Outputs the NSI array length to use for exporting.
	\param[out] o_nsi_type
		Outputs the NSI type flags to use for exporting.
*/
void AttributeExportType(
	const FnAttribute::DataAttribute &i_value_attribute,
	const FnAttribute::GroupAttribute &i_group,
	NSIType_t *o_nsi_type,
	unsigned *o_arraylength,
	int *o_nsi_flags )
{
	*o_nsi_type = NSITypeInvalid;
	*o_arraylength = 0;
	*o_nsi_flags = 0;

	int input_tuple_size = 1;

	std::string inputType;
	GetString( i_group, "inputType", inputType );
	if( !inputType.empty() )
	{
		/* Try parsing the provided type. */
		if( inputType == "float" || inputType == "double" )
		{
			*o_nsi_type = NSITypeFloat;
		}
		else if( inputType == "int" )
		{
			*o_nsi_type = NSITypeInteger;
		}
		else if( inputType == "string" )
		{
			*o_nsi_type = NSITypeString;
		}
		else if( inputType == "point2" )
		{
			/* We don't have a native point2 type so export as float[2]. */
			*o_nsi_type = NSITypeFloat;
		}
		else if( inputType == "color3" )
		{
			*o_nsi_type = NSITypeColor;
			input_tuple_size = 3;
		}
		else if( inputType == "normal3" )
		{
			*o_nsi_type = NSITypeNormal;
			input_tuple_size = 3;
		}
		else if( inputType == "vector3" )
		{
			*o_nsi_type = NSITypeVector;
			input_tuple_size = 3;
		}
		else if( inputType == "point3" )
		{
			*o_nsi_type = NSITypePoint;
			input_tuple_size = 3;
		}
		else if( inputType == "matrix16" )
		{
			*o_nsi_type = NSITypeDoubleMatrix;
			input_tuple_size = 16;
		}
	}

	if( *o_nsi_type == NSITypeInvalid )
	{
		/* Guess from the attribute's data type. */
		if( FnAttribute::FloatAttribute( i_value_attribute ).isValid() )
		{
			*o_nsi_type = NSITypeFloat;
		}
		else if( FnAttribute::DoubleAttribute( i_value_attribute ).isValid() )
		{
			/* Export as float as shaders don't have double type. */
			*o_nsi_type = NSITypeFloat;
		}
		else if( FnAttribute::IntAttribute( i_value_attribute ).isValid() )
		{
			*o_nsi_type = NSITypeInteger;
		}
		else if( FnAttribute::StringAttribute( i_value_attribute ).isValid() )
		{
			*o_nsi_type = NSITypeString;
		}
	}

	auto data_tuple_size = i_value_attribute.getTupleSize();
	if( inputType == "point2" && data_tuple_size == 1 )
	{
		/* Patch for "st" which sometimes don't have a tuple size of 2. */
		data_tuple_size = 2;
	}
	if( data_tuple_size > input_tuple_size )
	{
		*o_arraylength = data_tuple_size / input_tuple_size;
		*o_nsi_flags |= NSIParamIsArray;
	}
	/*
		This is an optional array length. If it specifies an array, we use it
		regardless of what we deduced above.
	*/
	int elementSize = 0;
	GetInteger( i_group, "elementSize", elementSize );
	if( elementSize > 1 )
	{
		*o_arraylength = elementSize;
		*o_nsi_flags |= NSIParamIsArray;
	}
}
}

/**
	\brief Export one arbitrary value on a primitive.

	\param i_ctx
		The export context.
	\param i_location
		The location where to export the arbitrary value.
	\param i_vertexList
		Optional list of indices into the vertices for the primitive. This is
		used for "point" scope arbitrary values, if provided.
	\param i_value_name
		The name of the value (name of the group under geometry.arbitrary).
	\param i_value
		The group attribute of the value (under geometry.arbitrary).

	The format of these values is documented in the katana user guide under
	"Appendix G: Standard Attributes".
*/
void ExportOneArbitraryValue(
	ExportContext &i_ctx,
	const SGLocation &i_location,
	const FnAttribute::IntAttribute &i_vertexList,
	const std::string &i_value_name,
	const FnAttribute::GroupAttribute &i_value )
{
	if( !i_value.isValid() )
		return;

	std::string node_handle = i_location.getFullName() + "|leaf";

	/* First try a plain value. */
	FnAttribute::FloatAttribute value_attr( i_value.getChildByName( "value" ) );
	FnAttribute::IntAttribute index_attr;

	if( !value_attr.isValid() )
	{
		/* Try getting an indexed value instead. */
		value_attr = i_value.getChildByName( "indexedValue" );
		index_attr = i_value.getChildByName( "index" );
	}

	/* If there is no index, use i_vertexList for "point" scope attributes. */
	if( !index_attr.isValid() && i_vertexList.isValid() )
	{
		std::string scope;
		GetString( i_value, "scope", scope );
		if( scope == "point" )
		{
			index_attr = i_vertexList;
		}
	}

	if( value_attr.isValid() )
	{
		NSIType_t nsi_type = NSITypeInvalid;
		unsigned arraylength = 0;
		int nsi_flags = 0;

		AttributeExportType(
			value_attr, i_value, &nsi_type, &arraylength, &nsi_flags );

		ExportSG::ExportAnyTypeAttribute(
			i_ctx,
			value_attr,
			node_handle.c_str(),
			i_value_name.c_str(),
			nsi_type,
			arraylength,
			nsi_flags );
	}

	if( value_attr.isValid() && index_attr.isValid() )
	{
		FnAttribute::IntAttribute::array_type index_data =
			index_attr.getNearestSample( 0.0f );

		i_ctx.EvictionManager().CountData( index_data.size() * sizeof(int) );
		i_ctx.m_nsi.SetAttribute(
			node_handle,
			*NSI::Argument( i_value_name + ".indices" )
				.SetType( NSITypeInteger )
				->SetCount( index_data.size() )
				->SetValuePointer( index_data.data() ) );
	}
}

/**
	\brief Exports the user attributes imported from an alembic file.

	\param i_ctx
		The export context.
	\param i_location
		The location where to export the arbitrary value.

	These attributes end up under the abcUser group and don't have all the
	structure of geometry.arbitrary attributes. I have not found documentation
	on them so this is an educated guess.
*/
void ExportAlembicUserAttributes(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	FnAttribute::GroupAttribute alembic_group(
		i_location.getAttribute( "abcUser" ) );

	if( !alembic_group.isValid() )
		return;

	std::string node_handle = i_location.getFullName() + "|leaf";

	int64_t n = alembic_group.getNumberOfChildren();
	for( int64_t i = 0; i < n; ++i )
	{
		const std::string &value_name = alembic_group.getChildName( i );
		FnAttribute::DataAttribute value( alembic_group.getChildByIndex( i ) );
		if( !value.isValid() )
			continue;

		NSIType_t nsi_type = NSITypeInvalid;
		unsigned arraylength = 0;
		int nsi_flags = 0;

		AttributeExportType(
			value, FnAttribute::GroupAttribute(),
			&nsi_type, &arraylength, &nsi_flags );

		ExportSG::ExportAnyTypeAttribute(
			i_ctx,
			value,
			node_handle.c_str(),
			value_name.c_str(),
			nsi_type,
			arraylength,
			nsi_flags );
	}
}

}
