/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "AttributeUtils.h"
#include "ExportCamera.h"
#include "ExportContext.h"
#include "ExportSG.h"
#include "Renderer.h"
#include "utils.h"

namespace ExportCamera
{

/**
	\brief Export a camera node in the scene graph.

	Note that as in many applications, the katana camera expects to be looking
	towards negative Z.

	The camera is normally parsed by FnKat::Render::CameraSettings but we can't
	use it because it only works with a scene graph iterator. Fortunately, we
	can read its source and do roughly the same thing here.
*/
void ExportCamera(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	using namespace AttributeUtils;

	/* This will export the camera's transform. */
	ExportSG::ExportGroup( i_ctx, i_location );

	/* We'll create a "/path|leaf" node for the camera itself. */
	std::string xform_name = i_location.getFullName();
	std::string cam_name = xform_name + "|leaf";

	if( i_location.IsDeleted() )
	{
		/* Not well supported but nothing we can do about it. */
		std::string screen_name = xform_name + "|screen";
		i_ctx.RemoveScreen( screen_name );
		return;
	}

	/* Parse Katana camera settings. Look at CameraSettings source for info. */
	// FIXME : those aren't used anywhere!
	int displayWindow[4];
	int overscan[4];
	i_ctx.m_renderSettings.getDisplayWindow( displayWindow );
	i_ctx.m_renderSettings.getOverscan( overscan );

	std::string group_name(_3DELIGHT_RENDERER_NAME);
	group_name += "CameraSettings";

	FnAttribute::GroupAttribute settings_attr( 
		i_location.getAttribute( group_name.c_str() ) );

	float shutter_open_eff = 0.75;
	float shutter_close_eff = 0.75;
	int useFinite = 0;
	int number_blades = 0;
	float rotation = 0;
	float horizontal_fov = 0;
	std::string distortion_map = "";
	float fstop = 2;
	float focal_length = 0;
	float focal_distance = 0;
	if( settings_attr.isValid() )
	{
		GetFloat( settings_attr, "shutter.shutterOpenEff", shutter_open_eff );
		GetFloat( settings_attr, "shutter.shutterCloseEff", shutter_close_eff );
		GetInteger( settings_attr, "lens.useFinite", useFinite );
		GetInteger( settings_attr, "lens.numberBlades", number_blades );
		GetFloat( settings_attr, "lens.rotation", rotation );
		GetString( settings_attr, "lens.distortionMap", distortion_map );
		GetFloat( settings_attr, "lens.fstop", fstop );
		GetFloat( settings_attr, "lens.focalLength", focal_length );
		GetFloat( settings_attr, "lens.focalDistance", focal_distance );
	}
	/* fstop is needed by ExportOptions. */
	i_ctx.m_renderSettings.SetFStop(fstop);

	std::string projection = "perspective";
	GetString( i_location, "geometry.projection", projection );
	GetFloat( i_location, "geometry.auxFov", horizontal_fov );

	/* Set camera attributes. */
	NSI::DynamicArgumentList camera_args;

	const char* typeName = "perspectivecamera";
	if ( projection == "orthographic" )
	{
		typeName = "orthographiccamera";
	}
	else if ( projection == "Cylindrical" )
	{
		typeName = "cylindricalcamera";
		camera_args.Add( new NSI::FloatArg( "horizontalfov", horizontal_fov ) );
	}
	else if ( projection == "Fisheye stereographic" )
	{
		typeName = "fisheyecamera";
		camera_args.Add( new NSI::StringArg( "mapping", "stereographic" ) );
	}
	else if ( projection == "Fisheye equidistant" )
	{
		typeName = "fisheyecamera";
		camera_args.Add( new NSI::StringArg( "mapping", "equidistant" ) );
	}
	else if ( projection == "Fisheye equisolid" )
	{
		typeName = "fisheyecamera";
		camera_args.Add( new NSI::StringArg( "mapping", "equisolid" ) );
	}
	else if ( projection == "Fisheye orthographic" )
	{
		typeName = "fisheyecamera";
		camera_args.Add( new NSI::StringArg( "mapping", "orthographic" ) );
	}
	else if ( projection == "Spherical" )
	{
		typeName = "sphericalcamera";
	}

	/* Create camera node and connect to transform. */
	i_ctx.m_nsi.Create( cam_name, typeName );
	i_ctx.m_nsi.Connect( cam_name, "", xform_name, "objects" );

	bool interactiveRender =
		i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive();

	/* FOV */
	ExportSG::ExportFloatAttribute(
		i_ctx,
		i_location.getAttribute( "geometry.fov" ),
		cam_name.c_str(),
		"fov",
		NSITypeFloat, 0, 0 );

	/* Clipping. */
	double clip_near = 0.0, clip_far = 1e38;
	GetDouble( i_location, "geometry.near", clip_near );
	GetDouble( i_location, "geometry.far", clip_far );
	double katana_clipping[2] = { clip_near, clip_far };
	camera_args.Add(
		NSI::Argument::New( "clippingrange" )
		->SetType( NSITypeDouble )
		->SetCount( 2 )
		->CopyValue( katana_clipping, sizeof(katana_clipping) ) );

	/*
		Set all depth of field parameters.
	*/
	int dof = !( interactiveRender &&
		i_ctx.m_renderSettings.GetDisableDepthOfField() );
	/* No DoF if any of these is not positive. */
	if( focal_length <= 0.0f || focal_distance <= 0.0f || fstop <= 0.0f )
		dof = 0;

	camera_args.Add( new NSI::IntegerArg( "depthoffield.enable", dof) );
	camera_args.Add(
		new NSI::IntegerArg( "depthoffield.aperture.enable", useFinite) );
	camera_args.Add(
		new NSI::IntegerArg( "depthoffield.aperture.sides", number_blades ));
	camera_args.Add(
		new NSI::DoubleArg( "depthoffield.aperture.angle", rotation) );
	if( dof )
	{
		/* Don't output these when no DoF. Otherwise, 3Delight emits a warning
		   about out of range values. */
		camera_args.Add( new NSI::DoubleArg(
				"depthoffield.focallength", focal_length ) );
		camera_args.Add( new NSI::DoubleArg(
				"depthoffield.focaldistance", focal_distance ) );
		camera_args.Add( new NSI::DoubleArg(
				"depthoffield.fstop", fstop ) );
	}

    /* Shutter */
	double camera_shutter[2] =
	{
		interactiveRender && i_ctx.m_renderSettings.GetDisableMotionBlur()
		? 0 : i_ctx.m_renderSettings.getShutterOpen(),
		interactiveRender && i_ctx.m_renderSettings.GetDisableMotionBlur()
		? 0 : i_ctx.m_renderSettings.getShutterClose()
	};
	camera_args.Add(
		NSI::Argument::New( "shutterrange" )
		->SetType( NSITypeDouble )
		->SetCount( 2 )
		->CopyValue( camera_shutter, sizeof(camera_shutter) ) );

	/* Shutter efficiency */
	double camera_shutter_eff[2] =
	{
		1.0 - shutter_open_eff,
		shutter_close_eff
	};
	camera_args.Add(
		NSI::Argument::New( "shutteropening" )
		->SetType( NSITypeDouble )
		->SetCount( 2 )
		->CopyValue( camera_shutter_eff, sizeof(camera_shutter_eff) ) );

	i_ctx.m_nsi.SetAttribute( cam_name, camera_args );

	/* Distortion Map */
	if( distortion_map != "")
	{
		NSI::DynamicArgumentList shader_args;

		i_ctx.m_nsi.Create( "shader_lens_distortion", "shader" );
		std::string shader_name = Utils::GetOSLDirectory();
		shader_name += "/";
		shader_name += "lens_distortion";
		shader_args.Add( new NSI::StringArg( "shaderfilename", shader_name ) );
		shader_args.Add( new NSI::StringArg( "st_map", distortion_map ) );
		i_ctx.m_nsi.SetAttribute( "shader_lens_distortion", shader_args );
		i_ctx.m_nsi.Connect( "shader_lens_distortion", "", cam_name, "lensshader" );
	}
}

/**
	\brief Exports one screen node and attaches it to a camera node.
*/
void ExportScreen(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	using namespace AttributeUtils;

	std::string xform_name = i_location.getFullName();
	std::string screen_name = xform_name + "|screen";
	std::string cam_name = xform_name + "|leaf";

	/*
		We keep a global list of screens for settings update which must be
		applied to the screens. ie. things which are global in katana but
		specific to the screen in NSI.
	*/
	i_ctx.AddScreen( screen_name );

	i_ctx.m_nsi.Create( screen_name, "screen" );
	i_ctx.m_nsi.Connect( screen_name, "", cam_name, "screens" );

	NSI::DynamicArgumentList screen_args;

	bool interactiveRender =
		i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive();

	/* Resolution. */
	int camera_res[2];
	i_ctx.m_renderSettings.ComputeRenderResolution(
		interactiveRender, camera_res );
	screen_args.Add(
		NSI::Argument::New( "resolution" )
		->SetArrayType( NSITypeInteger, 2 )
		->CopyValue( camera_res, sizeof(camera_res) ) );

	/*
		Screen window.

		This should also take overscan into account (TODO). See CameraSettings
		class implementation.
	*/
	{
		double sw[4] = { -1.0, -1.0, 1.0, 1.0 };
		if (i_location.getFullName() == "__VIEWERPATH__")
		{
			/* Retrieves it from RenderSettings since i_location has bad
				screen window (-1 -1 1 1). */
			i_ctx.m_renderSettings.getScreenWindow(sw);
		}
		else
		{
			GetDouble( i_location, "geometry.left", sw[0] );
			GetDouble( i_location, "geometry.right", sw[2] );
			GetDouble( i_location, "geometry.bottom", sw[1] );
			GetDouble( i_location, "geometry.top", sw[3] );

			/* Keeps it updated in RenderSettings. */
			i_ctx.m_renderSettings.UpdateScreenWindow(sw);
		}

		screen_args.Add(
			NSI::Argument::New( "screenwindow" )
			->SetArrayType( NSITypeDouble, 2 )
			->SetCount( 2 )
			->CopyValue( sw, sizeof(sw) ) );
	}

	/*
		By default, the Katana includes the Region of Interest in the crop
		window. That's ok for disk/batch renders but we do something different
		for live and preview renders.
	*/
	bool ROI_as_crop =
		!(i_ctx.m_renderer.IsLive() || i_ctx.m_renderer.IsPreview());

	/* Crop window */
	float katana_crop[4];
	if( ROI_as_crop )
	{
		i_ctx.m_renderSettings.getCropWindow( katana_crop );
	}
	else
	{
		i_ctx.m_renderSettings.getRealCropWindow( katana_crop );
	}
	float dl_crop[4] =
	{
		katana_crop[0], /* left */
		katana_crop[2], /* top */
		katana_crop[1], /* right */
		katana_crop[3]  /* bottom */
	};
	screen_args.Add(
		NSI::Argument::New( "crop" )
		->SetArrayType( NSITypeFloat, 2 )
		->SetCount( 2 )
		->CopyValue( dl_crop, sizeof(dl_crop) ) );

	if( !ROI_as_crop )
	{
		ExportPriorityWindow( i_ctx, screen_name );
	}

	/* Oversampling */
	int num_samples = i_ctx.m_renderSettings.GetPixelSamples();
	screen_args.Add( new NSI::IntegerArg( "oversampling", num_samples ) );

	i_ctx.m_nsi.SetAttribute( screen_name, screen_args );
}

/**
	\brief Output the region of interest as a priority window.

	The katana ROI attribute is 4 integers: left, bottom, width, height.
	Bottom is a pixel count from the bottom of the frame.

	NSI's priority window is similar to EXR display and data windows. It is an
	inclusive range with the origin at the top left of the frame.
*/
void ExportPriorityWindow(
	ExportContext &i_ctx,
	const std::string &i_screen_handle )
{
	bool interactiveRender =
		i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive();
	int camera_res[2];
	i_ctx.m_renderSettings.ComputeRenderResolution(
		interactiveRender, camera_res );

	int ROI[4];
	if( !i_ctx.m_renderSettings.getROI( ROI ) )
	{
		/* ROI might have been disabled by a live update. */
		i_ctx.m_nsi.DeleteAttribute( i_screen_handle, "prioritywindow" );
		return;
	}

	/* left, top, right, bottom. */
	int pw[4] =
	{
		ROI[0],
		camera_res[1] - (ROI[1] + ROI[3]),
		ROI[0] + ROI[2],
		camera_res[1] - ROI[1]
	};
	i_ctx.m_nsi.SetAttribute(
		i_screen_handle,
		*NSI::Argument( "prioritywindow" )
			.SetArrayType( NSITypeInteger, 2 )
			->SetCount( 2 )
			->SetValuePointer( pw ) );
}

/**
	\brief Updates camera attributes which depend of global options.

	Overrides options "disable motion blur" can modify shutterrange.
*/
void ExportOptions(	ExportContext &i_ctx )
{
	bool interactiveRender =
		i_ctx.m_renderer.IsPreview() || i_ctx.m_renderer.IsLive();

	std::string cam_name = i_ctx.m_renderSettings.getCameraName() + "|leaf";

	/* Updates some camera attributes. */
	NSI::DynamicArgumentList camera_args;

	/* Shutter */
	double camera_shutter[2] =
	{
		interactiveRender && i_ctx.m_renderSettings.GetDisableMotionBlur()
		? 0 : i_ctx.m_renderSettings.getShutterOpen(),
		interactiveRender && i_ctx.m_renderSettings.GetDisableMotionBlur()
		? 0 : i_ctx.m_renderSettings.getShutterClose()
	};
	camera_args.Add(
		NSI::Argument::New( "shutterrange" )
		->SetType( NSITypeDouble )
		->SetCount( 2 )
		->CopyValue( camera_shutter, sizeof(camera_shutter) ) );

	int dof = interactiveRender &&
		i_ctx.m_renderSettings.GetDisableDepthOfField();

	camera_args.Add( new NSI::IntegerArg( "depthoffield.enable", !dof));

	i_ctx.m_nsi.SetAttribute( cam_name, camera_args );
}

}
