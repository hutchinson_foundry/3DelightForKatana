/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

/* Lots of includes from the katana example. */
#ifdef _WIN32
#include <FnPlatform/Windows.h>
#define _USE_MATH_DEFINES // for C++
#endif
#include <cmath>

#include <FnViewerModifier/plugin/FnViewerModifier.h>
#include <FnViewerModifier/plugin/FnViewerModifierInput.h>
#include <FnAttribute/FnGroupBuilder.h>
#include <FnAttribute/FnAttribute.h>
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
/* End of katana example includes. */

#include "LightViewerModifier.h"

#include "AttributeUtils.h"

#include <algorithm>

FnKat::ViewerModifier* LightViewerModifier::create( FnKat::GroupAttribute args )
{
	return new LightViewerModifier( args );
}

FnKat::GroupAttribute LightViewerModifier::getArgumentTemplate()
{
	return FnKat::GroupAttribute();
}

const char* LightViewerModifier::getLocationType()
{
	return "light";
}

void LightViewerModifier::flush()
{
}

void LightViewerModifier::onFrameBegin()
{
}

void LightViewerModifier::onFrameEnd()
{
}


LightViewerModifier::LightViewerModifier( FnKat::GroupAttribute args )
:
	FnKat::ViewerModifier( args ),
	m_type( type_unknown )
{
}


LightViewerModifier::~LightViewerModifier()
{
}


void LightViewerModifier::setup( FnKat::ViewerModifierInput& input )
{
	FnAttribute::GroupAttribute info_grp( input.getLiveAttribute( "info" ) );

	std::string package;
	AttributeUtils::GetString( info_grp, "gaffer.packageClass", package );

	if( package == "AreaLightPackage" )
	{
		m_type = type_area;

		FnAttribute::GroupAttribute geo_grp(
			input.getLiveAttribute( "geometry" ) );
		std::string shape;
		AttributeUtils::GetString( geo_grp, "shape", shape );

		if( shape == "disk" )
			m_type = type_area_disk;
		else if( shape == "sphere" )
			m_type = type_area_sphere;
		else if( shape == "cylinder" )
			m_type = type_area_cylinder;
	}
	else if( package == "DistantLightPackage" )
		m_type = type_distant;
	else if( package == "SpotLightPackage" )
		m_type = type_spot;
	else if( package == "MeshLightPackage" )
		m_type = type_mesh;
	else if( package == "PointLightPackage" )
		m_type = type_point;
}


void LightViewerModifier::cleanup( FnKat::ViewerModifierInput& input )
{
}


void LightViewerModifier::deepSetup( FnKat::ViewerModifierInput& input )
{
	/*
		Disable default drawing for lights we support, except for the mesh
		light which uses the default drawing.
	*/
	if( m_type != type_unknown && m_type != type_mesh )
	{
		input.overrideHostGeometry();
	}
}


void LightViewerModifier::draw( FnKat::ViewerModifierInput& input )
{
	/* Don't draw anything if we're looking through the light. */
	/* TODO: Barn doors might still be nice here. */
	if( input.isLookedThrough() )
		return;

	glPushAttrib( GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_LINE_BIT );
	glDisable( GL_LIGHTING );
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	if( input.getDrawOption("isPicking") )
	{
		/* Easier picking. */
		glLineWidth( 7 );
	}
	else
	{
		if( input.isSelected() )
		{
			/* White when selected. */
			glColor3f( 1.0, 1.0, 1.0 );
		}
		else
		{
			/* Yellow seems to be the convention for katana lights. */
			glColor3f( 1.0, 1.0, 0.0 );
		}
	}

	switch( m_type )
	{
		case type_unknown:
			break;
		case type_area:
			drawAreaLight( input );
			break;
		case type_distant:
			drawDistantLight( input );
			break;
		case type_spot:
			drawSpotLight( input );
			break;
		case type_mesh:
			/* Everything is done by the viewer's default drawing. */
			break;
		case type_area_disk:
			drawDiskLight( input );
			break;
		case type_area_sphere:
			drawSphereLight( input );
			break;
		case type_area_cylinder:
			drawCylinderLight( input );
			break;
		case type_point:
			drawPointLight( input );
			break;
	}

	glPopAttrib();
}


void LightViewerModifier::deepCleanup( FnKat::ViewerModifierInput& input )
{
}


FnKat::DoubleAttribute LightViewerModifier::getLocalSpaceBoundingBox(
	FnKat::ViewerModifierInput& input )
{
	/* We'll just fit everything into that box for now. */
	double bounds[6] = { -1, 1, -1, 1, -1, 1 };
	return FnKat::DoubleAttribute( bounds, 6, 1 );
}

/*
	Draws a quad with a single line for orientation.
*/
void LightViewerModifier::drawAreaLight( FnKat::ViewerModifierInput& input )
{
	/*
		We could fetch the geometry from the attributes. But for now it's much
		simpler to just assume the values used in our AreaLightPackage.
	*/
	glBegin( GL_POLYGON );
	glVertex3f( -0.5, 0.5, 0.0 );
	glVertex3f( 0.5, 0.5, 0.0 );
	glVertex3f( 0.5, -0.5, 0.0 );
	glVertex3f( -0.5, -0.5, 0.0 );
	glEnd();

	/* Extra line for direction. */
	glBegin( GL_LINE_STRIP );
	glVertex3f( 0.0, 0.0, 0.0 );
	glVertex3f( 0.0, 0.0, -0.5 );
	glEnd();
}

/*
	Draws 3 arrows in a circle, aimed at -Z
*/
void LightViewerModifier::drawDistantLight( FnKat::ViewerModifierInput& input )
{
	glPushMatrix();
	glScalef( 0.8, 0.8, 0.8 );
	for( unsigned i = 0; true; ++i )
	{
		glBegin( GL_POLYGON );
		glVertex3f( 0.04, 0.4, -0.7 );
		glVertex3f( 0.04, 0.4, 1.0 );
		glVertex3f( -0.04, 0.4, 1.0 );
		glVertex3f( -0.04, 0.4, -0.7 );
		glVertex3f( -0.13, 0.4, -0.7 );
		glVertex3f( 0.0, 0.4, -1.0 );
		glVertex3f( 0.13, 0.4, -0.7 );
		glEnd();

		if( i == 2 )
			break;

		glRotatef( 120.0, 0.0, 0.0, 1.0 );
	}
	glPopMatrix();
}

/*
	Draws a cone with its apex at the origin and its base aimed at -Z.
*/
void LightViewerModifier::drawSpotLight( FnKat::ViewerModifierInput& input )
{
	/* Fetch cone angle from spotLight shader's parameter. */
	double radius = 0.5;
	FnAttribute::FloatAttribute coneAttr(
		input.getAttribute( "material.dlLightParams.coneAngle" ) );
	if( coneAttr.isValid() )
	{
		double angle = coneAttr.getValue( 0.0f );
		angle = std::min( 179.0, std::max( 0.0, angle ) );
		angle = angle * M_PI / 180.0;
		radius = std::tan( angle * 0.5 );
	}

	double length = 1.0;
	/* Shorten wide cones to stay inside bbox. */
	if( radius > 1.0 )
	{
		length /= radius;
		radius = 1.0;
	}

	glPushMatrix();
	glTranslatef( 0.0, 0.0, -length );
	GLUquadric *q = gluNewQuadric();
	gluQuadricDrawStyle( q, GLU_SILHOUETTE );
	gluDisk( q, 0.0, radius, 24, 1 );
	gluDeleteQuadric( q );
	glPopMatrix();

	glBegin( GL_LINES );
	glVertex3f( 0.0, 0.0, 0.0 );
	glVertex3f( radius, 0.0, -length );
	glVertex3f( 0.0, 0.0, 0.0 );
	glVertex3f( -radius, 0.0, -length );
	glVertex3f( 0.0, 0.0, 0.0 );
	glVertex3f( 0.0, radius, -length );
	glVertex3f( 0.0, 0.0, 0.0 );
	glVertex3f( 0.0, -radius, -length );
	glEnd();
}

void LightViewerModifier::drawDiskLight( FnKat::ViewerModifierInput& input )
{
	GLUquadric *q = gluNewQuadric();
	gluQuadricDrawStyle( q, GLU_SILHOUETTE );
	gluDisk( q, 0.0, 0.5, 24, 1 );
	gluDeleteQuadric( q );

	/* Extra line for direction. */
	glBegin( GL_LINE_STRIP );
	glVertex3f( 0.0, 0.0, 0.0 );
	glVertex3f( 0.0, 0.0, -0.5 );
	glEnd();
}

void LightViewerModifier::drawSphereLight( FnKat::ViewerModifierInput& input )
{
	GLUquadric *q = gluNewQuadric();
	gluSphere( q, 0.5, 18, 9 );
	gluDeleteQuadric( q );
}

void LightViewerModifier::drawPointLight( FnKat::ViewerModifierInput& input )
{
	double radius = 0.5;
	FnAttribute::DoubleAttribute pointRadius(
		input.getAttribute( "geometry.pointRadius" ) );

	if( pointRadius.isValid() )
	{
		radius = pointRadius.getValue( 0.0f );
	}

	GLUquadric *q = gluNewQuadric();
	gluSphere( q, radius, 18, 9 );
	gluDeleteQuadric( q );
}

void LightViewerModifier::drawCylinderLight( FnKat::ViewerModifierInput& input )
{
	/*
		The commented disks are the end caps. They are not shown because they
		are not rendered (don't make sense for typical use of a lighting tube).
		We might want a "capped cylinder" eventually.
	*/
	glPushMatrix();
	GLUquadric *q = gluNewQuadric();
	glRotatef( 90, 1, 0, 0 );
	glTranslatef( 0.0, 0.0, 0.5 );
	//gluDisk( q, 0.0, 0.5, 24, 1 );
	glTranslatef( 0.0, 0.0, -1.0 );
	gluCylinder( q, 0.5, 0.5, 1.0, 18, 1 );
	//gluQuadricOrientation( q, GLU_INSIDE );
	//gluDisk( q, 0.0, 0.5, 24, 1 );
	gluDeleteQuadric( q );
	glPopMatrix();
}
