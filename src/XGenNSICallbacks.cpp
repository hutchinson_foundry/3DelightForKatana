/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "XGenNSICallbacks.h"

#include "ExportContext.h"
#include "FnAttribute/FnGroupBuilder.h"
#include "FnGeolibServices/FnXFormUtil.h"
#include "LogReporting.h"

#include <float.h>
#include <sstream>

/*
	Fudge factor that tries to account for variations in hair density over the
	surface. Each footprint is made half the average footprint size to avoid
	blurring the texture too much in denser areas. This would not be required if
	we had a better method to compute hair footprints.
*/
static const float kFootprintMultiplier = 0.5f;

XGenCameraInfo::XGenCameraInfo(
	ExportContext& i_ctx,
	const FnKat::FnScenegraphIterator &i_root )
{
	// Get the render camera settings.
	const Foundry::Katana::Render::CameraSettings* camera =
		i_ctx.m_renderSettings.getCameraSettings();

	// Figure if the render camera is orthogonal
	bool is_ortho = camera->getProjection() != "perspective";

	// FOV - irRenderCamFOV
	float fov = is_ortho ? 90.0 : camera->getFov();

	m_fovAsString = std::to_string( fov ) + ", " + std::to_string( fov );

	/*
		Concatenate all transforms for the camera. Do it into a group so we can
		use the geolib util to get the final matrix.
	*/
	FnKat::FnScenegraphIterator cameraIt =
		i_root.getByPath( camera->getName() );
	FnKat::GroupBuilder xformBuilder;
	while( cameraIt.isValid() )
	{
		FnAttribute::GroupAttribute xformAttr = cameraIt.getAttribute("xform");
		if( xformAttr.isValid() )
		{
			xformBuilder.setWithUniqueName( "xform", xformAttr );
		}
		cameraIt = cameraIt.getParent();
	}
	auto xformMatrix = FnGeolibServices::FnXFormUtil::CalcTransformMatrixAtTime(
		xformBuilder.build(), 0.0f );
	auto &&xformData = xformMatrix.first.getNearestSample( 0.0f );

	/*
		"Inverse" transform - irRenderCamXform
		By inverse they really mean the transform which places the camera,
		which is the inverse of what was specified in RenderMan days where the
		camera transform was effectively moving the scene.
	*/
	std::ostringstream transform_buffer;
	transform_buffer.precision(8);
	for( unsigned i = 0; i < 16; ++i )
	{
		transform_buffer << xformData[i];
		if( i != 15 )
		{
			transform_buffer << ", ";
		}
	}
	m_transformAsString = transform_buffer.str();

	// Position - irRenderCam
	m_positionAsString = is_ortho ? "true" : "false";
	m_positionAsString += ", ";
	m_positionAsString += std::to_string( xformData[12] );
	m_positionAsString += ", ";
	m_positionAsString += std::to_string( xformData[13] );
	m_positionAsString += ", ";
	m_positionAsString += std::to_string( xformData[14] );

	// Camera ratio - irRenderCamRatio 
	/* TODO */
	float ratio = 1.0f;
	m_ratioAsString = std::to_string( ratio );
}

XGenCameraInfo::~XGenCameraInfo()
{
}

const char* XGenCameraInfo::getFOVAsString() const
{
	return m_fovAsString.c_str();
}

const char* XGenCameraInfo::getRatioAsString() const
{
	return m_ratioAsString.c_str();
}

const char* XGenCameraInfo::getPositionAsString() const
{
	return m_positionAsString.c_str();
}

const char* XGenCameraInfo::getTransformAsString() const
{
	return m_transformAsString.c_str();
}


XGenNSICallbacks::XGenNSICallbacks(
	ExportContext& i_ctx,
	const std::string& i_nsiNodeName,
	const XGenCameraInfo& i_cameraInfo ) :
	m_ctx( i_ctx ),
	m_nsiNodeName( i_nsiNodeName ),
	m_cameraInfo( i_cameraInfo )
{
}

XGenNSICallbacks::~XGenNSICallbacks() 
{
}

void XGenNSICallbacks::flush( 
	const char* i_geo, 
	PrimitiveCache* i_primCache )
{
	if( i_primCache->get( XGenRenderAPI::PrimitiveCache::PrimIsSpline ) )
	{
		flushSplines( i_geo, i_primCache );
	}
	else if( i_primCache->get( XGenRenderAPI::PrimitiveCache::PrimIsArchive ) )
	{
		flushArchives( i_geo, i_primCache );
	}
	else
	{
		const char* primTypeStr = 
			i_primCache->get( XGenRenderAPI::PrimitiveCache::PrimitiveType );

		if( strcmp( primTypeStr, "CardPrimitive" ) == 0 )
		{
			flushCards( i_geo, i_primCache );
		}
		else if( strcmp( primTypeStr, "SpherePrimitive" ) == 0 )
		{
			flushSpheres( i_geo, i_primCache );
		}
	}
}

/*
	If i_name begins with i_prefix, return true and put the rest of i_name in
	o_suffix. Otherwise, return false.
*/
bool split_name(
	const std::string &i_name,
	const char *i_prefix,
	std::string &o_suffix)
{
	size_t n = strlen(i_prefix);
	if( n > i_name.size() )
		return false;

	if( i_name.substr(0, n) == i_prefix )
	{
		o_suffix = i_name.substr(n, std::string::npos);
		return true;
	}

	return false;
}

void XGenNSICallbacks::flushSplines(
	const char* i_geo,
	PrimitiveCache* i_primCache )
{
	// Create the NSI primitive node right away. Even if in the end we are unable
	// to fill it with data, the scene export code expects that it will
	// connect to a parent.
	//
	m_ctx.m_nsi.Create( m_nsiNodeName.c_str(), "cubiccurves" );
	m_ctx.m_nsi.SetAttribute( m_nsiNodeName,
		NSI::CStringPArg( "basis", "b-spline" ) );

	int numCurves = i_primCache->get( PrimitiveCache::CacheCount );

	if( numCurves == 0 )
	{
		return;
	}

	// Get the number of motion samples and their time offsets
	int numMotionSamples = i_primCache->get( PrimitiveCache::NumMotionSamples );
	const float* timeOffsets = i_primCache->get( PrimitiveCache::Shutter );

	for( int s = 0; s < numMotionSamples; s++ )
	{
		const int* nvertices = i_primCache->get( PrimitiveCache::NumVertices, s ); 
		const vec3* points = i_primCache->get( PrimitiveCache::Points, s );

		// In case we have a weird situation where the curves don't have the same
		// number of vertices. The NSI primitive expects a constant number of verties
		// for all curves.
		/*
			FIXME : This is not true anymore, but the fix is not obvious as it's
			now this export function that doesn't support per-curve nvertices!
		*/
		int numVertices = nvertices[0];
		for( int i = 1; i < numCurves; i++ )
		{
			if( numVertices != nvertices[ i ] )
			{
				return;
			}
		}

		NSI::ArgumentList nsi_attributes;

		// Gather nvertices and P
		//
		int numPoints = numCurves * numVertices;
		nsi_attributes.push( new NSI::IntegerArg( "nvertices", numVertices ) );
		nsi_attributes.push( 
			NSI::Argument::New( "P" )
				->SetType( NSITypePoint )
				->SetCount( numPoints )
				->SetValuePointer( points ) );

		/*
			Width are specifed as varying or as a constant, depending on the ConstWidth
			XGen flag.
		*/
		std::vector<float> widths;
		widths.reserve( numVertices );

		/*
			Not using i_primCache->get(PrimitiveCache::ConstWidth) == true here
			but replicating the handling of varying / constant width as it is 
			done in theXgRendermanProcedural.cpp. This seems to work better with
			XGen libs from Maya 2017 and up.
		*/
		int numXGenWidths = i_primCache->getSize( PrimitiveCache::Widths );
		if( numXGenWidths > 0 )
		{
			const float* xgenWidths = i_primCache->get( PrimitiveCache::Widths );

			if( numXGenWidths == numCurves * ( numVertices - 2 ) )
			{
				/*
					Convert from RenderMan::varying to NSI's per-vertex 
					attribute by duplicating end points.
				*/
				int curr_width = 0;
				for( int i = 0; i < numCurves; i++ )
				{
					// Provide the first width twice
					widths.push_back( xgenWidths[ curr_width ] );

					for( int j = 0; j < numVertices - 2; j++ )
					{
						widths.push_back( xgenWidths[ curr_width++ ] );
					}

					// Provide the last width twice
					curr_width--;
					widths.push_back( xgenWidths[ curr_width++ ] );
				}
			}

			nsi_attributes.push(
				NSI::Argument::New( "width" )
					->SetType( NSITypeFloat )
					->SetCount( widths.size() )
					->SetValuePointer( &widths[0] ) );			
		}
		else
		{
			float constant_width = 0.0f;
			constant_width = i_primCache->get( PrimitiveCache::ConstantWidth );
			nsi_attributes.push( new NSI::FloatArg( "width", constant_width ) );
		}

		// Gather normals, only if relevant i.e. when the Face Camera option is off.
		if( !i_primCache->get( PrimitiveCache::FaceCamera ) )
		{
			const vec3* xgenNormals = i_primCache->get( PrimitiveCache::Norms, 0 );


			/* Same varying -> per-vertex conversion as for the width. */
			std::vector<vec3> normals;
			normals.reserve( numVertices );

			int curr_normal = 0;
			for( int i = 0; i < numCurves; i++ )
			{
				// Provide the first width twice
				normals.push_back( xgenNormals[ curr_normal ] );

				for( int j = 0; j < numVertices - 2; j++ )
				{
					normals.push_back( xgenNormals[ curr_normal++ ] );
				}

				// Provide the last width twice
				curr_normal--;
				normals.push_back( xgenNormals[ curr_normal++ ] );
			}

			nsi_attributes.push(
				NSI::Argument::New( "N" )
					->SetType( NSITypeNormal )
					->SetCount( normals.size() )
					->SetValuePointer( &normals[0] ) );
		}

		/*
			Now output surface variables. Those are attached to the root of each
			curve and hence are uniform. As an  example, the (u_XS,v_XS) is
			provided to have a surface texture coordinate at the surface point.
			This might be used to access a texture file to colour each hair
			strands.
		*/

		struct attrdecl
		{
			attrdecl( int xgenname, NSIType_t nsitype, const char *name )
				:  m_xgenname(xgenname), m_nsitype(nsitype), m_name(name)
			{
			}

			int m_xgenname;
			NSIType_t m_nsitype;
			const char *m_name;
		};

		attrdecl surface_attributes[] =
		{
			attrdecl( PrimitiveCache::RandomInt_XP, NSITypeInteger,
				"RandomInt_XP" ),
			attrdecl( PrimitiveCache::RandomFloat_XP, NSITypeFloat,
				"RandomFloat_XP" ),
			attrdecl( PrimitiveCache::Length_XP, NSITypeFloat, "Length_XP" ),
			attrdecl( PrimitiveCache::FaceID_XS, NSITypeInteger, "FaceID_XS" ),
			attrdecl( PrimitiveCache::GeomID_XS, NSITypeInteger, "GeomID_XS" ),
			attrdecl( PrimitiveCache::PrimitiveID_XP, NSITypeInteger,
				"PrimitiveID_XP" ),
			attrdecl( PrimitiveCache::P_XS, NSITypePoint, "P_XS" ),
			attrdecl( PrimitiveCache::Pg_XS, NSITypePoint, "Pg_XS" ),
			attrdecl( PrimitiveCache::Pref_XS, NSITypePoint, "Pref_XS" ),
			attrdecl( PrimitiveCache::N_XS, NSITypePoint, "N_XS" ),
			attrdecl( PrimitiveCache::Nref_XS, NSITypePoint, "Nref_XS" ),
			attrdecl( PrimitiveCache::Ng_XS, NSITypePoint, "Ng_XS" ),
			attrdecl( PrimitiveCache::Nrefg_XS, NSITypePoint, "Nrefg_XS" ),
			attrdecl( PrimitiveCache::dPdu_XS, NSITypeVector, "dPdu_XS" ),
			attrdecl( PrimitiveCache::dPdv_XS, NSITypeVector, "dPdv_XS" ),
			attrdecl( PrimitiveCache::dPdug_XS, NSITypeVector, "dPdug_XS" ),
			attrdecl( PrimitiveCache::dPdvg_XS, NSITypeVector, "dPdvg_XS" ),
			attrdecl( PrimitiveCache::dPduref_XS, NSITypeVector, "dPduref_XS" ),
			attrdecl( PrimitiveCache::dPdvref_XS, NSITypeVector, "dPdvref_XS" ),
			attrdecl( PrimitiveCache::dPdurefg_XS, NSITypeVector,
				"dPdurefg_XS" ),
			attrdecl( PrimitiveCache::dPdvrefg_XS, NSITypeVector,
				"dPdvrefg_XS" ),
			attrdecl( PrimitiveCache::T_XP, NSITypeVector, "T_XP" ),
		};

		for( int i=0;
			i<sizeof(surface_attributes)/sizeof(surface_attributes[0]); i++ )
		{
			int name = surface_attributes[i].m_xgenname;
			const void *attr = 0x0;
			size_t s = 0;

			switch( surface_attributes[i].m_nsitype )
			{
			case NSITypeFloat:
				attr = i_primCache->get(
					PrimitiveCache::EFloatArrayAttribute(name) );
				if( attr)
				{
					s = i_primCache->getSize(
						PrimitiveCache::EFloatArrayAttribute(name) );
				}
				break;
			case NSITypeInteger:
				attr = i_primCache->get(
					PrimitiveCache::EIntArrayAttribute(name) );
				if( attr )
				{
					s = i_primCache->getSize(
						PrimitiveCache::EIntArrayAttribute(name) );
				}
				break;
			case NSITypePoint:
			case NSITypeNormal:
			case NSITypeVector:
				attr = i_primCache->get(
					PrimitiveCache::EVec3ArrayAttribute(name) );
				if( attr )
				{
					s = i_primCache->getSize(
						PrimitiveCache::EVec3ArrayAttribute(name) );
				}
				break;
			default:
				break;
			}

			if( !attr )
			{
				/* This attribute is not selected in XGen. */
				continue;
			}

			nsi_attributes.push(
				NSI::Argument::New( surface_attributes[i].m_name )
					->SetType( surface_attributes[i].m_nsitype )
					->SetCount( s )
					->SetValuePointer( attr ) );
		}

		/*
			Export surface u & v in a single attribute to avoid multiple calls
			to getattribute() in shaders, since both are always used together.
		*/
		const float* u_attr = (float*)i_primCache->get(PrimitiveCache::U_XS);
		size_t u_size = u_attr ? i_primCache->getSize(PrimitiveCache::U_XS) : 0;
		const float* v_attr = (float*)i_primCache->get(PrimitiveCache::V_XS);
		size_t v_size = v_attr ? i_primCache->getSize(PrimitiveCache::V_XS) : 0;
		if(u_size == v_size && u_size == numCurves)
		{
			NSI::Argument* uv_xs =
				NSI::Argument::New("UV_XS")
				->SetArrayType(NSITypeFloat, 3)
				->SetCount(numCurves);
			float* uv_xs_data =
				(float*)uv_xs->AllocValue(3*numCurves*sizeof(float));

			float uv_bbox[4] = { FLT_MAX, FLT_MAX, -FLT_MAX, -FLT_MAX };

			for(int c = 0; c < numCurves; c++)
			{
				float u = u_attr[c];
				float v = v_attr[c];
				uv_xs_data[3*c] = u;
				uv_xs_data[3*c+1] = v;
				uv_bbox[0] = std::min(uv_bbox[0], u);
				uv_bbox[1] = std::min(uv_bbox[1], v);
				uv_bbox[2] = std::max(uv_bbox[2], u);
				uv_bbox[3] = std::max(uv_bbox[3], v);
			}

			/*
				Compute an approximation for the footprint of each curve on the
				attached surface's "uv" domain. It will be used by 3Delight to
				compute derivatives for the texture coordinates, which will end
				up as an area for the texture lookup. This is repeated for each
				"uv" coordinate in case we eventually need a more accurate
				computation.
				That could be achieved by splitting the surface in multiple
				areas or computing the average distance of the N nearest
				neighbors for each "uv" coordinate.
			*/
			float uv_xs_area =
				std::max(0.0f, uv_bbox[2] - uv_bbox[0]) *
				std::max(0.0f, uv_bbox[3] - uv_bbox[1]);
			float uv_xs_curve_footprint =
				uv_xs_area / float(numCurves) * kFootprintMultiplier;
			for(int c = 0; c < numCurves; c++)
			{
				uv_xs_data[3*c+2] = uv_xs_curve_footprint;
			}

			nsi_attributes.push(uv_xs);
		}

		if( numMotionSamples > 1 )
		{
			m_ctx.m_nsi.SetAttributeAtTime(
				m_nsiNodeName, timeOffsets[ s ], nsi_attributes );
		}
		else
		{
			m_ctx.m_nsi.SetAttribute( m_nsiNodeName, nsi_attributes );
		}
	}

	/* Export custom attributes. */
	auto num_custom_attr = i_primCache->getSize(
		PrimitiveCache::CustomAttrNames);
	/* This should match but take the min of both just in case. */
	num_custom_attr = std::min(num_custom_attr, i_primCache->getSize(
		PrimitiveCache::CustomAttrValues));
	for( decltype(num_custom_attr) i = 0; i < num_custom_attr; ++i )
	{
		std::string xgen_attr_name = i_primCache->get(
			PrimitiveCache::CustomAttrNames, i);
		auto nfloat = i_primCache->getSize2(PrimitiveCache::CustomAttrValues, i);
		std::string attr_name;
		if( split_name(xgen_attr_name, "uniform color ", attr_name) )
		{
			auto nvalues = nfloat / 3;
			const float *values = i_primCache->get(
				PrimitiveCache::CustomAttrValues, i);
			m_ctx.m_nsi.SetAttribute(m_nsiNodeName,
				*NSI::Argument(attr_name)
				.SetType(NSITypeColor)
				->SetCount(nvalues)
				->SetValuePointer(values));
		}
	}
}

void XGenNSICallbacks::flushArchives( 
	const char* i_geo, 
	PrimitiveCache* i_primCache )
{

}

void XGenNSICallbacks::flushCards( 
	const char* i_geo, 
	PrimitiveCache* i_primCache )
{

}

void XGenNSICallbacks::flushSpheres( 
	const char* i_geo, 
	PrimitiveCache* i_primCache )
{

}

void XGenNSICallbacks::log( const char* in_str )
{
	LOGVAMESSAGE(eLevelInfo, "XGen: %s", in_str);
}

bool XGenNSICallbacks::get( EBoolAttribute ) const
{
	// todo
	return false;
}

const char* XGenNSICallbacks::get( EStringAttribute i_attr ) const
{
	if( i_attr == RenderCam )
	{
		/* Return string value for camera position - irRenderCam */
		return m_cameraInfo.getPositionAsString();
	}
	else if( i_attr == RenderCamFOV )
	{
		/* Return string value for cmaera FOV  - irRenderCamFOV */
		return m_cameraInfo.getFOVAsString();
	}
	else if( i_attr == RenderCamXform )
	{
		/* Return string value for camera inverse transform - irRenderCamXform */
		return m_cameraInfo.getTransformAsString();
	}
	else if( i_attr == RenderCamRatio )
	{
		/* Return string value for camera ratio - irRenderCamRatio */
		return m_cameraInfo.getRatioAsString();
	}

	return "";
}

float XGenNSICallbacks::get( EFloatAttribute ) const
{
	// todo
	return 0.0f;
}

const float* XGenNSICallbacks::get( EFloatArrayAttribute ) const
{
	// todo
	return NULL;
}

unsigned int XGenNSICallbacks::getSize( EFloatArrayAttribute ) const
{
	// todo
	return 0;
}

const char* XGenNSICallbacks::getOverride( const char* in_name ) const
{
	// todo
	return "";
}

bool XGenNSICallbacks::getArchiveBoundingBox( 
	const char* in_filename, 
	bbox& out_bbox ) const
{
	// todo
	out_bbox.xmin = 0.f;
	out_bbox.xmax = 0.f;
	out_bbox.ymin = 0.f;
	out_bbox.ymax = 0.f;
	out_bbox.zmin = 0.f;
	out_bbox.zmax = 0.f;

	return true;
}

void XGenNSICallbacks::getTransform( float in_time, mat44& out_mat ) const
{

}
