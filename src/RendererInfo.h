/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __rendererinfo__h
#define __rendererinfo__h

#include <FnRendererInfo/plugin/RendererInfoBase.h>
#include <FnRendererInfo/plugin/ShaderInfoCache.h>

#include <FnAttribute/FnGroupBuilder.h>
#include <FnConfig/FnConfig.h>
#include <FnLogging/FnLogging.h>

#include "3Delight/ShaderQuery.h"

#define OutputTypePtc "ptc"

class RendererInfo : public FnKat::RendererInfo::RendererInfoBase
{
public:
	RendererInfo();
	virtual ~RendererInfo();

	static FnKat::RendererInfo::RendererInfoBase* create();

	void fillRenderMethods(
			std::vector<FnKat::RendererInfo::RenderMethod*>& renderMethods)const;

	void fillRendererObjectNames(
			std::vector<std::string>& rendererObjectNames,
			const std::string& type,
			const std::vector<std::string>& typeTags) const;

	void fillRendererObjectTypes(
			std::vector<std::string>& renderObjectTypes,
			const std::string& type) const;

	void configureBatchRenderMethod(
			FnKat::RendererInfo::DiskRenderMethod& batchRenderMethod) const;

	std::string getRegisteredRendererName() const;

	std::string getRegisteredRendererVersion() const;

	bool isPresetLocalFileNeeded(const std::string& outputType) const;

	bool isNodeTypeSupported(const std::string& nodeType) const;

	bool isPolymeshFacesetSplittingEnabled() const;

	void fillShaderInputNames(
			std::vector<std::string>& shaderInputNames,
			const std::string& shaderName) const;

	void fillShaderInputTags(
			std::vector<std::string>& shaderInputTags,
			const std::string& shaderName,
			const std::string& inputName) const;

	void fillShaderOutputNames(
			std::vector<std::string>& shaderOutputNames,
			const std::string& shaderName) const;

	void fillShaderOutputTags(
			std::vector<std::string>& shaderOutputTags,
			const std::string& shaderName,
			const std::string& outputName) const;

	void fillRendererShaderTypeTags(
			std::vector<std::string>& shaderTypeTags,
			const std::string& shaderType) const;

	std::string getRendererCoshaderType() const;

	bool buildRendererObjectInfo(
			FnAttribute::GroupBuilder& rendererObjectInfo,
			const std::string& name,
			const std::string& type,
			const FnAttribute::GroupAttribute inputAttr) const;

	static FnPlugStatus setHost(FnPluginHost* host);

	static void AddOpScript(
		const char *i_script,
		OpDefinitionQueue &terminalOps );

	static void AddOpLocalizeLightListPath( OpDefinitionQueue &terminalOps );

	static void AddOpLightGatherParents( OpDefinitionQueue &terminalOps );

	static void AddOpLightLocalState( OpDefinitionQueue &terminalOps );

	static void AddOpGatherLightFilters( OpDefinitionQueue &terminalOps );

	static void AddOpIncandescenceLightCEL( OpDefinitionQueue &terminalOps );

	static void AddOpNoMaterialLayoutLive( OpDefinitionQueue &terminalOps );

	static void AddOpLocalizeMaterial( OpDefinitionQueue &terminalOps );

	static void AddOpUseLocalMaterial( OpDefinitionQueue &terminalOps );

	static void AddOpApplyVisibility( OpDefinitionQueue &terminalOps );

	static void AddOpLiveGlobalStatements( OpDefinitionQueue &terminalOps );

	virtual void fillLiveRenderTerminalOps(
		OpDefinitionQueue& terminalOps,
		const FnAttribute::GroupAttribute& stateArgs) const;

	void fillRenderTerminalOps(
		OpDefinitionQueue& terminalOps,
		const FnAttribute::GroupAttribute& stateArgs) const;

	void flushCaches() ;

	/* flush per plugin cache */
	static void flush() {};

protected:
	/* Creates parameters to create a ramp.
	   Minimal Katana ramp should be represented by four shading parameters:

	   <int name='colorRamp' default='3' widget='colorRamp'/>
	   <string name='colorRamp_Interpolation' default='linear' widget='null'/>
	   <float name='colorRamp_Knots' default='0,0.5,1' size='3' widget='null'/>
	   <float name='colorRamp_Colors' default='1,0,0,0,1,0,0,0,1' size='9'
		   tupleSize='3' widget='null'/>

	   This method creates colorRamp and colorRamp_Interpolation. For more
	   information see FormMaster/Editors/BaseRamp.py */
	void BuildRampShaderParameters(
			const std::string& i_parameter_name,
			int i_ramp_size,
			const std::string& i_parameter_positions_name,
			const std::string& i_ramp_type,
			const std::string& i_ramp_parameter_hint_name,
			const std::string& i_page,
			FnAttribute::GroupBuilder& io_hints,
			FnAttribute::GroupBuilder& io_renderer_object_info ) const;

	/* Adds a parameter to a Katana's render object. It builds an attribute
	   which describes parameter and its properties such as its type and UI
	   hints. */
	void BuildSingleShaderParameter(
			const DlShaderInfo::Parameter* i_parameter,
			const std::string& i_parameters_prefix,
			FnAttribute::GroupBuilder& io_renderer_object_info,
			std::vector<std::string>& o_color_parameters,
			std::vector<std::string>& o_intensity_parameters,
			std::vector<std::string>& o_exposure_parameters ) const;
};

#endif

