/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2018                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __LightLocator_h
#define __LightLocator_h

/*
    This is an adaptation of the LightLocator example provided with
    Katana.
*/


#include <FnViewer/utils/FnBaseLocator.h>


class LightLocatorVDC : public FnKat::ViewerUtils::FnBaseLocatorVDC
{
public:
    LightLocatorVDC();
    virtual ~LightLocatorVDC() {}

    static void flush() {}

    static FnKat::ViewerAPI::ViewerDelegateComponent* create()
    {
        return new LightLocatorVDC();
    }

    /// Handles viewer location events and maintains the SceneNode tree.
    virtual bool locationEvent(
        const Foundry::Katana::ViewerAPI::ViewerLocationEvent& event,
        bool locationHandled);

};

class LightLocator : public FnKat::ViewerUtils::FnBaseLocator
{
public:
    LightLocator() {}
    virtual ~LightLocator() {}

    static FnKat::ViewerUtils::FnBaseLocator* create()
    {
        return new LightLocator();
    }

    /// Determines whether this locator plug-in should be used to draw the
    /// location described in the given viewer location event.
    static bool matches(const FnKat::ViewerAPI::ViewerLocationEvent& event);

    /// Determines whether this locator plug-in overrides the standard geometry
    /// representation for matched locations.
    static bool overridesBaseGeometry(
        const FnKat::ViewerAPI::ViewerLocationEvent& event);

    /// Gets the bounds of the given location.
    static FnAttribute::DoubleAttribute getBounds(
        const FnKat::ViewerAPI::ViewerLocationEvent& event);

    /// Computes the extent of the given location.
    static FnAttribute::DoubleAttribute computeExtent(
        const FnKat::ViewerAPI::ViewerLocationEvent& event);

    /// Initializes the locator plug-in's resources.
    virtual void setup() {};
    /// Cleans up the locator plug-in's resources.
    virtual void cleanup() {};

    /// Draws the object that's represented by the scene graph location with
    /// the given path using the given shader program.
    virtual void draw(const std::string& locationPath);

    /// Draws the object that's represented by the scene graph location with
    /// the given path using the given shader program as part of a picking
    /// pass.
    virtual void pickerDraw(const std::string& locationPath);

    /// Returns the vertex and index data for a cone.
    void generateCone(float radius,
                      float height,
                      int segments,
                      std::vector<float>& vertices,
                      std::vector<unsigned int>& indices);

private:
   enum eType
    {
        type_unknown, type_area, type_distant, type_spot, type_mesh, type_point,
        type_area_disk, type_area_sphere, type_area_cylinder, type_gobo_filter
    };

private:
    eType getLightType( const std::string& i_locationPath );

    /// Draws the actual light geometry
    void drawLight(
        const std::string& locationPath, 
        bool i_drawForPicking, 
        bool i_selected );

    void drawAreaLight( const std::string& i_locationPath );
    void drawDistantLight( const std::string& i_locationPath );
    void drawSpotLight( const std::string& i_locationPath );
    void drawDiskLight( const std::string& i_locationPath );
    void drawSphereLight( const std::string& i_locationPath );
    void drawCylinderLight( const std::string& i_locationPath );
    void drawPointLight(
        const std::string& i_locationPath,
        bool i_drawForPicking,
        bool i_selected );

    void drawGoboFilter( const std::string& i_locationPath );
    
    void setDrawColor( bool i_selected, float i_alpha );

    GLuint m_vao;

    /// The Element Buffer Object containing indices to vertices.
    GLuint m_ebo;

    /// The Vertex Buffer Object containing vertices.
    GLuint m_vbo;

    std::vector<float> m_vertices;
    std::vector<unsigned int> m_indices;
};

#endif
