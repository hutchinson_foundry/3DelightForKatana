################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2014 The Foundry Visionmongers Ltd. All Rights Reserved.

import os
import PackageSuperToolAPI

# Insert our common light package modules into this package.
lc = os.path.dirname(__path__[0])
lc = os.path.dirname(lc)
lc = os.path.dirname(lc)
lc = os.path.join(lc, 'Python', 'LightPackageCommon')
__path__.append(lc)

# Import Package modules
import SkyLightPackage

# Import UIDelegate modules if we are running in UI mode
if PackageSuperToolAPI.IsUIMode():
    import SkyLightUIDelegate

