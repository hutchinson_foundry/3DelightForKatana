# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from PackageSuperToolAPI import UIDelegate
from SkyLightPackage import (SkyLightPackage, SkyLightEditPackage)

# Our module with common light package code.
import EnvironmentLightUIBase

class SkyLightUIDelegate(EnvironmentLightUIBase.EnvironmentLightUIDelegate):
    """
    The UI delegate for the EnvironmentLight package.

    This class is responsible for exposing the parameters on each of the
    parameter tabs. This is done by creating parameter policies attached to the
    parameters on the package's nodes. We can also modify the appearance of the
    parameter tabs by modifying the hints dictionaries on the policies.
    """

    # The hash used to uniquely identify the action of creating a package
    # This was generated using:
    #     hashlib.md5('SkyLight.AddSkyLight').hexdigest()
    AddPackageActionHash = '06271ccb0c007e7d336e0831848a1edf'

    # The keyboard shortcut for creating a package
    DefaultShortcut = 'Y'

# Register the UI delegates
UIDelegate.RegisterUIDelegateClass(SkyLightPackage, SkyLightUIDelegate)
UIDelegate.RegisterUIDelegateClass(
    SkyLightEditPackage, EnvironmentLightUIBase.EnvironmentLightEditUIDelegate)

