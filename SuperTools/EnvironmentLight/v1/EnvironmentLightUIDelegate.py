# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from PackageSuperToolAPI import UIDelegate
from EnvironmentLightPackage import (
    EnvironmentLightPackage, EnvironmentLightEditPackage)

# Our module with common light package code.
import EnvironmentLightUIBase

class EnvironmentLightUIDelegate(
    EnvironmentLightUIBase.EnvironmentLightUIDelegate):
    #
    # The hash used to uniquely identify the action of creating a package
    # This was generated using:
    #     hashlib.md5('EnvironmentLight.AddEnvironmentLight').hexdigest()
    AddPackageActionHash = '9fbc8a11e831503b985ab274889647ea'

    # The keyboard shortcut for creating a package
    DefaultShortcut = 'E'

# Register the UI delegates
UIDelegate.RegisterUIDelegateClass(
    EnvironmentLightPackage, EnvironmentLightUIDelegate)
UIDelegate.RegisterUIDelegateClass(
    EnvironmentLightEditPackage, \
    EnvironmentLightUIBase.EnvironmentLightEditUIDelegate)

