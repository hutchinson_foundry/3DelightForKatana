################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import Nodes3DAPI, NodegraphAPI, Utils, UniqueName, KatanaFile
try:
	from Katana import QtWidgets
except ImportError:
	# Qt4 fallback
	from Katana import QtGui as QtWidgets
from PackageSuperToolAPI import NodeUtils

from Upgrade import Upgrade
import ScriptActions as SA
import logging

_3DELIGHT_RENDERER_NAME = "dl"

log = logging.getLogger(_3DELIGHT_RENDERER_NAME.title() + "Settings.Node")


class DelightSettingsNode(NodegraphAPI.SuperTool):

	warning1 = 'Only nodes of type "Material" or "NetworkMaterial" are supported.'
	warning2 = 'Node "Material" or "NetworkMaterial" should be a shader of type "dlVolumeShader".'

	def __init__(self):
		self.hideNodegraphGroupControls()
		self.addInputPort('input')
		self.addOutputPort('out')

		# Hidden version parameter to detect out-of-date internal networks
		# and upgrade it.
		self.getParameters().createChildNumber('version', 1)

		# Parameter for parent location of all the ponies.
		self.getParameters().createChildString('location', '/root/world/geo')

		self.BuildDefaultNetwork()
		self.AddImageLayer('primary', 'Ci', 'exr', 'exr')

		Utils.EventModule.RegisterCollapsedHandler(
			self.UpdateCamLocation1EH, 'port_connect')
		Utils.EventModule.RegisterCollapsedHandler(
			self.UpdateCamLocation2EH, 'parameter_setValue')
		Utils.EventModule.RegisterCollapsedHandler(
			self.ValidateEH, 'parameter_setValue')

	#
	# Update location of camera_node when DlSettings is connected, or
	# internal nodes RenderSettings/DlCameraSettings.
	# We assume that cameraName is synchonized with the camera location...
	#
	def UpdateCamLocation1EH(self, args):
		values = args[0][2].values()
		if 'DlSettings' not in values and \
		'RenderSettings' not in values and \
		'DlCameraSettings' not in values:
			return

		camera_name = '/root/world/cam/camera'
		root = Nodes3DAPI.GetGeometryProducer(self)
		if root:
			group_attrs = root.getFnAttribute('renderSettings')
			if group_attrs:
				str_attr = group_attrs.getChildByName('cameraName')
				if str_attr:
					camera_name = str_attr.getValue()

		camera_node = SA.GetRefNode(self, "camera_settings")
		if camera_node:
			camera_node.getParameter('location').setValue(camera_name, 0)
	#
	# Update location of camera_node when RenderSettings's cameraName is
	# changed.
	# We assume that cameraName is synchonized with the camera location...
	#
	def UpdateCamLocation2EH(self, args):
		if 'node' not in args[0][2]:
			return
		node = args[0][2]['node']
		if node.getType() != 'RenderSettings':
			return

		if 'param' not in args[0][2]:
			return
		param = args[0][2]['param']

		members = dir(param)
		if 'getFullName' not in members or 'getValue' not in members:
			return

		fullname = param.getFullName()
		if 'cameraName.value' not in fullname:
			return

		camera_name = param.getValue(0)
		if not camera_name:
			return

		camera_node = SA.GetRefNode(self, "camera_settings")
		if camera_node:
			camera_node.getParameter('location').setValue(camera_name, 0)

	def Warning(self, message, param):
		QtWidgets.QMessageBox.warning(None, 'warning', message)
		param.setExpressionFlag(False)
		param.setValue('', 0)

	#
	# Use to validate any drop on parameter assignAtmosphere 
	#
	def ValidateEH(self, args):
		param = args[0][2]['param']
		fullName = param.getFullName()
		if 'assignAtmosphere.enable' in fullName:
			if 'param' not in args[1][2]:
				return
			param = args[1][2]['param']
			fullName = param.getFullName()
		if 'assignAtmosphere.value' in fullName:
			location = param.getValue(0)
			# No location means when reset this parameter
			if not location:
				return
			index = location.rfind('/')
			nodeName = location[index+1:]
			if not nodeName:
				self.Warning(self.warning1, param)
				return
			# Assume that the leaf of location is the node's name
			node = NodegraphAPI.GetNode(nodeName)
			if not node:
				self.Warning(self.warning1, param)
				return
			type = node.getType()
			# Check the node's type
			if type != 'Material' and type != 'NetworkMaterial':
				self.Warning(self.warning1, param)
				return
			# Check for the correct shader's type
			nodes = NodegraphAPI.GetAllNodesByType(type)
			for node in nodes:
				if node.getName() == nodeName:
					root = Nodes3DAPI.GetGeometryProducer(node)
					myProd = root.getProducerByPath(location) 
					group_attrs = myProd.getFnAttribute('material')
					if group_attrs:
						if type == 'Material':
							group2_attrs = group_attrs.getChildByName('dlVolumeShader')
							if not group2_attrs:
								self.Warning(self.warning2, param)
						else:
							group2_attrs = group_attrs.getChildByName('terminals')
							if group2_attrs:
								vol_attr = group2_attrs.getChildByName('dlVolume')
								if not vol_attr:
									self.Warning(self.warning2, param)
					break

	def BuildDefaultNetwork(self):
		# Add a Merge node before the RenderSettings node. This is merely to
		# provide some 3D input to RenderSettings; with such an input, it will
		# initialize correctly and our 'renderer' = 'dl' assignation will
		# actually add our extra attributes in renderOutputDefine nodes down
		# the chain.
		#
		mergeNode = NodegraphAPI.CreateNode('Merge', self)
		mergeNode.addInputPort('in')
		mergeNode.setName('3DInput')

		# Add the RenderSettings node
		settingsNode = NodegraphAPI.CreateNode('RenderSettings', self)
		SA.AddNodeReferenceParam(self, 'node_render_settings', settingsNode)

		# Set 3Delight as the current renderer
		renderer = settingsNode.getParameter(
				"args.renderSettings.renderer.value")
		renderer.setExpression('"' + _3DELIGHT_RENDERER_NAME + '"')
		renderer.setExpressionFlag(True)
		renderer = settingsNode.getParameter(
				"args.renderSettings.renderer.enable")
		renderer.setValue(True, 0)

		# Connect this node to the group
		settingsNode.getInputPortByIndex(0).connect(
				self.getSendPort(self.getInputPortByIndex(0).getName()) )

		# Add DlCameraSettings and connect this node to the previous node
		camera_node = NodegraphAPI.CreateNode(
			_3DELIGHT_RENDERER_NAME.title() + 'CameraSettings',
			self)
		SA.AddNodeReferenceParam(self, 'node_camera_settings', camera_node)
		camera_node.getInputPortByIndex(0).connect(
				settingsNode.getOutputPortByIndex(0) )

		globals_node = \
				NodegraphAPI.CreateNode(
						_3DELIGHT_RENDERER_NAME.title() + 'GlobalSettings',
						self)
		SA.AddNodeReferenceParam(self, 'node_render_globals', globals_node)

		globals_node.getInputPortByIndex(0).connect(
				camera_node.getOutputPortByIndex(0) )
		self.getReturnPort(self.getOutputPortByIndex(0).getName()).connect(
				globals_node.getOutputPortByIndex(0))

		# Set expression for maxTimeSamples
		maxTimeSamples = settingsNode.getParameter(
				"args.renderSettings.maxTimeSamples.value")
		quality = \
			'getNode("' + \
			globals_node.getName() + \
			'").args.' + \
			_3DELIGHT_RENDERER_NAME + \
			'GlobalStatements.quality.'
		camera = \
			'getNode("' + \
			globals_node.getName() + \
			'").args.' + \
			_3DELIGHT_RENDERER_NAME + \
			'GlobalStatements.camera.'
		expression = \
			'(' + quality + 'motionSamples.value ' + \
			'if ' + quality + 'motionSamples.enable ' + \
			'else ' + quality + 'motionSamples.default) '
		maxTimeSamples.setExpression( expression )
		maxTimeSamples.setExpressionFlag(True)
		maxTimeSamples = settingsNode.getParameter(
				"args.renderSettings.maxTimeSamples.enable")
		maxTimeSamples.setValue(True, 0)

		# Set expression for shutterOpen
		shutterOpen = settingsNode.getParameter(
				"args.renderSettings.shutterOpen.value")
		expression = \
			'- (' + camera + 'shutterAngle.value ' + \
			'if ' + camera + 'shutterAngle.enable ' + \
			'else ' + camera + 'shutterAngle.default) / 360.0 / 2.0'
		shutterOpen.setExpression( expression )
		shutterOpen.setExpressionFlag(True)
		shutterOpen = settingsNode.getParameter(
				"args.renderSettings.shutterOpen.enable")
		shutterOpen.setValue(True, 0)

		# Set expression for shutterClose
		shutterClose = settingsNode.getParameter(
				"args.renderSettings.shutterClose.value")
		expression = \
			'(' + camera + 'shutterAngle.value ' + \
			'if ' + camera + 'shutterAngle.enable ' + \
			'else ' + camera + 'shutterAngle.default) / 360.0 / 2.0'
		shutterClose.setExpression( expression )
		shutterClose.setExpressionFlag(True)
		shutterClose = settingsNode.getParameter(
				"args.renderSettings.shutterClose.enable")
		shutterClose.setValue(True, 0)

	# Public API - used by the UI, but can also be called directly by scripts
	
	#
	# Add a new image layer (AOV) in the image layer list.
	#
	def AddImageLayer( self, i_name, i_channel, i_ext, i_driver ):
		Utils.UndoStack.OpenGroup(
			'Add a render layer to node "%s"' % self.getName())

		try:
			# Create the layer group
			group = SA.AddLayerGroup( self, i_name, i_channel )

			# Make sure that everything in the SuperTool so far is properly
			# connected. This must be done before we attempt to set any image
			# layer parameter else some renderOutputDefine attributes may not 
			# be found.
			#
			self.CleanupInternalNetwork()
			 
			# Add the relevant output nodes to it
			SA.AddLayerNodeToGroup( 
				self, group, i_channel, i_ext, i_driver )
			self.RebuildOutputsInLayerGroup(group)
			return group

		finally:
			Utils.UndoStack.CloseGroup()
			self.EnableLayers()

	def RenameLayer(self, i_n, i_name):
		name = str(i_name)
		group = self.GetLayerGroup(i_n)
		name_param = group.getParameter("imagelayername")
		name_param.setValue(name, 0)

		for node in self.GetLayerNodes(i_n):
			SA.SetOutputNodeParameters(node)

		self.EnableLayers()

	def EnableLayers(self):
		names = ""
		for layer_name in SA.GetLayerNodesNames( self ):
			if len(names) == 0:
				names = layer_name
			else:
				names += "," + layer_name

		settingsNode = SA.GetRefNode(self, "render_settings")

		# Enable custom value
		interactiveOutputs = settingsNode.getParameter(
				"args.renderSettings.interactiveOutputs.enable")
		interactiveOutputs.setValue(True, 0)

		# Set the layers
		interactiveOutputs = settingsNode.getParameter(
				"args.renderSettings.interactiveOutputs.value")
		interactiveOutputs.setValue(names, 0)

	#
	# Remove the i_index'th image layer from the image layer list.
	#
	def RemoveImageLayer( self, i_index ):
		Utils.UndoStack.OpenGroup(
			'Remove the render layer of node "%s"' % self.getName())

		try:
			group_to_remove = SA.GetLayerGroups( self )[ i_index ]
			if group_to_remove:
				group_to_remove.delete()

		finally:
			Utils.UndoStack.CloseGroup()
			self.EnableLayers()
			self.CleanupInternalNetwork()

	def GetLayerGroup( self, i_number ):
		current = 0
		for group in SA.GetLayerGroups(self):
			if current < i_number:
				current += 1
				continue

			return group

	def GetLayerNodes( self, i_number ):
		current = 0
		for group in SA.GetLayerGroups(self):
			if current < i_number:
				current += 1
				continue

			return SA.GetLayerNodes(group)

	def GetLayerLastNode( self, i_group ):
		return i_group.getChildren()[len(i_group.getChildren())-1]

	def SetLightParam(self, i_node, i_name):
		light_name = \
			SA.rendererSettings_prefix + "lightSet."
		enable = i_node.getParameter(light_name+"enable")
		enable.setValue(True, 0)
		value = i_node.getParameter(light_name+"value")
		value.setValue(i_name, 0)

	def SetCameraParam(self, i_node, i_name):
		light_name = SA.rendererSettings_prefix + 'cameraName.'
		i_node.getParameter(light_name + 'enable').setValue(True, 0)
		i_node.getParameter(light_name + 'value').setValue(i_name, 0)

	# Add a light to the multi-light selection and to all shading component 
	# layer groups.
	def AddMultiLight(self, i_light):
		self.AddLightGroup(i_light)

		for curr_group in SA.GetShadingComponentLayerGroups(self):
			self.RebuildOutputsInLayerGroup(curr_group)

	# Removes the specified light from the multi-light selection. This also
	# removes output nodes related to the specified light layer from all layer
	# groups.
	def RemoveMultiLight(self, i_light):
		self.RemoveLightGroup(i_light)

		for curr_group in SA.GetShadingComponentLayerGroups(self):
			self.RebuildOutputsInLayerGroup(curr_group)

	def AddMultiCamera(self, i_camera):
		self.AddAltCamera(i_camera)

		for curr_group in SA.GetLayerGroups(self):
			self.RebuildOutputsInLayerGroup(curr_group)

	def RemoveMultiCamera(self, i_camera):
		self.RemoveAltCamera(i_camera)

		for curr_group in SA.GetLayerGroups(self):
			self.RebuildOutputsInLayerGroup(curr_group)

	def RebuildOutputsInLayerGroup(self, i_group):
		"""
		Build the secondary outputs for multi-light and multi-camera from the
		primary output in a layer group.
		"""
		# Remove any previous secondary outputs. Assumes the primary is first.
		outputs = SA.GetLayerNodes(i_group)
		main_output = outputs.pop(0)
		for node in outputs:
			node.delete()

		cameras = self.GetAltCameras()
		cameras.insert(0, '') # placeholder for render camera
		light_groups = []
		# Multi-light is only for AOVs which are shading components.
		if SA.AOVIsShadingComponent(SA.GetLayerGroupAOV(i_group)):
			light_groups = self.GetLightGroups()

		# Copy the node for duplication below.
		nodexml = NodegraphAPI.BuildNodesXmlIO([main_output])

		for camera in cameras:
			# If not main camera, add an output for the camera.
			if camera != '':
				KatanaFile.Paste(nodexml, i_group)
				pasted_node = self.GetLayerLastNode(i_group)
				# Should not be necessary, but just to be safe
				pasted_node.checkDynamicParameters()
				# Set cameraName parameter of new node.
				self.SetCameraParam(pasted_node, camera)
				# Copy this node for the multi-light
				nodexml = NodegraphAPI.BuildNodesXmlIO([pasted_node])
				# Update fields which depend on camera name.
				SA.SetOutputNodeParameters(pasted_node)

			# Add the outputs for the lights.
			for light in light_groups:
				KatanaFile.Paste(nodexml, i_group)
				pasted_node = self.GetLayerLastNode(i_group)
				# Should not be necessary, but just to be safe
				pasted_node.checkDynamicParameters()
				# Set light parameter of new node.
				self.SetLightParam(pasted_node, light)
				# Update fields which depend on light set.
				SA.SetOutputNodeParameters(pasted_node)

		# Reconnect it all.
		NodeUtils.WireInlineNodes(i_group, i_group.getChildren(), 0, 25)
		# Enable all those outputs.
		self.EnableLayers()

	def _getStringArray(self, param):
		"""
		Returns a list with the values of a dynamic string array parameter.
		"""
		globals_node = SA.GetRefNode(self, "render_globals")
		value_param = globals_node.getParameter(param)
		values = []
		for i in range(value_param.getNumChildren()):
			group = value_param.getChildByIndex(i).getValue(0)
			if group != '':
				values.append(group)
		return values

	def _addToStringArray(self, param, value):
		"""
		Add a value to a dynamic string array parameter.
		"""
		globals_node = SA.GetRefNode(self, "render_globals")

		enable_param = globals_node.getParameter(param + '.enable')
		enable_param.setValue(1, 0)

		value_param = globals_node.getParameter(param + '.value')
		new_item = value_param.insertArrayElement(value_param.getNumChildren())
		new_item.setValue(value, 0)

		default_param = globals_node.getParameter(param + '.default')
		new_item = default_param.insertArrayElement(
			default_param.getNumChildren())
		new_item.setValue('', 0)

	def _removeFromStringArray(self, param, value):
		"""
		Remove a value from a dynamic string array parameter.
		"""
		globals_node =  SA.GetRefNode(self, "render_globals")

		value_param = globals_node.getParameter(param + '.value')
		default_param = globals_node.getParameter(param + '.default')

		for i in range(value_param.getNumChildren()):
			curr_value = value_param.getChildByIndex(i).getValue(0)
			if curr_value == value:
				value_param.removeArrayElement(i)
				default_param.removeArrayElement(i)
				break

	# Returns the list of scene graph locations used for multi-light stored in
	# the DlGlobalSettings parameter.
	def GetLightGroups(self):
		return self._getStringArray(
			'args.dlGlobalStatements.layers.multilight.lightgroups.value')

	# Adds a scene graph location used for multi-light to the DlGlobalsSettings
	# parameter.
	# For internal use only.
	def AddLightGroup(self, i_group):
		existing_groups = self.GetLightGroups()
		if i_group in existing_groups:
			return
		self._addToStringArray(
			'args.dlGlobalStatements.layers.multilight.lightgroups',
			i_group)

	# Removes the specified scene graph location from the multi-light list
	# stored in the DlGlboalSettings parameter.
	# For internal use only.
	def RemoveLightGroup(self, i_group):
		self._removeFromStringArray(
			'args.dlGlobalStatements.layers.multilight.lightgroups',
			i_group)

	# Returns the list of scene graph locations used for multi-camera stored in
	# the DlGlobalSettings parameter.
	def GetAltCameras(self):
		return self._getStringArray(
			'args.dlGlobalStatements.layers.multicamera.cameras.value')

	# Add SGL to the multi-camera list on DlGlobalSettings node.
	def AddAltCamera(self, i_camera):
		existing_cameras = self.GetAltCameras()
		if i_camera in existing_cameras:
			return
		self._addToStringArray(
			'args.dlGlobalStatements.layers.multicamera.cameras',
			i_camera)

	# Removes SGL from the multi-camera list on DlGlobalSettings node.
	def RemoveAltCamera(self, i_camera):
		self._removeFromStringArray(
			'args.dlGlobalStatements.layers.multicamera.cameras',
			i_camera)

	def CleanupInternalNetwork(self):
		NodeUtils.WireInlineNodes(self, self.getChildren(), 0, 25)

	def Upgrade(self):
		if not self.isLocked():
			Upgrade(self)
		else:
			log.warning(
				'Cannot upgrade locked %sSettings node "%s".'%(
					_3DELIGHT_RENDERER_NAME, self.getName()) )
