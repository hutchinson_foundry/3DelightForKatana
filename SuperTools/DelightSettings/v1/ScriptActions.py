################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import FarmAPI, NodegraphAPI, Utils, Nodes3DAPI
from PackageSuperToolAPI import NodeUtils
from itertools import izip
import os

_3DELIGHT_RENDERER_NAME = "dl"

# List of available display drivers. Name in the UI, extension, driver
display_drivers = (
	("TIFF", "tif", "tiff", ("8-bit", "16-bit", "32-bit")),
	("OpenEXR", "exr", "exr", ("16-bit", "32-bit")),
	("OpenEXR (deep)", "exr", "deepexr", ("32-bit")) )

# AOVs. outputName, variable, token string, layertype, variablesource, withAlpha
shading_components = (
	# "RGBA (color + alpha)": ("rgba", "rgba"),
	("primary", "Ci", "primary", "color", "shader", 1),
	("Diffuse", "diffuse", "diffuse", "color", "shader", 0),
	("Subsurface scattering", "subsurface", "subsurface", "color", "shader", 0),
	("Reflection", "reflection", "reflection", "color", "shader", 0),
	("Refraction", "refraction", "refraction", "color", "shader", 0),
	("Volume scattering", "volume", "volume", "color", "shader", 0),
	("Incandescence", "incandescence", "incandescence", "color", "shader", 0),
	("Toon Base", "toon_base", "toon_base", "color", "shader", 0),
	("Toon Diffuse", "toon_diffuse", "toon_diff", "color", "shader", 0),
	("Toon Specular", "toon_specular", "toon_spec", "color", "shader", 0),
	("Outlines", "outlines", "outlines", "quad", "shader", 0) )

# layertype should probably be vector for P, N and UV
auxiliary_components = (
	("Z (depth)", "z", "zdepth", "scalar", "builtin", 0),
	("Camera space position", "P.camera", "position", "vector", "builtin", 0),
	("Camera space normal", "N.camera", "normal", "vector", "builtin", 0),
	("UV", "uv", "uv", "color", "builtin", 0),
	("Geometry Cryptomatte", "id.geometry", "geoid", "scalar", "builtin", 0),
	("Scene Path Cryptomatte", "id.scenepath", "scenepathid", "scalar", "builtin", 0),
	("Surface Shader Cryptomatte", "id.surfaceshader", "surfaceid", "scalar", "builtin", 0),
	("Relighting Multiplier", "relighting_multiplier", "relightingmultiplier", "color", "shader", 0),
	("Relighting Reference", "relighting_reference", "relightingref", "color", "shader", 0),
	("Motion Vector", "motionvector", "motion", "vector", "builtin", 0) )

# List of available image depths
quantizes = ("8-bit", "16-bit", "32-bit")

outputName_prefix = \
	"args." + \
	"renderSettings." + \
	"outputs." + \
	"outputName."

rendererSettings_prefix = outputName_prefix + "rendererSettings."
locationSettings_prefix = outputName_prefix + "locationSettings."

# Functions used by both the node and editor, but not part of the public API.

# Returns the AOV definition tuple that matches the 'aov' variable.
def GetAOVDefinitionTuple( aov ):
	aov_tuple = ( )
	for i in range( 0, len(shading_components) ):
		if shading_components[i][1] == aov:
			aov_tuple = shading_components[i]
			return aov_tuple

	for i in range( 0, len(auxiliary_components) ):
		if auxiliary_components[i][1] == aov:
			aov_tuple = auxiliary_components[i]
			return aov_tuple

	return aov_tuple

# Returns the label as defined in the aov tuple
def GetAOVLabel( aov_tuple ):
	return aov_tuple[0]

# Returns the aov variable name as defined in the aov tuple
def GetAOVVariable( aov_tuple ):
	return aov_tuple[1]

# Returns the aov token string as defined in the aov tuple
def GetAOVToken( aov_tuple ):
	return aov_tuple[2]

# Returns the aov layer type as defined in the aov tuple
def GetAOVLayerType( aov_tuple ):
	return aov_tuple[3]

# Returns the aov variable source as defined in the aov tuple
def GetAOVVariableSource( aov_tuple ):
	return aov_tuple[4]

# Returns the withAlpha value as defined in the aov tuple
def GetAOVWithAlpha( aov_tuple ):
	return aov_tuple[5]

# We use hidden parameters on the SuperTool node to reference internal nodes
# using an expression.  Since expressions automatically track node renames,
# evaluating the parameter value will always give you the correct name for
# the internal node.
def GetRefNode(gnode, key):
    p = gnode.getParameter('node_'+key)
    if not p:
        return None
    
    return NodegraphAPI.GetNode(p.getValue(0))

def AddNodeReferenceParam(destNode, paramName, node):
    param = destNode.getParameter(paramName)
    if not param:
        param = destNode.getParameters().createChildString(paramName, '')
    
    param.setExpression('getNode(%r).getNodeName()' % node.getName())

# Returns all layer groups
def GetLayerGroups( i_node ):
	"""
	i_node: The DlSettings supertool node.
	"""
	children = i_node.getChildren()
	groups = []

	for child in children:
		if( child.getType() == 'Group' ):
			name_param = child.getParameter("imagelayername")
			if( name_param ):
				groups.append( child )

	return groups

# Returns layer group named i_layername
def GetLayerGroup( i_node, i_layernamne ):
	groups = GetLayerGroups( i_node )

	for group in groups:
		layername_param = group.getParameter("imagelayername")
		if( layername_param.getValue(0) == i_layernamne ):
			return group

	return None

# Returns all layer groups with aov named i_aov
def GetLayerGroupsForAOV( i_node, i_aov ):
	all_groups = GetLayerGroups( i_node )
	groups = [ ]

	for group in all_groups:
		aov_param = group.getParameter("aov")
		if( aov_param.getValue(0) == i_aov ):
			groups.append( group )

	return groups

# Returns the AOV declaration used by the specified layer group
def GetLayerGroupAOV( i_group ):
	aov_param = i_group.getParameter("aov")
	return aov_param.getValue(0)

# Returns all defined layger groups that are using a shading component AOV.
def GetShadingComponentLayerGroups( i_node ):
	all_groups = GetLayerGroups( i_node )
	groups = [ ]

	for group in all_groups:
		aov = GetLayerGroupAOV( group )
		if AOVIsShadingComponent( aov ):
			groups.append( group )

	return groups

# Returns all output nodes of the specified layer group
def GetLayerNodes( i_group ):
	return i_group.getChildren()

# Returns names of output nodes for all layer groups
def GetLayerNodesNames( i_node ):
	groups = GetLayerGroups( i_node )
	names = [ ]

	for group in groups:
		for node in GetLayerNodes(group):
			name_param = node.getParameter("outputName")
			if name_param:
				names.append( name_param.getValue(0) )
	
	return names

# Returns names of all layer groups
def GetLayerGroupNames( i_node ):
	groups = GetLayerGroups( i_node )
	names = [ ]
	for group in groups:
		name_param = group.getParameter("imagelayername")
		if name_param:
			names.append( name_param.getValue(0) )
	
	return names

def GetDefaultFilename():
	filename = "<scene>_<aov>_#.<ext>"
	scenename = \
		os.path.splitext(os.path.basename(FarmAPI.GetKatanaFileName()))[0]
	if len(scenename) == 0:
		scenename = "untitled"
	filename = filename.replace("<scene>", scenename)
	return filename

def EnableParamAndSetValue( node, param_name, value ):
	param = node.getParameter( param_name + ".enable" )
	if param:
		param.setValue( True, 0 )

	param = node.getParameter( param_name + ".value" )
	if param:
		param.setValue( value, 0 )

def SetOutputNodeParameters(
		node,
		channel=None,
		extension=None,
		driver=None):
	#  This call is necessary to populate the node with its "dynamic"
	#  parameters, if any.
	node.checkDynamicParameters()

	layer_group = node.getParent();

	# Fetch layer name from parent group.
	name = layer_group.getParameter('imagelayername').getValue(0)

	# Decorate with camera and light set names.
	cam_param = node.getParameter(
		rendererSettings_prefix + 'cameraName.value')
	if cam_param:
		cam_name = cam_param.getValue(0)
		if cam_name:
			name += ' ' + cam_name[len('/root/world/'):]
	light_param = node.getParameter(
		rendererSettings_prefix + 'lightSet.value')
	if light_param:
		light_name = light_param.getValue(0)
		if light_name:
			name += ' ' + light_name[len('/root/world/'):]

	# Finally set the name of this specific output.
	node.getParameter('outputName').setValue(name, 0)

	if channel:
		param_name = rendererSettings_prefix + "channel"
		EnableParamAndSetValue( node, param_name, channel )

		aov_tuple = GetAOVDefinitionTuple( channel )
		if aov_tuple:
			param_name = rendererSettings_prefix + "layerType"
			EnableParamAndSetValue(
				node, param_name, GetAOVLayerType( aov_tuple ))

			param_name = rendererSettings_prefix + "variableSource"
			EnableParamAndSetValue(
				node, param_name, GetAOVVariableSource( aov_tuple ))

			param_name = rendererSettings_prefix + "withAlpha"
			EnableParamAndSetValue(
				node, param_name, GetAOVWithAlpha( aov_tuple ))

	if extension:
		param_name = rendererSettings_prefix + "fileExtension"
		EnableParamAndSetValue( node, param_name, extension )
		if driver:
			param_name = rendererSettings_prefix + "driver"
			EnableParamAndSetValue(node, param_name, driver)

	# Fetch original image file name from DlGlobalSettings
	# We need this to (re)apply the <> tags as needed.
	dls_node = layer_group.getParent()
	global_settings_node = [c for c in dls_node.getChildren()
		if c.getType() == 'DlGlobalSettings'][0]

	outputfile = global_settings_node.getParameter(
		'args.dlGlobalStatements.layers.imageFilename.value').getValue(0)

	if outputfile:
		# Get extension for replace tokens in the filename
		extension_parameter_name = \
			rendererSettings_prefix + \
			"fileExtension." + \
			"value"
		param = node.getParameter(extension_parameter_name)
		ext = param.getValue(0)

		# Get AOV for replace tokens in the filename
		channel_parameter_name = \
			rendererSettings_prefix + \
			"channel." + \
			"value"
		param = node.getParameter(channel_parameter_name)
		full_aov = param.getValue(0)

		aov_tuple = GetAOVDefinitionTuple( full_aov )
		aov = GetAOVToken( aov_tuple )

		# Replace tokens
		filename = outputfile
		filename = filename.replace("<ext>", ext)
		filename = filename.replace("<aov>", aov)

		scenename = \
			os.path.splitext(os.path.basename(FarmAPI.GetKatanaFileName()))[0]
		if len(scenename) == 0:
			scenename = "untitled"

		filename = filename.replace("<scene>", scenename)
			
		lightset = node.getParameter(rendererSettings_prefix + "lightSet.value")
		light_value = 'all'
		if lightset:
			light_value = lightset.getValue(0)
		if light_value == "":
			light_value = 'all'
		else:
			# strip leading /root/world
			light_value = light_value[len("/root/world/"):]
			# get rid of slashes
			light_value = light_value.replace('/', '_')

		filename = filename.replace("<light>", light_value)

		camera = node.getParameter(rendererSettings_prefix + 'cameraName.value')
		camera_name = 'main'
		if camera:
			camera_name = camera.getValue(0)
		if camera_name == '':
			camera_name = 'main'
		else:
			# strip leading /root/world
			camera_name = camera_name[len("/root/world/"):]
			# get rid of slashes
			camera_name = camera_name.replace('/', '_')

		filename = filename.replace("<camera>", camera_name)

		# Set location type to File
		outputfile_parameter_name = \
				outputName_prefix + \
				"locationType." + \
				"value"
		param = node.getParameter(outputfile_parameter_name)
		param.setValue("file", 0)
		node.getParameter("args.__lastLocationType").setValue("local", 0)

		outputfile_enable_name = \
				outputName_prefix + \
				"locationType." + \
				"enable"
		param = node.getParameter(outputfile_enable_name)
		param.setValue(True, 0)

		# Call checkDynamicParameters() to ensure parameters dependent on
		# "locationType" are made available.
		# Despite all those calls, it sometimes fails, such as when the node is
		# inside an InteractiveRenderFilters. So check for param validity.
		Utils.EventModule.ProcessAllEvents()
		Nodes3DAPI.CommitChanges()
		node.checkDynamicParameters()

		# Set the filename
		outputfile_parameter_name = \
				locationSettings_prefix + \
				"renderLocation." + \
				"value"
		param = node.getParameter(outputfile_parameter_name)
		if param:
			param.setValue(filename, 0)

		outputfile_enable_name = \
				locationSettings_prefix + \
				"renderLocation." + \
				"enable"
		param = node.getParameter(outputfile_enable_name)
		if param:
			param.setValue(True, 0)

# Adds a new layer that produces the specified aov (channel) in a new group.
# Each group corresponds to an entry in the Image Layer list.
def AddLayerGroup( i_node, i_name, i_aov ):
	# Create new group node
	group = NodegraphAPI.CreateNode('Group', i_node)
	group.setName("LayerGroup")

	# Add i/o ports
	group.addInputPort("in")
	group.addOutputPort("out")

	# Add a imagelayername parameter and set it to the specified name
	group_root_param = group.getParameters()
	group_root_param.createChildString("imagelayername", i_name)
	group_root_param.createChildString("aov", i_aov)
	group_root_param.createChildString("selected", "No")

	# Let CleanupInternalNetwork handle the new node wiring.
	return group

def AddLayerNodeToGroup( i_node, i_group, i_channel, i_extension, i_driver ):
	# Create the RenderOutputDefine node and the DlOutputChannelDefine node
	# in the group
	#
	output_node = NodegraphAPI.CreateNode('RenderOutputDefine', i_group)

	# Append the node to the group's node chain
	NodeUtils.WireInlineNodes( i_group, i_group.getChildren(), 0, 25 )

	SetOutputNodeParameters(
		output_node,
		i_channel,
		i_extension,
		i_driver )

# Returns True if the specified i_aov declaration is a shading component.
def AOVIsShadingComponent( i_aov ):
	declarations = [aovs[1] for aovs in shading_components]
	return i_aov in declarations
