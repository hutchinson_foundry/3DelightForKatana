################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import QtCore, Nodes3DAPI, UI4
try:
	from Katana import QtWidgets
except ImportError:
	# Qt4 fallback
	from Katana import QtGui as QtWidgets

ITEM_TYPE_ROLE = 1

class QDLMultilightWidget(QtWidgets.QWidget):
	def __init__(self, parent, node):
		QtWidgets.QWidget.__init__(self, parent)

		self.__parent = parent
		self.__node = node
		# Number of visible items in the list
		self.nbitems = 10

		self.InitUI()

	def SetListHeight(self):
		item = QtWidgets.QListWidgetItem('Rigs')
		# Need to set this to get the correct height!
		item.setData( ITEM_TYPE_ROLE, 'Separator' )
		self.list_widget.addItem( item )
		self.list_widget.setFixedHeight( 
			self.nbitems*self.list_widget.visualItemRect(item).height() )
		self.list_widget.removeItemWidget( item )

	def CenterWidget(self, i_widget, i_layout):
		layout = QtWidgets.QHBoxLayout()
		layout.addStretch()
		layout.addWidget(i_widget)
		layout.addStretch()
		i_layout.addLayout( layout )

	def InitUI(self):
		self.main_layout = QtWidgets.QVBoxLayout()

		# A warning about missing lights & rigs, to be shown when relevant.
		#
		icon = UI4.Util.ScenegraphIconManager.GetIcon('warning')

		self.missing_warning_icon = QtWidgets.QLabel( self )

		font_metrics = self.missing_warning_icon.fontMetrics()
		fm_line_spacing = font_metrics.lineSpacing()
		
		icon_size = 2.5 * fm_line_spacing

		margin = fm_line_spacing
		self.main_layout.setContentsMargins(margin, margin, margin, margin)

		self.missing_warning_icon.setPixmap( 
			icon.pixmap( icon_size, icon_size  ) )
		self.missing_warning_icon.setVisible(False)

		self.missing_warning_label = QtWidgets.QLabel( self )
		self.missing_warning_label.setText( 'Some previously selected lights ' +
			'or rigs\nare not present in the incoming scene.')

		self.missing_warning_label.setVisible(False)
		self.missing_warning_layout = QtWidgets.QHBoxLayout()
		self.missing_warning_layout.addStretch()
		self.missing_warning_layout.addWidget(self.missing_warning_icon)
		self.missing_warning_layout.addWidget(self.missing_warning_label)
		self.missing_warning_layout.addStretch()
		self.main_layout.addLayout(self.missing_warning_layout)

		self.list_widget = QtWidgets.QListWidget( self )
		self.list_widget.setMinimumWidth( 22 * fm_line_spacing )

		self.list_widget.setSelectionMode(
				QtWidgets.QAbstractItemView.MultiSelection)

		# Override the didsalbed item stylesheet
		# The AOV selector uses the style sheet in QDLStyle.py
		# For the "category" item to be shown on a background that matches
		# the frame color, we must override the style sheet; trying to set
		# the background color will not work because the style sheet appears to 
		# be applied after.
		#
		self.list_widget.setStyleSheet( "::item:disabled { color: black; " +
			"padding-left: -10px; background-color: #404040 }")
		
		# Center widget
		self.CenterWidget(self.list_widget, self.main_layout)

		# Callback when selection changed (deals with click or
		# with press and drag)
		self.list_widget.itemSelectionChanged.connect(
			self.OnItemSelectionChanged)

		self.display_toggle = QtWidgets.QCheckBox("Display All Lights")
		self.display_toggle.stateChanged[int].connect(
				self.OnDisplayAllLightsChanged)

		# Center toggle
		self.CenterWidget(self.display_toggle, self.main_layout)

		self.setLayout(self.main_layout)

	def GetLightsFromLightList(self, root, io_rigs, io_lights, io_rig_lights):
		"""
		Fills light and rig sets from the light list at /root/world
		"""
		world = root.getChildByName('world')
		if not world:
			return
		light_list = world.getFnAttribute('lightList')
		if not light_list:
			return
		for l in range(light_list.getNumberOfChildren()):
			light_path_attr = light_list.getChildByIndex(l).getChildByName('path')
			if not light_path_attr:
				continue
			light_path = light_path_attr.getValue()
			light_geo_producer = root.getProducerByPath(light_path)
			if light_geo_producer:
				parent_producer = light_geo_producer.getParent()
				if parent_producer and parent_producer.getType() == 'rig':
					# Light is in a rig
					io_rigs.add(parent_producer.getFullName())
					io_rig_lights.add(light_path)
					continue
				# Light is standalone
				io_lights.add(light_path)

	# Clear then fill the multi-light list.
	def FillLightList(self):
		old_state = self.list_widget.blockSignals(True)

		# Clear the list
		self.list_widget.clear()
		self.list_widget.setVerticalScrollBarPolicy( 
			QtCore.Qt.ScrollBarAlwaysOff )

		# The light list is built as follows:
		# - Missing lights & rigs.
		#     Lists the lights used by layers that are no longer defined as
		#     proper scene graph locations.
		# - Rigs.
		#     List the rigs defined in the scene. The ones that are used by 
		#     layers are set as selected.
		# - Lights.
		#     List the lights defined in the scene. Lights that are children
		#     of rigs will also be listed if "Display All Lights" is checked.
		#     The lights that are used by layers are set as selected.
		#
		# If a layer is set to use a light / or rig, it must be shown in the 
		# list.
		#

		# Figure the list of lights & rigs available in the scene.
		# Get the root node
		root = Nodes3DAPI.GetGeometryProducer(self.__node)
		if not root:
			self.list_widget.blockSignals( old_state )
			return

		rigs = set()
		lights = set()
		rig_lights = set()

		# Parse the light list to get rigs & lights
		self.GetLightsFromLightList(root, rigs, lights, rig_lights)

		# Add the rig lights to the lights set if Display All Lights is enabled
		if self.display_toggle.checkState() == QtCore.Qt.Checked:
			lights.update(rig_lights)

		# Figure the list of lights used by layers
		layer_lights = self.__node.GetLightGroups()

		missing_layer_lights = []
		for i in layer_lights:
			# Find layer_lights which refer to invalid scene graph locations
			light_prod = root.getProducerByPath( i )
			if not light_prod:
				# Invalid scene graph location. Add to the missing list.
				missing_layer_lights.append( i )
			elif light_prod.getType() == 'light':
				# This happens if the layer refers to a single rig light while
				# Display All Lights is not enabled. We still want this entry
				# to be displayed.
				lights.add( i )

		# Figure the list item height based on font metrics, like the AOV
		# selector.
		#
		font_metrics = self.list_widget.fontMetrics()
		item_height = font_metrics.lineSpacing() * 1.77

		# Add the missing light layers first
		if missing_layer_lights:
			item = QtWidgets.QListWidgetItem('Missing')
			font = item.font()
			font.setBold(True)
			item.setFont(font)
			item.setFlags( QtCore.Qt.NoItemFlags )
			item.setData(ITEM_TYPE_ROLE, 'Separator' )
			AdjustItemHeight(item, item_height)
			self.list_widget.addItem( item )

			icon = UI4.Util.ScenegraphIconManager.GetIcon('warning')
			for i in missing_layer_lights:
				item = QtWidgets.QListWidgetItem( i )
				item.setData(ITEM_TYPE_ROLE, 'Missing' )
				item.setIcon(icon)
				AdjustItemHeight(item, item_height)
				self.list_widget.addItem( item )
				item.setSelected( True )

			self.missing_warning_icon.setVisible( True )
			self.missing_warning_label.setVisible( True )
		else:
			self.missing_warning_icon.setVisible( False )
			self.missing_warning_label.setVisible( False )


		# Add the rigs
		if rigs:
			item = QtWidgets.QListWidgetItem('Rigs')
			font = item.font()
			font.setBold(True)
			item.setFont(font)
			item.setFlags( QtCore.Qt.NoItemFlags )
			item.setData(ITEM_TYPE_ROLE, 'Separator' )
			AdjustItemHeight(item, item_height)
			self.list_widget.addItem( item )

			rig_list = sorted(rigs)
			for i in rig_list:
				item = QtWidgets.QListWidgetItem( i )
				item.setData(ITEM_TYPE_ROLE, 'Rig' )
				AdjustItemHeight(item, item_height)
				self.list_widget.addItem( item )
				if i in layer_lights:
					item.setSelected( True )

		# Add the lights
		if lights:
			item = QtWidgets.QListWidgetItem('Lights')
			font = item.font()
			font.setBold(True)
			item.setFont(font)
			item.setFlags( QtCore.Qt.NoItemFlags )
			item.setData(ITEM_TYPE_ROLE, 'Separator' )
			AdjustItemHeight(item, item_height)
			self.list_widget.addItem( item )

			light_list = sorted(lights)
			for i in light_list:
				item = QtWidgets.QListWidgetItem( i )
				item.setData(ITEM_TYPE_ROLE, 'Light' )
				AdjustItemHeight(item, item_height)
				self.list_widget.addItem( item )

				if i in layer_lights:
					item.setSelected( True )

		self.list_widget.blockSignals(old_state);

		if self.list_widget.count() > self.nbitems:
			self.list_widget.setVerticalScrollBarPolicy( 
				QtCore.Qt.ScrollBarAlwaysOn )

	def OnDisplayAllLightsChanged(self, state):
		self.FillLightList()

	def OnItemSelectionChanged(self):
		selectedItems = self.list_widget.selectedItems()
		layer_lights = self.__node.GetLightGroups()

		if len(selectedItems) > len(layer_lights):
			for i in selectedItems:
				if i.text() not in layer_lights:
					self.__node.AddMultiLight(str(i.text()))
					break
		else:
			selectedText = []
			for i in selectedItems:
				selectedText.append(str(i.text()))
			for i in layer_lights:
				if i not in selectedText:
					self.__node.RemoveMultiLight(i)
					break

def AdjustItemHeight( item, height ):
	size_hint = item.sizeHint()
	size_hint.setHeight( height )
	item.setSizeHint( size_hint )

