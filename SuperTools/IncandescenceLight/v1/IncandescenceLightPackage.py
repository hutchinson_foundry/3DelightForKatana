# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2017                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

# Copyright (c) 2015 The Foundry Visionmongers Ltd. All Rights Reserved.

from Katana import NodegraphAPI, Decorators, Plugins
import PackageSuperToolAPI.NodeUtils as NU
from PackageSuperToolAPI import Packages

import os
import textwrap
import logging
import math

# Our module with common light package code.
import LightBase;

log = logging.getLogger("GafferThree.IncandescenceLightPackage")

_iconsDir = os.path.join(os.path.dirname(__file__), 'icons')

# Get base classes for our packages from the registered GafferThree packages
GafferThreeAPI = Plugins.GafferThreeAPI
LightPackage = GafferThreeAPI.PackageClasses.LightPackage
LightEditPackage = GafferThreeAPI.PackageClasses.LightEditPackage


class IncandescenceLightBasePackage(LightBase.BasePackage):
    """
    Common code for IncandescenceLightPackage and IncandescenceLightEditPackage.
    """

    @classmethod
    def addLightAttributesNode(cls, packageNode, editing):
        node = NodegraphAPI.CreateNode('DlIncandescenceLightAttributes', packageNode)
        cls.setGenericAssignCEL(node)
        if not editing:
            # Always set value locally.
            node.getParameter(
                'args.geometry.sourceMesh.enable').setValue(1, 0)
        NU.AddNodeRef(packageNode, 'lightAttrNode', node)
        cls.appendNode(packageNode, editing, node)

class IncandescenceLightPackage(LightPackage,IncandescenceLightBasePackage):
    """
	Implements a Incandescence Light package. We inherit LightPackage, and override
	some functions to modify behaviour.
    """

    # Class Variables ---------------------------------------------------------

    # The name of the package type as it should be shown in the UI
    DISPLAY_NAME = 'Incandescence Light'

    # The default name of a package when it is created. This also defines the
    # default name of the package's scene graph location
    DEFAULT_NAME = 'incandescenceLight'

    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        A factory method which returns an instance of the class.

        @type enclosingNode: C{NodegraphAPI.Node}
        @type locationPath: C{str}
        @rtype: L{LightPackage}
        @param enclosingNode: The parent node within which the new
            package's node should be created.
        @param locationPath: The path to the location to be created/managed
            by the package.
        @return: The newly-created package instance.
        """
        # Create the package node
        packageNode = cls.createPackageNode(enclosingNode, locationPath)
        # Add nodes to our package group.
        cls.addMasterMaterialPortNode(packageNode)
        # Create the location.
        createNode = cls.addCreateNode(packageNode)
        # Add some boilerplate.
        cls.addPostCreateStandardNodes(packageNode)
        # Add light Material.
        cls.addMaterialNodes(
            packageNode, False, 'Light', 'incandescenceMultiplier')
        # Add IncandescenceLight attributes node.
        cls.addLightAttributesNode(packageNode, False)
        # Add other standard stuff.
        cls.addPruneOpScriptNode(packageNode)
        cls.addChildPackageMergeNode(packageNode)
        cls.addLightLinkingNodes(packageNode, False)

        # Create a package node instance
        result = cls(packageNode)
        Packages.CallbackMixin.executeCreationCallback(result)

        # Create a post-merge stack node for this package
        postMergeNode = result.createPostMergeStackNode()

        cls.addIncandescenceControlNode(postMergeNode)

        return result

    @classmethod
    def addIncandescenceControlNode(cls, postMergeNode):
        """
        Adds a script to the post merge stack to implement support for the
        incandescence control mode. It will add material overrides where they
        are needed to apply the incandescence.
        """
        node = NodegraphAPI.CreateNode('OpScript', postMergeNode)
        node.setName('IncandescenceLightControl')
        # This script needs to run on the location (and its children) specified
        # in the mesh light. That location is only stored on the mesh light
        # location so we can't get to it with the conventional OpScript API. To
        # work around this, I use a python expression on a user param to fetch
        # the value directly in the DlIncandescenceLightAttributes node's
        # parameters. I could
        # probably do it all with an ugly expression for CEL but that would be
        # unreadable. The same trick is used to give the script the light's
        # location.  And to figure out if the light is disabled in the UI.
        node.getParameter('CEL').setValue('//*', 0)

        # Create the user param group and the params we want.
        userGroup = node.getParameters().createChildGroup('user')
        sourceParam = userGroup.createChildString('sourceMesh', '')
        lightParam = userGroup.createChildString('lightLocation', '')
        disabledParam = userGroup.createChildNumber('disabled', 0)

        # Now build the ultimate in ugly python expression to get to the
        # DlIncandescenceLightAttributes's parameters without explicit node
        # names. This uses the node references stored on the package node (ie.
        # the 'lightAttrNode' part is from addLightAttributesNode()
        ugly = ('getNode( str( getNode( str( self.getParent().node_package ) )'
                '.node_lightAttrNode ) ).args')
        sourceParam.setExpression(ugly + '.geometry.sourceMesh.value')
        # Then the not quite as bad expression to get the light's location.
        lightParam.setExpression(
            'getNode( str( self.getParent().node_package ) ).' +
            NU.GetPackageLocationParameterPath() )
        # The expression for disabled. I used a slightly different way as 'in'
        # appears to be some kind of reserved word so you can't get to that
        # parameter with the usual syntax of node.in (gives syntax error).
        disabledParam.setExpression(
            "getParam( str( getNode( str( self.getParent().node_package ) )"
            ".node_uiDisableSwitch ) + '.in' )" )

        script = textwrap.dedent("""\
        -- Function to check if an ancestor location matches CEL
        function AncestorMatchesCEL( cel, location )
            while true do
                location = PathUtils.GetLocationParent( location )
                if location == '' then
                    return false
                end
                if InterfaceUtils.MatchesCEL( cel, location ) then
                    return true
                end
            end
        end

        local disabled = Interface.GetOpArg('user.disabled'):getValue(0, false)
        local sourceMesh = Interface.GetOpArg('user.sourceMesh'):getValue('', false)
        -- Nothing to do if the light is disabled or no geo was specified
        if disabled == 1 or sourceMesh == '' then
            Interface.StopChildTraversal()
            return
        end
        -- Check 3 types of matches: parent, current location, child.
        local loc = Interface.GetOutputLocationPath()
        local matchesParent = AncestorMatchesCEL( sourceMesh, loc )
        local matchesLoc, matchesChild = InterfaceUtils.MatchesCEL( sourceMesh, loc )
        -- Stop traversal if this location is unrelated to the source.
        if not (matchesParent or matchesLoc or matchesChild) then
            Interface.StopChildTraversal()
            return
        end
        -- Nothing to do on parent locations of the source.
        if not (matchesParent or matchesLoc) then
            return
        end
        -- See if we need to add an override to this location. We need to:
        --   * At the root of the geometry used by the mesh light.
        --   * Anywhere there is some kind of material which might override the above.
        local needsOverride =
            (matchesLoc and not matchesParent)
            or Interface.GetAttr('materialAssign') ~= nil
            or Interface.GetAttr('material') ~= nil
            or Interface.GetAttr('materialOverride') ~= nil
        if not needsOverride then
            return
        end
        -- Fetch the intensity from the mesh light's material.
        -- This assumes the default areaLight material.
        local lightLoc = Interface.GetOpArg('user.lightLocation'):getValue('', false)
        local colorAttr = Interface.GetAttr('material.dlLightParams.color', lightLoc)
        local intensityAttr = Interface.GetAttr('material.dlLightParams.intensity', lightLoc)
        local exposureAttr = Interface.GetAttr('material.dlLightParams.exposure', lightLoc)
        local totalIntensity = 1
        if intensityAttr ~= nil then
            totalIntensity = intensityAttr:getValue(1, false)
        end
        if exposureAttr ~= nil then
            totalIntensity = totalIntensity * (2 ^ exposureAttr:getValue(0, false))
        end
        local finalInc = { totalIntensity, totalIntensity, totalIntensity }
        if colorAttr ~= nil then
            local data = colorAttr:getNearestSample(0)
            finalInc[1] = finalInc[1] * data[1]
            finalInc[2] = finalInc[2] * data[2]
            finalInc[3] = finalInc[3] * data[3]
        end
        -- Find the assigned material.
        local matAssignAttr = Interface.GetGlobalAttr('materialAssign')
        local matAssign = '';
        if matAssignAttr ~= nil then
            matAssign = matAssignAttr:getValue('', false)
        end
        -- See if it's a network material.
        local surfaceTerminal = ''
        local surfaceTerminalAttr = Interface.GetAttr('material.terminals.dlSurface', matAssign)
        if surfaceTerminalAttr ~= nil then
            surfaceTerminal = surfaceTerminalAttr:getValue('', false)
        end
        -- Figure out the name of the attributes to override.
        -- This default is for regular material (not network).
        local paramsGroup = 'materialOverride.dlSurfaceParams'
        -- Parameter to set with incandescence value. Changed for the multiplier
        -- if there is a connection on the base value (network material).
        local incandescenceParam = '.incandescence'
        if surfaceTerminal ~= '' then
            paramsGroup = 'materialOverride.nodes.' .. surfaceTerminal .. '.parameters'
            local connAttr = Interface.GetAttr(
                'material.nodes.' .. surfaceTerminal .. '.connections.incandescence',
                matAssign)
            if connAttr ~= nil then
                incandescenceParam = '.incandescence_multiplier'
            end
        end
        -- Apply to material's "incandescence" parameter with material override.
        Interface.SetAttr( paramsGroup .. incandescenceParam, FloatAttribute(finalInc, 3))
        -- Also set "incandescence_intensity" as 3dl mat has it and it defaults to 0.
        Interface.SetAttr( paramsGroup .. '.incandescence_intensity', FloatAttribute(1))
        -- Apply to volume material's incandescence parameter. No support for
        -- network material (yet) here so things are much simpler.
        Interface.SetAttr( 'materialOverride.dlVolumeParams.incandescence', FloatAttribute(finalInc, 3))

        -- Add a deferred OpScript do handle mute and solo. This is needed to
        -- see the solo from other lights in the scene. The implicit_preprocess
        -- Id makes it run before material resolve (where overrides are used).
        -- Note that the incandescence light location is baked into the script.
        -- What it does is delete the material override attributes set above.
        local opGroup = 'ops.IncandescenceMuteAndSolo'
        Interface.SetAttr(opGroup .. '.opType', StringAttribute('OpScript.Lua'))
        Interface.SetAttr(opGroup .. '.resolveIds', StringAttribute('implicit_preprocess'))
        Interface.SetAttr(opGroup .. '.opArgs.script', StringAttribute(
            "local lightLoc = '" .. lightLoc .. "'\\n" ..
            "local mute = 0\\n" ..
            "local muteAttr = Interface.GetGlobalAttr('info.light.mute', lightLoc)\\n" ..
            "if muteAttr ~= nil then\\n" ..
            "    mute = muteAttr:getValue(0, false)\\n" ..
            "end\\n" ..
            "local soloLightsAttr = Interface.GetAttr('globals.itemLists.soloedLocations', '/root/world')\\n" ..
            "if soloLightsAttr ~= nil then\\n" ..
            "    local soloLights = soloLightsAttr:getNearestSample(0)\\n" ..
            "    mute = 1\\n" ..
            "    for i, light in pairs(soloLights) do\\n" ..
            "        if PathUtils.IsAncestorOrEqual(light, lightLoc) then\\n" ..
            "            mute = 0\\n" ..
            "        end\\n" ..
            "    end\\n" ..
            "end\\n" ..
            "if mute ~= 0 then\\n" ..
            "    Interface.DeleteAttr('" .. paramsGroup .. incandescenceParam .. "')\\n" ..
            "    Interface.DeleteAttr('" .. paramsGroup .. '.incandescence_intensity' .. "')\\n" ..
            "    Interface.DeleteAttr('materialOverride.dlVolumeParams.incandescence')\\n" ..
            "end\\n"
            ))
        """)
        node.getParameter('script.lua').setValue(script, 0)
        postMergeNode.buildChildNode(adoptNode=node)


class IncandescenceLightEditPackage(
        LightEditPackage,IncandescenceLightBasePackage):
    """
    The edit package that allows a GafferThree to edit an existing
    IncandescenceLight package present in the input Scenegraph.
    """

    # Class Variables ---------------------------------------------------------


    # Class Functions ---------------------------------------------------------

    @classmethod
    def create(cls, enclosingNode, locationPath):
        """
        Creates the contents of the EditStackNode that contains the edit nodes.
        This could be any other kind of node with at least one input and one
        output, but the createPackageEditStackNode() helper function does all
        of the configuration boilerplate code of an EditStackNode for you.
        The return value is a IncandescenceLightEditPackage instance.

        This particular package node will contain a TransformEdit node on it,
        which will allow to edit the transform of a skyDome.
        """
        # Create the package node. Since this is an edit package we want to use
        # an EditStackNode instead of a GroupNode, since it already has an
        # input and an output by default. This also adds some necessary
        # parameters to this node.
        packageNode = cls.createPackageEditStackNode(enclosingNode,
                                                     locationPath)

        # Add the various nodes needed to edit the light.
        cls.addMaterialNodes(packageNode, True, 'Light', 'areaLight')
        # Left out because it makes no sense to change the mesh.
        #cls.addLightAttributesNode(packageNode, True)
        cls.addLightLinkingNodes(packageNode, True)

        # Instantiate a package with the package node
        return cls.createPackage(packageNode)

    @classmethod
    def getAdoptableLocationTypes(cls):
        """
        Returns the set of location types adoptable by this Package. In this
        case, the package can edit locations created by
        IncandescenceLightPackages, which are of the type light.
        """
        return set(('light',))

    # Instance Functions ------------------------------------------------------

    @Decorators.undogroup('Delete IncandescenceLightEdit Package')
    def delete(self):
        LightEditPackage.delete(self)


# Register the package classes, and associate the edit package class with the
# create package class
GafferThreeAPI.RegisterPackageClass(IncandescenceLightPackage)
GafferThreeAPI.RegisterPackageClass(IncandescenceLightEditPackage)
IncandescenceLightPackage.setEditPackageClass(IncandescenceLightEditPackage)
