# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2020                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import NodegraphAPI, Utils

# In an ideal world this would just be a member of AOVGroupNode.
# But we sometimes get incomplete objects so it must be here instead.
def GetShadingNodes(groupNode):
    """
    Parses the network inside an DlAOVGroup node and returns a list of
    (aovName, shadingNode) tuples describing its content.
    """
    network = []
    # Get output port of first node.
    nextPort = groupNode.getReturnPort('out').getConnectedPort(0)
    while nextPort:
        node = nextPort.getNode()
        nameParam = node.getParameter('parameters.aov_name.value')
        # Katana messes with shader parameters. No idea why.
        if not nameParam:
            nameParam = node.getParameter('parameters.__unused.aov_name.value')
        if nameParam:
            aovName = nameParam.getValue(0)
            network.append((aovName, node))
        # Get output port of next node.
        nextPort = node.getInputPort('in').getConnectedPort(0)
    return network

