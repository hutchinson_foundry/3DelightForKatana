# vim: set softtabstop=4 expandtab shiftwidth=4:

################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2020                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import FnAttribute
from Katana import Nodes3DAPI
from Nodes3DAPI.Manifest import FnGeolibServices
import os.path

def registerDlAOVNode(typeName):
    """
    Registers a new DlAOVxx node type using the NodeTypeBuilder utility class.
    """

    def buildDlAOVNodeOpChain(node, interface):
        """
        Callback function that creates the Op chain for a DlAOVxx node.

        @type node: C{Nodes3DAPI.NodeTypeBuilder.NSIArchive}
        @type interface: C{Nodes3DAPI.NodeTypeBuilder.BuildChainInterface}
        @param node: The node for which to define the Ops chain
        @param interface: The interface providing the functions needed to set
            up the Ops chain for the given node.
        """

        # No input ports for this node.
        interface.setMinRequiredInputs(0)

    def buildParameters(node):
        '''
        Callback function that may create or alter node parameters before its
        creation is completed.
        '''
        # Use this opportunity to remove the unused output port.
        node.removeOutputPort('out')

    # Create a NodeTypeBuilder to register the new type
    nodeTypeBuilder = Nodes3DAPI.NodeTypeBuilder('DlAOV' + typeName)

    # Set the callback responsible to build the Ops chain
    nodeTypeBuilder.setBuildOpChainFnc(buildDlAOVNodeOpChain)

    # Set the callback that allow gives us a chance to alter param values upon 
    # node creation.
    nodeTypeBuilder.setBuildParametersFnc(buildParameters)

    # Build the new node type
    nodeTypeBuilder.build()

def registerDlAOVNodeUI(typeName):
    """
    Registers UI for the DlAOVxx node type.
    """
    try:
        from Katana import UI4
    except ImportError:
        return

    # Register a policy delegate to give all parameter widgets mini state badges
    parameterPolicyDelegate = UI4.FormMaster.EnableableScalarPolicyDelegate()

    UI4.FormMaster.ParameterPolicy.RegisterPolicyDelegate(
        'DlAOV' + typeName,
        parameterPolicyDelegate)

# Register the nodes
registerDlAOVNode('Color')
registerDlAOVNode('Float')

# UI stuff if available
registerDlAOVNodeUI('Color')
registerDlAOVNodeUI('Float')
