# vim: set softtabstop=4 expandtab shiftwidth=4:
"""
/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/
"""

from Nodes3DAPI import AbstractTransform
from Nodes3DAPI import Node3D
from Nodes3DAPI.Manifest import FnAttribute
from Nodes3DAPI.Manifest import FnGeolibServices
from Nodes3DAPI.Manifest import NodegraphAPI

AT = AbstractTransform.AbstractTransform

class XGenIn(AT):
    def __init__(self, primitiveType = None):
        Node3D.__init__(self)
        self.addOutputPort('out')
        self.getParameters().parseXML(g_parameter_desc)
        self.__initName()

    def __initName(self):
        # Init name and add a number is necessary
        myName = self.getName()
        num = ''
        if myName:
            while True:
                new_val = myName[-1] + num
                myName = myName[:-1]
                if not new_val.isdigit():
                    break
                num = new_val

        self.getParameter('name').setValue('/root/world/geo/xgen' + num, 0)

    def getScenegraphLocation(self):
        return self.getParameterValue('name', NodegraphAPI.GetCurrentTime())

    def addParameterHints(self, attrName, inputDict):
        inputDict.update(g_widgets.get(attrName, {}))

    def customReset(self):
        NodegraphAPI.Util.GenericParameterReset(self)
        self.__initName()
        return True

    def _getOpChain(self, interface):
        # No input
        interface.setMinRequiredInputs(0)

        graph_state = interface.getGraphState()
        frame_time = graph_state.getTime()
        location_path = self.getParameter('name').getValue(frame_time)

        # Scene state
        sscb = FnGeolibServices.OpArgsBuilders.StaticSceneCreate()

        # Create an object
        sscb.createEmptyLocation(location_path, "XGen")

        # Copy parameters to the group
        args_group = FnAttribute.GroupBuilder()
        filename = self.getParameter('file').getValue(frame_time)
        args_group.set('file', filename)
        palette = self.getParameter('palette').getValue(frame_time)
        args_group.set('palette', palette)
        description = self.getParameter('description').getValue(frame_time)
        args_group.set('description', description)
        patch = self.getParameter('patch').getValue(frame_time)
        args_group.set('patch', patch)
        geom = self.getParameter('geom').getValue(frame_time)
        args_group.set('geom', geom)
        xgenlibdir = self.getParameter('xgenlibdir').getValue(frame_time)
        args_group.set('xgenlibdir', xgenlibdir)
        frametime = self.getParameter('frametime').getValue(frame_time)
        args_group.set('frametime', frametime)
        density = self.getParameter('density').getValue(frame_time)
        args_group.set('density', density)

        # Put the group to the object
        sscb.setAttrAtLocation(location_path, 'xgen', args_group.build())

        # Save the scene state
        interface.appendOp('StaticSceneCreate', sscb.build())

g_widgets = {
    'XGen_In.name': {'widget': 'newScenegraphLocation'},
    'XGen_In.file': {'widget': 'assetIdInput'},
    'XGen_In.geom': {'widget': 'assetIdInput'},
    'XGen_In.xgenlibdir': {'widget': 'assetIdInput'} }

g_parameter_desc = '''
    <group_parameter>
        <string_parameter name='name' value=''/>
        <string_parameter name='file' value=''/>
        <string_parameter name='palette' value=''/>
        <string_parameter name='description' value=''/>
        <string_parameter name='patch' value=''/>
        <string_parameter name='geom' value=''/>
        <string_parameter name='xgenlibdir' value=''/>
        <number_parameter name='frametime' value='1'/>
        <number_parameter name='density' value='0.1'/>
    </group_parameter>
'''

NodegraphAPI.RegisterPythonNodeType('XGen_In', XGenIn)
NodegraphAPI.AddNodeFlavor('XGen_In', '3d')
NodegraphAPI.AddNodeFlavor('XGen_In', 'procedural')

