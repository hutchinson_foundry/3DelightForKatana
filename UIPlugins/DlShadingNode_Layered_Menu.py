# vim: set softtabstop=4 expandtab shiftwidth=4:
"""
Module that defines a layered menu for creating 3Delight shading nodes when
choosing the name of a shader type from the menu.
"""

from Katana import Nodes3DAPI, NodegraphAPI, LayeredMenuAPI
from Nodes3DAPI.Manifest import FnAttribute
import RenderingAPI
import logging

def _isInternalShader(name):
    if name in ('uvCoord'):
        return True
    if name.startswith('NetworkMaterial_'):
        return True
    if name.startswith('dlAOV'):
        return True
    return False

def PopulateCallback(layeredMenu):
    """
    Callback for the layered menu, which adds entries to the given
    C{layeredMenu} based on the node types available for DlShadingNode nodes.

    @type layeredMenu: L{LayeredMenuAPI.LayeredMenu}
    @param layeredMenu: The layered menu to add entries to.
    """
    # Interrogate the 3Delight renderer info plug-in to obtain a list of names
    # of shading node types
    rendererInfoPlugin = RenderingAPI.RendererInfo.GetPlugin('dlRendererInfo')
    nodeTypes = rendererInfoPlugin.getRendererObjectNames(
        RenderingAPI.RendererInfo.kRendererObjectTypeShader)

    # Iterate over the names of shader types and add a menu entry for each of
    # them to the given layered menu, using the branded 3Delight red as the
    # color for the entries. Ignore some internal shaders.
    for nodeType in nodeTypes:
        if not _isInternalShader(nodeType):
            layeredMenu.addEntry(nodeType, text=nodeType,
                                 color=(0.7294, 0.102, 0.1137))

def _FindTerminalOutput(shader):
    """
    Given a shader name, this looks for an output of that shader which should
    be connected to a terminal. If one is found, a (output name, terminal type)
    tuple is returned.
    """
    rendererInfoPlugin = RenderingAPI.RendererInfo.GetPlugin('dlRendererInfo')
    outputs = rendererInfoPlugin.getShaderOutputNames(shader)
    for output in outputs:
        tags = rendererInfoPlugin.getShaderOutputTags(shader, output)
        for tag in tags:
            if tag in ('Surface', 'Displacement', 'Volume'):
                return (output, tag)
    return None

def ActionCallback(value, tab=None):
    """
    Callback for the layered menu, which creates a DlShadingNode node and sets
    its B{nodeType} parameter to the given C{value}, which is the name of a
    shader as set for the menu entry in L{PopulateCallback()}.

    @type value: C{str}
    @rtype: C{object}
    @param value: An arbitrary object that the menu entry that was chosen
        represents. In our case here, this is the name of a 3Delight shading
        node type as passed to the L{LayeredMenuAPI.LayeredMenu.addEntry()}
        function in L{PopulateCallback()}.
    @param tab: Newer versions of Katana will pass the instance of the
        NodegraphPanel where the Layered Menu originated.
    @return: An arbitrary object. In our case here, we return the created
        3Delight shading node, which is then placed in the B{Node Graph} tab
        because it is a L{NodegraphAPI.Node} instance.
    """

    def CreateDlShadingNode(nodeType, parentNode):
        """
        Helper function to create a DlShadingNode under the given parent node
        with its nodeType parameter set to the given node type name, and its
        name set to the given node type name, possibly followed by a numeric
        index to make the name unique in the node graph document.
        """
        result = NodegraphAPI.CreateNode('DlShadingNode', parentNode)
        result.getParameter('nodeType').setValue(nodeType, 0)
        result.setName(nodeType)
        result.getParameter('name').setValue(result.getName(), 0)
        return result

    def IsNetworkMaterialGroup(node):
        """
        Returns True if node is a network material group.
        """
        contextModeParam = node.getParameter('__nodeGraphContext')
        if contextModeParam:
            return contextModeParam.getValue(0.0) == 'networkMaterial'
        return False

    # Use the entered Group node of the largest Node Graph tab as the parent
    # node for the new nodes
    import UI4
    nodeGraphTab = tab or UI4.App.Tabs.FindTopTab('Node Graph')
    parentNode = nodeGraphTab.getEnteredGroupNode()

    if parentNode.getType() == 'LiveGroup':
        # Forbid creation in LiveGroup with contents locked.
        if parentNode.isContentLocked():
            logging.getLogger('3Delight Shading Nodes').error(
                'Cannot create shading nodes in %s node "%s", as the node\'s contents is locked.',
                parentNode.getType(), parentNode.getName())
            return None
    else:
        # Forbid creation in locked groups.
        if parentNode.isLocked(True):
            logging.getLogger('3Delight Shading Nodes').error(
                'Cannot create shading nodes in %s node "%s", as the node is locked.',
                parentNode.getType(), parentNode.getName())
            return None

    # Create a shading node with its name and node type parameters set to the
    # chosen shader type name as given in `value`
    node = CreateDlShadingNode(value, parentNode)

    # Ensure that the input and output parameters are shown when the user opens
    # the input or output parameter popup by clicking the triangle on the left
    # or right side of the shading node
    node.checkDynamicParameters()

    # Define a list to collect all nodes created in this function, starting
    # with the main shading node we created above
    nodes = []
    nodes.append(node)

    # Check if the chosen type of shading node requires additional nodes in
    # order to be useful

    terminal = _FindTerminalOutput(value)
    # Connect the shader to a NetworkMaterial. Except in ShadingGroup node.
    if terminal and parentNode.getType() != 'ShadingGroup':
        networkMaterialNode = None
        if IsNetworkMaterialGroup(parentNode):
            # New Katana 3.2 UI. Find the existing NetworkMaterial node.
            for child in parentNode.getChildren():
                if child.getType() == 'NetworkMaterial':
                    networkMaterialNode = child
        else:
            # Outside a NetworkMaterialGroup, create a NetworkMaterial.
            networkMaterialNode = NodegraphAPI.CreateNode(
                'NetworkMaterial', parentNode)
            nodes.append(networkMaterialNode)
            # Move it to the right of the shading node
            x, y = NodegraphAPI.GetNodePosition(node)
            NodegraphAPI.SetNodePosition(networkMaterialNode, (x + 250, y))

        if networkMaterialNode:
            # Make sure to call RendererInfo::fillRendererShaderTypeTags
            networkMaterialNode.addShaderInputPort('dl', terminal[1])

            # Connect the output of the shader node to the input port of the
            # NetworkMaterial node, if it is free.
            nmInputPort = networkMaterialNode.getInputPort('dl' + terminal[1])
            if nmInputPort.getNumConnectedPorts() == 0:
                node.getOutputPort(terminal[0]).connect(nmInputPort)

    # 2D texture shader connected to place2dTexture with a single connection
    elif value == 'bulge' or value == 'checker' or value == 'cloth' or value == 'fractal' or value == 'grid' or value == 'noise' or value == 'ocean' or value == 'ramp' or value == 'stencil':
        # Create a place2dTexture shading node
        place2dTextureNode = CreateDlShadingNode('place2dTexture', parentNode)
        nodes.append(place2dTextureNode)

        # Connect the UV Coordinates output of the place2dTexture node to the
        # input of the same name on the respective main shading node
        place2dTextureNode.checkDynamicParameters()
        place2dTextureNode.getOutputPort('outUV').connect(
            node.getInputPort('uvCoord'))

        # Move the place2dTexture node to the left of the main shading node
        x, y = NodegraphAPI.GetNodePosition(node)
        NodegraphAPI.SetNodePosition(place2dTextureNode, (x - 250, y))

    # 2D texture shader connected to place2dTexture with a lot
    # of connections (if file and psdFileTex had the correct input ports)
    elif value == 'file' or value == 'psdFileTex':
        # Create a place2dTexture shading node
        place2dTextureNode = CreateDlShadingNode('place2dTexture', parentNode)
        nodes.append(place2dTextureNode)

        # Connect the UV Coordinates output of the place2dTexture node to the
        # input of the same name on the respective main shading node
        place2dTextureNode.checkDynamicParameters()
        place2dTextureNode.getOutputPort('outUV').connect(
            node.getInputPort('uvCoord'))

        # Move the place2dTexture node to the left of the main shading node
        x, y = NodegraphAPI.GetNodePosition(node)
        NodegraphAPI.SetNodePosition(place2dTextureNode, (x - 250, y))

    # 3D texture shader connected to locationMatrix with single connection
    elif value == 'brownian' or value == 'cloud' or value == 'granite' or value == 'leather' or value == 'marble' or value == 'rock' or value == 'snow' or value == 'solidFractal' or value == 'stucco' or value == 'volumeNoise' or value == 'wood':
        # Create a locationMatrix shading node
        locationMatrix = CreateDlShadingNode('locationMatrix', parentNode)
        nodes.append(locationMatrix)

        # Connect the output world inverse matrix of the locationMatrix node to
        # the placement matrix input on the respective main shading node
        locationMatrix.checkDynamicParameters()
        locationMatrix.getOutputPort('o_worldInverseMatrix').connect(
            node.getInputPort('placementMatrix'))

        # Move the locationMatrix node to the left of the main shading node
        x, y = NodegraphAPI.GetNodePosition(node)
        NodegraphAPI.SetNodePosition(locationMatrix, (x - 250, y))

    # Check if more than one node has been created, and if so, make them move
    # along with the pointer in the Node Graph tab
    # TODO: Once it's possible to return a list of nodes by an action callback
    #       of a layered menu (instead of a single node only), we can change
    #       this function here to simply return the list of `nodes` we built
    if len(nodes) > 1:
        nodeGraphTab.floatNodes(nodes)

        # Return nothing as we're done here
        return None
    else:
        # Return the single node that was created, which is then made to move
        # along with the pointer in the Node Graph tab by the code that calls
        # this action callback
        return node


# Create and register the layered menu using the above callbacks
layeredMenu = LayeredMenuAPI.LayeredMenu(PopulateCallback, ActionCallback,
                                         'S', alwaysPopulate=False,
                                         onlyMatchWordStart=False)
LayeredMenuAPI.RegisterLayeredMenu(layeredMenu, '3Delight Shading Nodes')

