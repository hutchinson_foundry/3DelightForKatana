from __future__ import with_statement

import ViewerManipulators
import Katana, FnKatImport

FnKatImport.FromObject(Katana.Plugins.ViewerManipulatorAPI, merge = ['*'])

from math import sin, cos, radians
from math import pi as PI

# Inspired by LightDecay in plugin_apis/python/ViewerManipulators
class DlDecayFilter(ViewerManipulators.LightManipulator):

    UI_NAME = 'Decay Filter'

    vpw = ViewerManipulators.ValuePolicyWrapper

    radius = None

    decayType = vpw('material.dlLightFilterParams.decayType', uiOrder = 1)
    decayRangeStart = vpw('material.dlLightFilterParams.decayRangeStart', uiOrder = 2)
    decayRangeEnd = vpw('material.dlLightFilterParams.decayRangeEnd', uiOrder = 3)

    decayCurveFloats = vpw('material.dlLightFilterParams.decayCurve_Floats', uiOrder = 4)
    decayCurveKnots = vpw('material.dlLightFilterParams.decayCurve_Knots', uiOrder = 5)
    decayCurveInterp = vpw('material.dlLightFilterParams.decayCurve_Interpolation', uiOrder = 6)

    def __init__(self, *args):
        ViewerManipulators.LightManipulator.__init__(self, *args)

        rsHandle            = self.createHandle(AxisHandle, 'decayRangeStart')
        rsHandle.getAxis    = lambda: self.getAxis(-0.667, -0.667)
        rsHandle.minimum    = 0.0
        reHandle            = self.createHandle(AxisHandle, 'decayRangeEnd')
        reHandle.getAxis    = lambda: self.getAxis(0.0, 0.667)
        reHandle.maximum    = 100.0

        rsHandle.getMaximum = lambda: reHandle.getValue()
        reHandle.getMinimum = lambda: rsHandle.getValue()
        if self.__class__.radius:
            rHandle             = self.createHandle(AxisHandle, 'radius')
            rHandle.getAxis     = lambda: self.getAxis(0.0, 0.0)

        self.displaySettings = GL(depthTest = False,
                                  blend     = True,
                                  color     = (1,1,1,0.1),
                                  cameraLight = True)

        self.setTransformSpace(scale = 'world')

    def getAxis(self, lat, long):
        awa = None
        if not awa is None: mult = radians(awa) / 2
        else:               mult = PI
        lat  = mult * lat
        long = mult * long
        zr = sin(long)
        return Imath.V3d(sin(lat) * zr, cos(lat) * zr, -cos(long))


    def drawOutline(self, drawPick, pickId, currentView, currentCamera, manipulatorScale):

        angle = PI - radians(360) / 2

        with GL(transform = self.getWorldMatrix().sansScaling()):
            with self.displaySettings:
                for radius in (self.decayRangeStart, self.decayRangeEnd,
                               self.radius):
                    if radius:
                        ViewerUtils.drawSphere(startAngle = angle, endAngle = PI, radius = radius)
                        ViewerUtils.drawSphere(startAngle = angle, endAngle = PI, radius = radius, reverseNormals = True)


PluginRegistry = [
    ("KatanaManipulator", 2.0, "DlDecayFilter", DlDecayFilter),
]
