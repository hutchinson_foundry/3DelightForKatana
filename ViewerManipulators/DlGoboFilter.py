from __future__ import with_statement

import ViewerManipulators
import Katana, FnKatImport

FnKatImport.FromObject(Katana.Plugins.ViewerManipulatorAPI, merge = ['*'])

# Based on LightSlideMap in plugin_apis/python/ViewerManipulators
class DlGoboFilter(ViewerManipulators.LightSlideMap):

    UI_NAME = 'Gobo Filter'

    vpw = ViewerManipulators.ValuePolicyWrapper

    # Some values to please drawing stuff in LightSlideMap
    # 1.5 for centerOfInterest places manipulator on gobo filter
    # (see drawGobo in src/LightFilterViewerModifier)
    centerOfInterest = 1.5
    use_slidemap = None
    coneAngle = 30
    rotate = 0

    textureName = vpw('material.dlLightFilterParams.textureName', uiOrder = 1)
    density = vpw('material.dlLightFilterParams.density', uiOrder = 2)
    invert = vpw('material.dlLightFilterParams.invert', uiOrder = 3)
    useFilterCoordinateSystem = vpw('material.dlLightFilterParams.useFilterCoordinateSystem', uiOrder = 4)
    scaleS = vpw('material.dlLightFilterParams.scale[0]', uiOrder = 5)
    scaleT = vpw('material.dlLightFilterParams.scale[1]', uiOrder = 6)
    offsetS = vpw('material.dlLightFilterParams.offset[0]', uiOrder = 7)
    offsetT = vpw('material.dlLightFilterParams.offset[1]', uiOrder = 8)
    swrap = vpw('material.dlLightFilterParams.swrap', uiOrder = 9)
    twrap = vpw('material.dlLightFilterParams.twrap', uiOrder = 10)
    filterCoordinateSystem = vpw('material.dlLightFilterParams.filterCoordinateSystem', uiOrder = 11)

    def __init__(self, *args):
        ViewerManipulators.LightSlideMap.__init__(self, *args)
        # Don't want the rotate handle to be shown
        self.rotateHandle.radius = 0.0

PluginRegistry = [
    ("KatanaManipulator", 2.0, "DlGoboFilter", DlGoboFilter),
]
